<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['password'])) {
 
    // receiving the post params
    $email = $_POST['email'];
    $password = $_POST['password'];
 
    // get the user by email and password
    $user = $db->getUserByEmailAndPasswordUser($email, $password);
    if($user == false) {
        $user = $db->getUserByEmailAndPasswordResto($email, $password);
        if ($user == false) {
            $user = $db->getUserByEmailAndPasswordCustomer($email, $password);
            if ($user == false) {
                $user = $db->getUserByEmailAndPasswordCashier($email, $password);
            }
        }
    }

    if ($user != false) {
        // use is found
        if(array_key_exists('NamaPerusahaan', $user)) {
            $response["error"] = FALSE;
            $response["status_user"] = "admin";
            $response["user"]["id_restaurant"] = $user["UserID"];//ini id_restaurant
            $response["user"]["name"] = $user["NamaPerusahaan"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
            $response["user"]["address"] = $user["Alamat"];
            $response["user"]["path_photo"] = $user["path_photo"];
            $response["user"]["phone"] = $user["Telpon"];
			$response["user"]["keyword"] = $user["keyword"];
            $response["user"]["ig"] = $user["ig"];
            $response["user"]["fb"] = $user["fb"];
            $response["user"]["logo"] = $user["logo"];
            $response["user"]["isOrderNotifOn"] = $user["isOrderNotifOn"];
            $response["user"]["allow"] = $user["allow"];
        }
        elseif(array_key_exists('id_cust', $user)) {
            $response["error"] = FALSE;
            $response["status_user"] = "customer";
            $response["user"]["id_customer"] = $user["id_cust"];
            $response["user"]["name"] = $user["nama"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["created_at"] = $user["created"];
            $response["user"]["phone"] = $user["hp"];
            $response["user"]["status"] = $user["status"];
            $response["user"]["path_photo"] = $user["path_photo"];
        }
        elseif(array_key_exists('id_cashier', $user)) {
            $response["error"] = FALSE;
            $response["status_user"] = "cashier";
            $response["user"]["id_cashier"] = $user["id_cashier"];
            $response["user"]["id_restaurant"] = $user["UserID"];
            $response["user"]["address"] = $user["Alamat"];
            $response["user"]["name_restaurant"] = $user["name_resto"];
            $response["user"]["name"] = $user["nama"];
            $response["user"]["email"] = $user["email"];
            $response["user"]["created_at"] = $user["created"];
            $response["user"]["ig"] = $user["ig"];
            $response["user"]["fb"] = $user["fb"];
            $response["user"]["logo"] = $user["logo"];
        }
        else {
            $response["error"] = FALSE;
            $response["status_user"] = "user";
            $response["user"]["id_user"] = $user["id_user"];
            $response["user"]["name"] = $user["name"]; //nama user
            $response["user"]["email"] = $user["email"];
            $response["user"]["created_at"] = $user["created_at"];
            $response["user"]["updated_at"] = $user["updated_at"];
            $response["user"]["gender"] = $user["gender"];
            $response["user"]["path_photo"] = $user["path_photo"];
            $response["user"]["id_restaurant"] = $user["UserID"]; //id_restaurant
            $response["user"]["name_restaurant"] = $user["name_resto"];
            $response["user"]["address"] = $user["address"]; //nama restaurant
            $response["user"]["ig"] = $user["ig"];
            $response["user"]["fb"] = $user["fb"];
            $response["user"]["logo"] = $user["logo"];
        }
        
        echo json_encode($response);
    } else {
        // user is not found with the credentials
        $response["error"] = TRUE;
        $response["error_msg"] = "Your email or password is wrong. Please try again!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "Email or password is missing!";
    echo json_encode($response);
}
?>