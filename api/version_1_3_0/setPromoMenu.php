<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_menu']) && isset($_POST['promo_price']) && isset($_POST['begin_date']) && isset($_POST['end_date']) && isset($_POST['begin_time']) && isset($_POST['end_time']) && isset($_POST['note_promo'])) {
 
    // receiving the post params
    $id_menu        = $_POST['id_menu'];
    $promo_status   = "1"; // 1 = promo aktif
    $promo_price    = $_POST['promo_price'];
    $begin_date     = $_POST['begin_date'];
    $end_date       = $_POST['end_date'];
    $begin_time     = $_POST['begin_time'].":00";
    $end_time       = $_POST['end_time'].":00"; 
    $note_promo     = $_POST['note_promo'];
 
    $result = $db->setPromoMenu($id_menu, $promo_status, $promo_price, $begin_date, $end_date, $begin_time, $end_time, $note_promo);
    if($result) {
        $response["status"] = "success";
        $response["error"] = FALSE;
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while set promo menu!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "parameter is missing!";
    echo json_encode($response);
}
?>