<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
if (isset($_POST['token']) && isset($_POST['userID']) && isset($_POST['status_user'])) {
 
    // receiving the post params
    $token = $_POST['token'];
	$userID = $_POST['userID'];
    $status_user = $_POST['status_user'];
    
    $user = $db->updateToken($token, $userID, $status_user);
    if ($user) {
        // user stored successfully
        $response["error"] = FALSE;
        echo json_encode($response);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Failed update token";
        echo json_encode($response);
    }

} 

else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters token is missing!";
    echo json_encode($response);
}
?>