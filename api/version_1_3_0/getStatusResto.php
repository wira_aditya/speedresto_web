<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_resto']) && isset($_POST['id_user']) && isset($_POST['printer_cashier']) && isset($_POST['printer_kitchen']) && isset($_POST['printer_waiters'])) {
    
    $id_resto = $_POST['id_resto'];
    $id_user = $_POST['id_user'];
    $printer_cashier = $_POST['printer_cashier'];
    $printer_kitchen = $_POST['printer_kitchen'];
    $printer_waiters = $_POST['printer_waiters'];
    $info = $db->getStatusAktif($id_resto, $id_user, $printer_cashier, $printer_kitchen, $printer_waiters);

    if ($info) {
        // info already existed
        $phone = "081236660688";
        $response["error"] = FALSE;
        $response["info"]["status"] = $info["stts"];
        $response["info"]["phone"] = $phone;
		$response["info"]["pesan"] = "Thank You for using SpeedResto. Your Resto not yet registered, please contact Admin of SpeedResto in Whatsapp : ".$phone;
        echo json_encode($response);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters id resto is missing!";
    echo json_encode($response);
}
?>