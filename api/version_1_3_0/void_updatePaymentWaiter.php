<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_tr']) && isset($_POST['total']) && isset($_POST['disc']) && isset($_POST['tunai']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['disc_nota'])) {
 
    // receiving the post params
 
	$id_tr = $_POST['id_tr'];//
    $total = $_POST['total'];//
    $disc = $_POST['disc']; //valuenya dari edittext diskon di dialog bayar, bukan persen
    $tunai = $_POST['tunai'];//
    $tax = $_POST['tax'];//
    $service = $_POST['service'];//
    $disc_nota = $_POST['disc_nota']; //valuenya dari edittext diskon di dialog bayar, berupa persen
 
    $result = $db->VoidUpdatePaymentWaiter($id_tr, $total, $disc, $tunai, $tax, $service, $disc_nota);
    if($result["error"] == false) {
        $response["status"] = "success";
        $response["last_id"] = $result["last_id"];
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>

