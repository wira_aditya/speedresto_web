<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['idMenu'])) {
 
    // receiving the post params
    $idMenu = $_POST['idMenu'];
 
    $result = $db->deletePromoMenu($idMenu);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted promo menu!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id menu is missing!";
    echo json_encode($response);
}
?>