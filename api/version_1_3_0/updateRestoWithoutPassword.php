<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);

if (isset($_POST['keyword']) && isset($_POST['name']) && isset($_POST['new_email']) && isset($_POST['address']) && isset($_POST['id_restaurant']) && isset($_POST['old_email']) && isset($_POST['phone']) && isset($_POST['ig']) && isset($_POST['fb']) && isset($_POST['allow']) && isset($_POST['isOrderNotifOn'])) {

    // receiving the post params
    $name = $_POST['name'];
    $newEmail = $_POST['new_email'];
    $oldEmail = $_POST['old_email'];
    $address = $_POST['address'];
	$keyword = $_POST['keyword'];
    $id_restaurant = $_POST['id_restaurant'];
    $phone = $_POST['phone'];
    $ig = $_POST['ig'];
    $fb = $_POST['fb'];
    $isOrderNotifOn = $_POST['isOrderNotifOn']; //berupa true or false
    $allow = $_POST['allow']; //berupa 0 or 1
 
    if($newEmail == $oldEmail) {
        $result = $db->updateResto1($id_restaurant, $name, $newEmail, $address, $keyword, $phone, $ig, $fb, $isOrderNotifOn, $allow);
        if($result) {
            $response["status"] = "success";
            echo json_encode($response);
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknow error while update resto!";
            echo json_encode($response);
        }
    } else {
        $user = $db->isUserExistedUsers($newEmail);
        if($user == false) {
            $user = $db->isUserExistedResto($newEmail);
            if($user == false) {
                $user = $db->isUserExistedCustomer($newEmail);
                if($user == false) {
                    $user = $db->isUserExistedCashier($newEmail);
                }
            }
        }
        if ($user == false) {
            $result = $db->updateResto1($id_restaurant, $name, $newEmail, $address, $keyword, $ig, $fb, $isOrderNotifOn, $allow);
            if($result) {
                $response["status"] = "success";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknow error while update resto!";
                echo json_encode($response);
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Email already used!";
            echo json_encode($response);
        }
    }
    
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id_restaurant, name, email, address, keyword or gender is missing!";
    echo json_encode($response);
}

?>