<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_cashier']) && isset($_POST['id_resto'])) {
 
    // receiving the post params
    $id_cashier = $_POST['id_cashier'];
    $id_resto = $_POST['id_resto'];
 
    $result = $db->deleteBillTmp($id_cashier, $id_resto); 
    if($result) {
        $response["status"] = "success";
        $response["info"]["ig"] = $result["ig"];
        $response["info"]["fb"] = $result["fb"];
        $response["info"]["logo"] = $result["logo"];
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted booking!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>