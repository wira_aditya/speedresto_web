<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE);
 
if (isset($_POST['id_cashier'])) {
 
    $id_cashier = $_POST['id_cashier'];
    $result = $db->getListTransactionCashier($id_cashier);

    if ($result1["error"] == FALSE) {
        echo json_encode($result);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list bill!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters id resto is missing!";
    echo json_encode($response);
}
?> 