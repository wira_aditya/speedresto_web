<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE);
 
if (isset($_POST['id_tr']) && isset($_POST['id_waiter']) && isset($_POST['totalBill']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['disc']) && isset($_POST['kode'])) {
 
    // receiving the post params
    $id_waiter = $_POST['id_waiter'];
    $id_tr = $_POST['id_tr'];
    $total_bill = $_POST['totalBill'];
    $tax = $_POST['tax'];
	$service = $_POST['service'];
    $disc = $_POST['disc'];
    $code = $_POST['kode'];

    $user = $db->voidUpdateTransaction($id_tr, $total_bill, $tax, $service,$disc, $id_waiter, $code);
    if ($user) {
        // user stored successfully
        echo json_encode($user);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Error Network occurred in transaction!";
        echo json_encode($response);
    } 
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (tax, service, discount or total bill) is missing!";
    echo json_encode($response);
}
?>