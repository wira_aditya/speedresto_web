<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
if (isset($_POST['name']) && isset($_POST['price']) && isset($_POST['id_kategori']) && !isset($_FILES['image']['name']) && isset($_POST['path_photo']) && isset($_POST['id_restaurant']) && isset($_POST['description'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $price = $_POST['price'];
    $path_photo = $_POST['path_photo'];
    $description = $_POST['description'];
    $id_restaurant = $_POST['id_restaurant'];
    $type_item = "0"; // 0 = barang, 1 = jasa
    $NoItem = "SPR".rand(00000,99999);
    $category = $_POST['id_kategori']; 
    $JenisItem = $_POST['id_kategori'];

    $print = (isset($_POST['print'])) ? $_POST['print'] : "1";
    
    $user = $db->storeMenu($name, $price, $category, $path_photo, $id_restaurant, $type_item, $JenisItem, $NoItem, $description, $print);
    if ($user) {
        // user stored successfully
        $response["error"] = FALSE;
        echo json_encode($response);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknown error occurred in add menu!";
        echo json_encode($response);
    }

} 

else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name, price, category, path photo or id restaurant) is missing!";
    echo json_encode($response);
}
?>