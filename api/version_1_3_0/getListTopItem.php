<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto']) && isset($_POST['limit'])) {
 
	$fromDate = "";
	$toDate = "";
	if (isset($_POST['fromDate']) && isset($_POST['toDate'])) {
        $fromDate = $_POST['fromDate'];
        $toDate = $_POST['toDate'];
    }
    //from
    elseif (isset($_POST['fromDate'])) {
        $fromDate = $_POST['fromDate']; //set far far away date :v
    }
    //to
    elseif (isset($_POST['toDate'])) {
        $toDate = $_POST['toDate'];
    }
	
	
    $id_resto = $_POST['id_resto'];
    $limit = $_POST['limit']; 

    $result = $db->getListTopItem($id_resto, $limit, $fromDate, $toDate);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list top item!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (id resto and limit) is missing!";
    echo json_encode($response);
}
?>