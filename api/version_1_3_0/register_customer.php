<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['phone'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $phone = $_POST['phone'];

    // check if user is already existed with the same email
    if ($db->isUserExistedUsers($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "Customer already existed with " . $email;
        echo json_encode($response);
    } else {
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "Customer already existed with " . $email;
            echo json_encode($response);
        } else {

            if ($db->isUserExistedCustomer($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "Customer already existed with " . $email;
            echo json_encode($response);
            }
            else {
                if ($db->isUserExistedCashier($email)) {
                    // user already existed
                    $response["error"] = TRUE;
                    $response["error_msg"] = "Customer already existed with " . $email;
                    echo json_encode($response);
                }
                else {
                    // create a new user
                    $user = $db->storeCustomer($name, $email, $password, $phone);
                    if ($user) {
                        // user stored successfully
                        $response["error"] = FALSE;
                        $response["user"]["id_customer"] = $user["id_cust"];
                        $response["user"]["name"] = $user["nama"];
                        $response["user"]["email"] = $user["email"];
                        $response["user"]["phone"] = $user["hp"];
                        $response["user"]["created_at"] = $user["created"];
                        $response["user"]["status"] = $user["status"];
                        echo json_encode($response);
                    } else {
                        // user failed to store
                        $response["error"] = TRUE;
                        $response["error_msg"] = "Unknown error occurred in registration!";
                        echo json_encode($response);
                    }
                }
            }
        }   
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name resto, address, phone, email, or password) is missing!";
    echo json_encode($response);
}
?>