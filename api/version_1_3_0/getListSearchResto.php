<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_customer']) && isset($_POST['keyword'])) {
 
    $id_customer = $_POST['id_customer'];
    $keyword = $_POST['keyword']; 

    $result = $db->getListSearchResto($id_customer, $keyword);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list menu!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (id customer and keyword) is missing!";
    echo json_encode($response);
}
?>