<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email'])) {
 
    $email = $_POST['email'];
    $user = $db->getPathPhotoProfileResto($email);

    if ($user) {
        // user already existed
        $response["error"] = FALSE;
        $response["user"]["path_photo"] = $user["path_photo"];
        echo json_encode($response);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get photo profile!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email is missing!";
    echo json_encode($response);
}
?>