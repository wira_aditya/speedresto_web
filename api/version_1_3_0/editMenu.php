<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
 
 function compress_image($source_url, $destination_url, $quality) {
    $info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source_url); 
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source_url); 
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source_url); 
        imagejpeg($image, $destination_url, $quality); 
        return $destination_url; 
} 

// json response array
$response = array("error" => FALSE);
if(isset($_POST['type']))
{ 
    if($_POST['type'] == "update_photo")
    {
        if (isset($_POST['id_menu']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['id_kategori']) && isset($_POST['status']) && !isset($_FILES['image']['name']) && isset($_POST['description'])) 
        {  
            $id_menu = $_POST['id_menu'];
            $name = $_POST['name'];
            $price = $_POST['price'];
            $status = $_POST['status']; 
            $description = $_POST['description'];
            $path_photo = $_POST['path_photo'];

            $category = $_POST['id_kategori'];  
            $JenisItem = $_POST['id_kategori'];

            if ($_POST['status'] == "A") {
                $status = 1;
            }
            elseif ($_POST['status'] == "U") {
                $status = 0;
            }

            $print = (isset($_POST['print'])) ? $_POST['print'] : "1";
            

            $result = $db->updateMenu2($id_menu, $name, $price, $category, $JenisItem, $status, $description, $print);
                if($result) {
                    $response["status"] = "success";
                    echo json_encode($response);
                } else {
                    $response["error"] = TRUE;
                    $response["error_msg"] = "Unknow error while update menu!";
                    echo json_encode($response);
                }
        }

        else if(isset($_FILES['image']['name']))
        {
            $fileinfo = pathinfo($_FILES['image']['name']);
            $name_photo = $_FILES['image']['name'];
            $extension = "jpg";
            $tmp = explode(".", $name_photo);
            $path_photo = $tmp[0] . "." . $extension;
            $filename = compress_image($_FILES["image"]["tmp_name"], __DIR__.'/../storage/photo_menu/'.$path_photo, 40);
            // $upload_path = '../storage/photo_menu/';
            // $file_path = $upload_path . $path_photo;
            // move_uploaded_file($_FILES['image']['tmp_name'], $file_path);
            //echo $file_path;
        }
        else 
        {
            // required post params is missing
            $response["error"] = TRUE;
            $response["error_msg"] = "id menu, name, price, category or path photo is missing!";
            echo json_encode($response);
        } 
    }

    else 
    {
        if (isset($_POST['id_menu']) && isset($_POST['name']) && isset($_POST['price']) && isset($_POST['category']) && isset($_POST['status']) && isset($_POST['path_photo']) && isset($_POST['description'])) {
    
        // receiving the post params
        $id_menu = $_POST['id_menu'];
        $name = $_POST['name'];
        $price = $_POST['price'];
        $status = $_POST['status']; 
        $description = $_POST['description'];
        $path_photo = $_POST['path_photo'];
        $category = $_POST['id_kategori'];  
        $JenisItem = $_POST['id_kategori'];

        if ($_POST['status'] == "A") {
                $status = 1;
            }
        elseif ($_POST['status'] == "U") {
            $status = 0;
        }

        $print = (isset($_POST['print'])) ? $_POST['print'] : "1";

        $result = $db->updateMenu($id_menu, $name, $price, $category, $JenisItem, $path_photo, $status, $description, $print);
            if($result) {
                $response["status"] = "success";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknow error while update menu!";
                echo json_encode($response);
            }
        } else {
        // required post params is missing
        $response["error"] = TRUE;
        $response["error_msg"] = "id menu, name, price, category or path photo is missing!";
        echo json_encode($response);
        }
    }
    
}

?>