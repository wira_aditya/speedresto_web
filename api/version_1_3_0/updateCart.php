<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_tr']) && isset($_POST['id_menu']) && isset($_POST['qty'])) {
 
    $id_tr = $_POST['id_tr'];
    $id_menu = $_POST['id_menu'];
    $qty = $_POST['qty'];

    $result = $db->updateCart($id_tr, $id_menu, $qty);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while update cart!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>