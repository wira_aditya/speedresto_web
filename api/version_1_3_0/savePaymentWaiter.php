<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_tr']) && isset($_POST['total']) && isset($_POST['disc']) && isset($_POST['tunai']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['id_waiter'])  && isset($_POST['id_resto']) && isset($_POST['disc_nota'])) {
 
    // receiving the post params
    $date = date("Y-m-d H:i:s");
    $date1 = str_replace("-", "", $date);
    $date2 = str_replace(" ", "", $date1);

    $no_bill = str_replace(":", "", $date2);
	$id_tr = $_POST['id_tr'];//
    $total = $_POST['total'];//
    $disc = $_POST['disc']; //valuenya dari edittext diskon di dialog bayar, bukan persen
    $tunai = $_POST['tunai'];//
    $tax = $_POST['tax'];//
    $service = $_POST['service'];//
    $id_waiter = $_POST['id_waiter'];//
    $disc_nota = $_POST['disc_nota']; //valuenya dari edittext diskon di dialog bayar, berupa persen
	$id_resto = $_POST['id_resto'];//
 
    $result = $db->savePaymentWaiter($id_tr, $no_bill, $total, $disc, $tunai, $tax, $service, $id_waiter, $disc_nota, $id_resto);
    if($result) {
        $response["status"] = "success";
        $response["last_id"] = $result;
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>