<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto'])) {
 
    $id_resto = $_POST['id_resto'];   

    //from + to
    if (isset($_POST['fromDate']) && isset($_POST['toDate'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";
    }
    //from
    elseif (isset($_POST['fromDate'])) {
        $fromDate = $_POST['fromDate']." 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59"; 
    }
    //to
    elseif (isset($_POST['toDate'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = $_POST['toDate']." 23:59:59";
    }

    $result = $db->getHistoryBookingWithFilter($id_resto, $fromDate, $toDate);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>