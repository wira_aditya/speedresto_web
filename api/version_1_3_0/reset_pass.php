<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['password'])) {
 
    // receiving the post params
    $email = $_POST['email'];
    $password = $_POST['password'];

    // check if user is already existed with the same email
    if ($db->isUserExistedUsers($email)) {
        // user already existed
		$Change = $db->ResetPasswordUser($email, $password);
		if($Change){
			$response["error"] = FALSE;
			$response["error_msg"] = "Success update password";
		}
		else{
			$response["error"] = TRUE;
			$response["error_msg"] = "Error update";
		}
        echo json_encode($response);
    } else {
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $Change = $db->ResetPasswordResto($email, $password);
			if($Change){
				$response["error"] = FALSE;
				$response["error_msg"] = "Success update password";
			}
			else{
				$response["error"] = TRUE;
				$response["error_msg"] = "Error update";
			}
            echo json_encode($response);
        } else {

           if ($db->isUserExistedCustomer($email)) {
                // user already existed
               $Change = $db->ResetPasswordCustomer($email, $password);
				if($Change){
					$response["error"] = FALSE;
					$response["error_msg"] = "Success update password";
				}
				else{
					$response["error"] = TRUE;
					$response["error_msg"] = "Error update";
				}
                echo json_encode($response);
            }
            else{
                // user failed to store
                $response["error"] = TRUE;
                $response["error_msg"] = "The previous email you entered is not registered!";
                echo json_encode($response);
            }
        }
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>