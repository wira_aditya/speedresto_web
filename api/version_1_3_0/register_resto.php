<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['phone']) && isset($_POST['address'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $address = $_POST['address']; 
    $phone = $_POST['phone'];

    // check if user is already existed with the same email
    if ($db->isUserExistedUsers($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "Resto already existed with " . $email;
        echo json_encode($response);
    } else {
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "Resto already existed with " . $email;
            echo json_encode($response);
        } else {
            if ($db->isUserExistedCustomer($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "Resto already existed with " . $email;
            echo json_encode($response);
            }
            else {
                if ($db->isUserExistedCashier($email)) {
                    // user already existed
                    $response["error"] = TRUE;
                    $response["error_msg"] = "Resto already existed with " . $email;
                    echo json_encode($response);
                }
                else {
                    // create a new user
                    $user = $db->storeResto($name, $address, $email, $password, $phone);
                    if ($user) {
                        // user stored successfully
                        $response["error"] = FALSE;
                        $response["user"]["id_restaurant"] = $user["UserID"];
                        $response["user"]["name"] = $user["NamaPerusahaan"];
                        $response["user"]["email"] = $user["email"];
                        $response["user"]["address"] = $user["Alamat"];
                        $response["user"]["phone"] = $user["Telpon"];
                        $response["user"]["created_at"] = $user["created_at"];
                        $response["user"]["updated_at"] = $user["updated_at"];
                        $response["user"]["path_photo"] = $user["path_photo"];
                        $response["user"]["keyword"] = $user["keyword"];
                        echo json_encode($response);
                    } else {
                        // user failed to store
                        $response["error"] = TRUE;
                        $response["error_msg"] = "Unknown error occurred in registration!";
                        echo json_encode($response);
                    }
                }
            }
        }   
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name resto, address, phone, email, or password) is missing!";
    echo json_encode($response);
}
?>