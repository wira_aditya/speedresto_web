<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_booking']) && isset($_POST['id_menu'])) {
 
    // receiving the post params
    $id_booking = $_POST['id_booking'];
    $id_menu = $_POST['id_menu'];
 
    $result = $db->deleteDetailBooking($id_booking, $id_menu);
	if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Error while deleted booking!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "data is missing!";
    echo json_encode($response);
}
?>