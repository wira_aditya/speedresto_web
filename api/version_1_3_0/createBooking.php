<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_customer']) && isset($_POST['id_resto']) && isset($_POST['disc']) && isset($_POST['metode_booking']) && isset($_POST['date']) && isset($_POST['time']) && isset($_POST['address'])) {
 
    // receiving the post params
    $id_customer = $_POST['id_customer'];
    $id_resto = $_POST['id_resto'];
    $disc = $_POST['disc'];
    $metode_booking = $_POST['metode_booking'];
    $date = $_POST['date'];
    $time = $_POST['time'];
    $address = $_POST['address'];

 
    $result = $db->createBooking($id_customer, $id_resto, $disc, $metode_booking, $date, $time, $address);
    if($result==='lebih') {
        $response["error"] = TRUE;
        $response["error_msg"] = "Your book quantity more than promo quantity.";
        echo json_encode($response);
    }
    else if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else { 
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>