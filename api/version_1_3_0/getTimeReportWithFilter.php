<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto']) && isset($_POST['fromDate']) && isset($_POST['toDate'])) {
 
    $id_resto = $_POST['id_resto'];
    $fromDate = $_POST['fromDate'];
    $toDate   = $_POST['toDate'];

    if ($fromDate == "From:") {
        $fromDate = "1900-2-1";
    }
    if ($toDate == "To:") {
        $toDate = "2101-1-31";
    }

    $result = $db->getTimeReportWithFilter($id_resto,$fromDate,$toDate);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error !";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>