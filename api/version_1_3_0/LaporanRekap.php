<?php 
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=Recap_Report.xls");
	
	require_once 'include/DB_Connect.php';
   	$db = new Db_Connect();
    $conn = $db->connect();

    $userID = $_GET['id_resto'];
    $dari = $_GET['fromDate']." 00:00:00";
    $sampai = $_GET['toDate']." 23:59:59";
 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak IKM</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<style>
		*{
			padding:0; 
			box-sizing: border-box; 
			font-family: 'Roboto', sans-serif;
			-moz-box-sizing: border-box;
			font-size : 12px;
		}	
		table{
			border-top:0.6px solid #000;
			border-bottom:0.6px solid #000;
			width: 100%;
			border-collapse: collapse;
		}
		th,td{padding: 5px;}
		th{text-align: center; font-weight: bold;}
		.kanan{float: right;}
		.kiri{float: left;}
		.half{width: 50%;padding:5px 10px 0px 0px;flex:1;position: relative;}
		@page {size: A4;padding-right:10px;size: landscape;}
	    @media print {
	        .page {
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: initial;
	            page-break-after: always;
	            page-break-before:always;
	        }
	    }
		.label{width:25%;display: inline-block;}
		.txt{width: 73%;display:inline-block;}
		.label,.txt{padding: 5px 0px}
		/*header*/
		header>.top{width: 100%;font-weight: bold}
		.top>p{margin-top: 10px}
		.group{
		    position: absolute;
		    bottom: 0;
		    width: 100%;
		}
		/*content*/
	    .head>th{border-bottom:double #000;}
		.footer{}
		
		#pageFooter:after {
		counter-increment: page;
		content: counter(page);
	</style>
</head>
<body>

	<header>
		<div class="top">
			<center><h1>LAPORAN REKAP SPEEDRESTO</h1></center>
		</div>
	</header>
	<content>
	
		<table border='1'>
			<thead>
				<tr>
					<th style="width:10%">No</th> <!-- no -->
					<th style="width:10%">Tanggal</th> <!-- table _ikm field desa_ -->
					<th style="width:10%">No Table</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Bill</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Disc.</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Disc. Nota</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Tax</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Service</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Charge</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Grand Total</th> <!-- table _ikm field telp_ -->
				</tr>
			</thead>
			<tbody>
				
			<?php
				
				$con="";
				$no=1;
				$t_bill = 0;
				$t_disc = 0;
				$t_nota = 0;
				$t_tax = 0;
				$t_serv = 0;
				$t_charge = 0;
				$t_grand = 0;

				$stmt = $conn->prepare("SELECT tr.charge,tr.total_bill,tr.disc,tr.disc_nota, DATE_FORMAT(tr.date_order, '%d-%m-%Y') AS tgl, tr.no_table, tr.tax, tr.service FROM transaction tr WHERE tr.UserID = ? AND tr.date_order BETWEEN ? AND ? GROUP BY tr.id_transaction ORDER BY tr.id_transaction DESC");
		        $stmt->bind_param("sss", $userID, $dari, $sampai);
		        $result_data["transaction"] = array();
		        $result_data["error"] = FALSE;
		        $result_data["num_rows"] = 0;

		        if ($stmt->execute()) {
		            $result = $stmt->get_result();
		            $result_data["num_rows"] = $result->num_rows;

		            while ($row = $result->fetch_assoc()) {
		            	echo "<tr>
							<td>$no</td>
							<td>".$row["tgl"]."</td>
							<td>".$row["no_table"]."</td>
							<td align='right'>".number_format($row["total_bill"],2,",",".")."</td>
							<td align='right'>".number_format($row["disc"],2,",",".")."</td>
							<td align='right'>".number_format($row["disc_nota"],2,",",".")."</td>
							<td align='right'>".number_format($row["tax"],2,",",".")."</td>
							<td align='right'>".number_format($row["service"],2,",",".")."</td>
							<td align='right'>".number_format($row["charge"],2,",",".")."</td>
							<td align='right'>".number_format(($row["total_bill"] - $row["disc"] - $row["disc_nota"] + $row["tax"] + $row["service"] + $row["charge"]),2,",",".")."</td>
						</tr>"; 
						$no++;
						$t_bill = $t_bill + $row["total_bill"];
						$t_disc = $t_disc + $row["disc"];
						$t_nota = $t_nota + $row["disc_nota"];
						$t_tax = $t_tax + $row["tax"];
						$t_serv = $t_serv + $row["service"];
						$t_charge = $t_charge + $row["charge"];
						$t_grand = $t_grand + ($row["total_bill"] - $row["disc"] - $row["disc_nota"] + $row["tax"] + $row["service"] + $row["charge"]);
		            }
		            $stmt->free_result();
		            $stmt->close();
		            
		            echo "<tr>
						<td align='center' colspan='3'><b>TOTAL</b></td>
						<td align='right'><b>".number_format($t_bill,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_disc,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_nota,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_tax,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_serv,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_charge,2,",",".")."</b></td>
						<td align='right'><b>".number_format($t_grand,2,",",".")."</b></td>
					</tr>";            
		        }
			?>
	
			</tbody>	
		</table>
		<div class="half kiri">
			Tanggal Cetak : <?php echo date('d-m-Y');?>
		</div>
		
	</content>
</body>
</html>
<script>
</script>