<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_tr']) && isset($_POST['id_waiter']) && isset($_POST['kode'])) {
 
    $id_tr = $_POST['id_tr']; 
    $id_waiter = $_POST['id_waiter']; 
    $code = $_POST['kode'];

    $result = $db->getListCartUpdate($id_tr, $id_waiter, $code);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list menu!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>