<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 

 
if (isset($_POST['id']) && isset($_POST['dateTimeUser'])) {
 
    $id_menu = $_POST['id_menu']; 
    $tgl = date('Y-m-d');
    $waktu = date('H:I:s');


    $result = $db->getStatusMenu($id_menu, $tgl, $waktu);
    if ($result["error"] == FALSE) {
        echo json_encode($result);   
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list menu!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>