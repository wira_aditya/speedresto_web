<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();

function compress_image($source_url, $destination_url, $quality, $nama) {
    ini_set('memory_limit', '128M');
    include 'ImageResize.php';
    
    $info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source_url); 
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source_url); 
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source_url); 
        imagejpeg($image, $destination_url, $quality); 
    
    
    //use \Eventviva\ImageResize;
    //use \Eventviva\ImageResizeException;
    $imageR = new \Eventviva\ImageResize($destination_url);
    $imageR->resize(100, 100);
    $imageR->save(__DIR__.'/../storage/photo_menu_s/'.$nama);
    
    $imageR2 = new \Eventviva\ImageResize($destination_url);
    $imageR2->resize(300, 300);
    $imageR2->save(__DIR__.'/../storage/photo_menu_m/'.$nama);
    return $imageR2; 
} 


// json response array
$response = array("error" => FALSE);
if(isset($_FILES['image']['name']) && $_FILES['image']['size'] > 1048576) {
    $response["error"] = TRUE;
    $response["error_msg"] = "Your image size more than 1MB";
    echo json_encode($response);
}
else if (isset($_POST['name']) && isset($_POST['price']) && isset($_POST['id_kategori']) && !isset($_FILES['image']['name']) && isset($_POST['id_restaurant']) && isset($_POST['description'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $price = $_POST['price'];
    $description = $_POST['description'];
    $id_restaurant = $_POST['id_restaurant'];
    $type_item = "0"; // 0 = barang, 1 = jasa
    $NoItem = "SPR".rand(00000,99999);
    $category = $_POST['id_kategori'];
    
    $print = (isset($_POST['print'])) ? $_POST['print'] : "1";
    
    $name_photo = ($db->getLastIdMenu());
    //$fileinfo = pathinfo($_FILES['image']['name']);
    //$extension = $fileinfo['extension'];
    $extension = "jpg";
    $path_photo = $name_photo . "." . $extension;   
    $user = $db->storeMenu($name, $price, $category, $path_photo, $id_restaurant, $type_item, $JenisItem, $NoItem, $description, $print);
    if ($user) {
        // user stored successfully
        $response["error"] = FALSE;
        echo json_encode($response);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknown error occurred in add menu!";
        echo json_encode($response);
    }

} else if(isset($_FILES['image']['name'])) {
    $name_photo = ($db->getLastIdMenu()) - 1;
    $fileinfo = pathinfo($_FILES['image']['name']);
    //$extension = $fileinfo['extension'];
    $extension = "jpg";
    $path_photo = $name_photo . "." . $extension;
    $filename = compress_image($_FILES["image"]["tmp_name"], __DIR__.'/../storage/photo_menu/'.$path_photo, 40, $path_photo);

    //$upload_path = 'storage/photo_menu/';
    //$file_path = $upload_path . $name_photo . "." . $extension;
    //move_uploaded_file($_FILES['image']['tmp_name'], $file_path);

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name, price, category, path photo or id restaurant) is missing!";
    echo json_encode($response);
}
?>