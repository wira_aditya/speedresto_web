<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_booking']) && isset($_POST['id_resto']) && isset($_POST['dateTimeUser'])) {
 
    $id_booking = $_POST['id_booking']; 
    $id_resto = $_POST['id_resto'];
    $dateTimeUser = $_POST['dateTimeUser'];

    $result = $db->getListMenuDetailBookingCustomer($id_booking, $id_resto, $dateTimeUser);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list detail booking!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>