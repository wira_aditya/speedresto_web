<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_kategori']) && isset($_POST['kategori']) && isset($_POST['status'])) {
 
    $id_kategori = $_POST['id_kategori'];
    $kategori = $_POST ['kategori']; 
    $status = $_POST['status'];

    $result = $db->editKategori($id_kategori, $kategori, $status);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>