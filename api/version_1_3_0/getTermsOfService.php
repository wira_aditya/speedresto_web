<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['type_user'])) {
    
    $type_user = $_POST['type_user'];
    
    if ($type_user == "waiters"){
        $response["tos"] = "TERMS OF USE BELOW MUST BE READ BEFORE USING THIS APPLICATION. THE USAGE OF THIS APPLICATION INDICATES ACCEPTANCE OF THE TERMS AND CONDITIONS BELOW.\n
SpeedResto is managed by Bamboomedia(we, us, or our). By  use of the Application, you acknowledge that you have read and understood, and agree to the Terms of Use set out below and other terms and conditions in relation to the Application, including but not limited to confidentiality and FAQs, which form an integral part of these Terms of Use (Terms). You must be at least eighteen (18) years old to use the Site.
Please note that we may change, modify, add and delete these Terms at any time without prior notice. The Terms must be read periodically. By continuing to use this Site after such changes to these Terms, visitors, users or Registered Users (you or user) agree and consent to the changes. If you use any of our other services, then your usage is based on the submission to and acceptance of the terms and conditions applicable to such services.\n\n
SCOPE OF OUR SERVICES\n
1.	Changes in market conditions or circumstances that occur can lead to changes in a short time causing the information provided to be inaccurate or not applicable. In case of any problems, contact customer service and they will assist you.\n
2.	This Application does not make any representations and should not be construed as making any recommendations or suggestions of the level of service quality or rating of the Resto listed on the Application. We hereby declare denial of any claims, losses or liability with respect to the quality or status of existing Resto listed on the Application.\n
3.	SpeedResto hereby grants the user certain limited rights (constituting a “Limited Licence”), which cannot be transferred or assigned, to access and use the Application to the extent expressly permitted by these Terms. In connection with this Limited Licence, we are not granting you any other rights or licences with respect to use of the Application; rights or licenses not expressly granted, are wholly owned by Bamboomedia or other third party owners of such licence. Content available on the Application(including the software infrastructure used to provide the Content) is wholly owned by Bamboomedia.\n
4.	You must bear all responsibility for the content of the reviews that you provide or submit. You allow Bamboomedia to act when there is a konten that violates rights of Bamboomedia or the rights of SpeedResto.\n
5.	You agree to not hold SpeedResto and Bamboomedia responsible for losses or damages that may occur as a result of fraud, forgery or false accusations of intellectual property rights infringement.\n
6.	All Intellectual Property Rights on this Site are owned by Bamboomedia. All information and materials, including but not limited to: software, text, data, graphics, images, sounds, videos, trade marks, logos, icons, html codes and other codes on this website are prohibited to be published, modified, copied, reproduced, duplicated or altered in any way outside the area of this Application without the express written permission of Bamboomedia. If you violate these rights, Bamboomedia reserves the right to bring a civil claim for the full amount of damages or losses suffered. These violations may also constitute criminal offences.
";
        echo json_encode($response);
    }
    elseif ($type_user == "resto"){
        $response["tos"] = "TERMS OF USE BELOW MUST BE READ BEFORE USING THIS APPLICATION. THE USAGE OF THIS APPLICATION INDICATES ACCEPTANCE OF THE TERMS AND CONDITIONS BELOW.\n
SpeedResto is managed by Bamboomedia(we, us, or our). By  use of the Application, you acknowledge that you have read and understood, and agree to the Terms of Use set out below and other terms and conditions in relation to the Application, including but not limited to confidentiality and FAQs, which form an integral part of these Terms of Use (Terms). You must be at least eighteen (18) years old to use the Site.
Please note that we may change, modify, add and delete these Terms at any time without prior notice. The Terms must be read periodically. By continuing to use this Site after such changes to these Terms, visitors, users or Registered Users (you or user) agree and consent to the changes. If you use any of our other services, then your usage is based on the submission to and acceptance of the terms and conditions applicable to such services.\n\n
SCOPE OF OUR SERVICES\n
1.	Changes in market conditions or circumstances that occur can lead to changes in a short time causing the information provided to be inaccurate or not applicable. In case of any problems, contact customer service and they will assist you.\n
2.	This Application does not make any representations and should not be construed as making any recommendations or suggestions of the level of service quality or rating of the Resto listed on the Application. We hereby declare denial of any claims, losses or liability with respect to the quality or status of existing Resto listed on the Application.\n
3.	SpeedResto hereby grants the user certain limited rights (constituting a “Limited Licence”), which cannot be transferred or assigned, to access and use the Application to the extent expressly permitted by these Terms. In connection with this Limited Licence, we are not granting you any other rights or licences with respect to use of the Application; rights or licenses not expressly granted, are wholly owned by Bamboomedia or other third party owners of such licence. Content available on the Application(including the software infrastructure used to provide the Content) is wholly owned by Bamboomedia.\n
4.	You must bear all responsibility for the content of the reviews that you provide or submit. You allow Bamboomedia to act when there is a konten that violates rights of Bamboomedia or the rights of SpeedResto.\n
5.	You agree to not hold SpeedResto and Bamboomedia responsible for losses or damages that may occur as a result of fraud, forgery or false accusations of intellectual property rights infringement.\n
6.	All Intellectual Property Rights on this Site are owned by Bamboomedia. All information and materials, including but not limited to: software, text, data, graphics, images, sounds, videos, trade marks, logos, icons, html codes and other codes on this website are prohibited to be published, modified, copied, reproduced, duplicated or altered in any way outside the area of this Application without the express written permission of Bamboomedia. If you violate these rights, Bamboomedia reserves the right to bring a civil claim for the full amount of damages or losses suffered. These violations may also constitute criminal offences.";
        echo json_encode($response);
    }
    elseif ($type_user == "customer"){
        $response["tos"] = "TERMS OF USE BELOW MUST BE READ BEFORE USING THIS APPLICATION. THE USAGE OF THIS APPLICATION INDICATES ACCEPTANCE OF THE TERMS AND CONDITIONS BELOW.\n
SpeedResto is managed by Bamboomedia(we, us, or our). By  use of the Application, you acknowledge that you have read and understood, and agree to the Terms of Use set out below and other terms and conditions in relation to the Application, including but not limited to confidentiality and FAQs, which form an integral part of these Terms of Use (Terms). You must be at least eighteen (18) years old to use the Site.
Please note that we may change, modify, add and delete these Terms at any time without prior notice. The Terms must be read periodically. By continuing to use this Site after such changes to these Terms, visitors, users or Registered Users (you or user) agree and consent to the changes. If you use any of our other services, then your usage is based on the submission to and acceptance of the terms and conditions applicable to such services.\n\n

SCOPE OF OUR SERVICES\n
1.	 Through the Application, SpeedResto provides an mobile platform where you can browse different types of restaurant, cafe and other resto services, temporary order as well as make a booking code. Users can make bookings of menus provided by  restaurant, cafe and any other resto in list of SpeedResto.\n
2.	Although we will use our expertise with caution in performing the services, we do not verify, and do not guarantee, that all information provided is accurate, complete, correct or the latest available, and we are not responsible for any errors (including placement and typing errors), obstruction (either because of temporary and/or partial, damage, repair or improvement to the site or otherwise), inaccuracy, misleading or false information or non-delivery of information. This includes every information provided on our Application or other platforms relating to the menu name, menu price, menu image or any other information provided in relation to the menu.\n
3.	 Changes in market conditions or circumstances that occur can lead to changes in a short time causing the information provided to be inaccurate or not applicable. In case of any problems, contact customer service and they will assist you.\n
4.	This Application does not make any representations and should not be construed as making any recommendations or suggestions of the level of service quality or rating of the Resto listed on the Application. We hereby declare denial of any claims, losses or liability with respect to the quality or status of existing Resto listed on the Application.\n
5.	You must bear all responsibility for the content of the reviews that you provide or submit. You allow Bamboomedia to act when there is a konten that violates rights of Bamboomedia or the rights of SpeedResto.\n
6.	You agree to not hold SpeedResto and Bamboomedia responsible for losses or damages that may occur as a result of fraud, forgery or false accusations of intellectual property rights infringement.\n
7.	All Intellectual Property Rights on this Site are owned by Bamboomedia. All information and materials, including but not limited to: software, text, data, graphics, images, sounds, videos, trade marks, logos, icons, html codes and other codes on this website are prohibited to be published, modified, copied, reproduced, duplicated or altered in any way outside the area of this Application without the express written permission of Bamboomedia. If you violate these rights, Bamboomedia reserves the right to bring a civil claim for the full amount of damages or losses suffered. These violations may also constitute criminal offences.
";
        echo json_encode($response);
    }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>