<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);

if (isset($_POST['name']) && isset($_POST['new_email']) && isset($_POST['phone']) && isset($_POST['id_customer']) && isset($_POST['old_email']) &&isset($_POST['old_pass']) && isset($_POST['new_pass'])) {

    // receiving the post params
    $name = $_POST['name'];
    $newEmail = $_POST['new_email'];
    $oldEmail = $_POST['old_email'];
    $newPassword = $_POST['new_pass'];
    $oldPassword = $_POST['old_pass'];
    $phone = $_POST['phone'];
    $id_cust = $_POST['id_customer'];

    if($newEmail == $oldEmail) {
        if(checkCorrectPassword($oldEmail, $oldPassword)) {
            $result = $db->updateCustomer2($id_cust, $name, $newEmail, $phone, $newPassword);
            if($result) {
                $response["status"] = "success";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknow error while update user!";
                echo json_encode($response);
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Current password is not correct!";
            echo json_encode($response);
        }
        
    } else {
        $user = $db->isUserExistedUsers($newEmail);
        if($user == false) {
            $user = $db->isUserExistedResto($newEmail);
            if ($user == false) {
                $user = $db->isUserExistedCustomer($newEmail);
                if($user == false) {
                    $user = $db->isUserExistedCashier($newEmail);
                }
            }
        }
        if ($user == false) {
            if(checkCorrectPassword($oldEmail, $oldPassword)) {
                $result = $db->updateCustomer2($id_cust, $name, $newEmail, $phone, $newPassword);
                if($result) {
                    $response["status"] = "success";
                    echo json_encode($response);
                } else {
                    $response["error"] = TRUE;
                    $response["error_msg"] = "Unknow error while update user!";
                    echo json_encode($response);
                }
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Current password is not correct!";
                echo json_encode($response);
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Email already used!";
            echo json_encode($response);
        }
    }
    
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id_cust, name, email, phone, or password is missing!";
    echo json_encode($response);
}


function checkCorrectPassword($email, $password) {
    $db = new DB_Functions();
    $user = $db->getUserByEmailAndPasswordUser($email, $password);
    if($user == false) {
        $user = $db->getUserByEmailAndPasswordResto($email, $password);
        if($user == false) {
            $user = $db->getUserByEmailAndPasswordCustomer($email, $password);
        }
    }
    if ($user) {
        return true;
    } else {
        return false;
    }
}

?>