<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto'])) {
 
    $id_resto = $_POST['id_resto']; 
    $id_customer = (isset($_POST['id_customer']) && $_POST['id_customer']!=''); 
    $fromCust = (isset($_POST['fromCust']) && $_POST['fromCust']=='1') ? '1': '0';  // 1 dari customer

    $result = $db->getkategori($id_resto, $fromCust, $id_customer);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>