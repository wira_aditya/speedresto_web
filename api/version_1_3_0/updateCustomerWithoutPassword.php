<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);

if (isset($_POST['name']) && isset($_POST['new_email']) && isset($_POST['phone']) && isset($_POST['id_customer']) && isset($_POST['old_email'])) {

    // receiving the post params
    $name = $_POST['name'];
    $newEmail = $_POST['new_email'];
    $oldEmail = $_POST['old_email'];
    $phone = $_POST['phone'];
    $id_customer = $_POST['id_customer'];

    if($newEmail == $oldEmail) {
        $result = $db->updateCustomer1($id_customer, $name, $newEmail, $phone);
        if($result) {
            $response["status"] = "success";
            echo json_encode($response);
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknow error while update user!";
            echo json_encode($response);
        }
    } else {
        $user = $db->isUserExistedUsers($newEmail);
        if($user == false) {
            $user = $db->isUserExistedResto($newEmail);
            if($user == false) {
                $user = $db->isUserExistedCustomer($newEmail);
                if($user == false) {
                    $user = $db->isUserExistedCashier($newEmail);
                }
            }
        }
        if ($user == false) {
            $result = $db->updateCustomer1($id_customer, $name, $newEmail, $phone);
            if($result) {
                $response["status"] = "success";
                echo json_encode($response);
            } else {
                $response["error"] = TRUE;
                $response["error_msg"] = "Unknow error while update user!";
                echo json_encode($response);
            }
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Email already used!";
            echo json_encode($response);
        }
    }
    
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id_cust, name, email or phone is missing!";
    echo json_encode($response);
}

?>