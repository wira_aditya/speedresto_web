<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_transaction'])) {
 
    // receiving the post params
    $id_transaction = $_POST['id_transaction'];
 
    $result = $db->unpaidTransactionCashier($id_transaction);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id is missing!";
    echo json_encode($response);
}
?>