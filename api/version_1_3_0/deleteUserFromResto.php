<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['idUser'])) {
 
    // receiving the post params
    $idUser = $_POST['idUser'];
 
    $result = $db->deleteUserFromResto($idUser);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted user!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id user is missing!";
    echo json_encode($response);
}
?>