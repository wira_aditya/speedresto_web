<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto'])) {
 
    $id_resto = $_POST['id_resto'];

    $result = $db->getOrderByResto($id_resto);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result); 
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list menu!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (id resto and keyword) is missing!";
    echo json_encode($response);
}
?>