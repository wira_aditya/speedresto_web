<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto']) && isset($_POST['waktu'])) {
 
    $waktu = $_POST['waktu']; 
    $id_resto = $_POST['id_resto'];

    $result = $db->CheckMenuUpdate($id_resto, $waktu);

    if ($result["update"] == TRUE) {
        
        echo json_encode($result);
    
    } else {
        echo json_encode($result);
    }
} else {
    $response["error"] = TRUE;
    $response["update"] = FALSE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>