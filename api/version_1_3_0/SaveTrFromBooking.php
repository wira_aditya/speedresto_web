<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE);
 
if (isset($_POST['idUser']) && isset($_POST['id_booking']) && isset($_POST['noTable']) && isset($_POST['totalBill']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['disc']) && isset($_POST['idResto']) && isset($_POST['metode_booking']) && isset($_POST['delivery_address']) && isset($_POST['delivery_date']) && isset($_POST['delivery_time']) && isset($_POST['id_cust'])) {
 
    // receiving the post params
	$idUser = $_POST['idUser'];
    $id_booking = $_POST['id_booking'];
    $no_table = $_POST['noTable'];
    $total_bill = $_POST['totalBill'];
	$tax = $_POST['tax'];
	$service = $_POST['service'];
	$disc = $_POST['disc'];
	$id_resto = $_POST['idResto'];
    $status = "U"; //unpaid / belum dibayar
    $jenis = "1"; //jenis order: dari booking

    $id_cust = $_POST['id_cust'];
    $metode_booking = $_POST['metode_booking'];
    $delivery_date = $_POST['delivery_date'];
    $delivery_time = $_POST['delivery_time'];
    $delivery_address = $_POST['delivery_address'];

    /*$metode_booking = "3";
    $delivery_date = "2017-02-01";
    $delivery_time = "10:45";
    $delivery_address = "jala";*/

    $user = $db->storeTransactionFromBooking($idUser, $id_booking, $no_table, $total_bill, $tax, $service, $disc, $status, $id_resto, $jenis, $metode_booking, $delivery_date, $delivery_time, $delivery_address, $id_cust);
    if ($user) {
        // user stored successfully
        echo json_encode($user);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Error network occurred in transaction!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (booking code,table number, tax, service, discount or total bill) is missing!";
    echo json_encode($response);
}
?>

