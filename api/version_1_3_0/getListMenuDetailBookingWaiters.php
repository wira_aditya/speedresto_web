<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['kode_booking']) && isset($_POST['id_resto']) && isset($_POST['dateTimeUser'])) {
 
    $kode_booking = $_POST['kode_booking']; 
    $id_resto = $_POST['id_resto'];
    $dateTimeUser = $_POST['dateTimeUser'];

    $result = $db->getListMenuDetailBookingWaiters($kode_booking, $id_resto, $dateTimeUser);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Booking Code Not Found Or Already Enabled";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>