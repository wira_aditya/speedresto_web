<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['idMenu']) && isset($_POST['path_photo'])) {
 
    // receiving the post params
    $idMenu = $_POST['idMenu'];
    $path_photo = $_POST['path_photo'];
 
    $result = $db->deleteMenu($idMenu, $path_photo);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted menu!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id menu is missing!";
    echo json_encode($response);
}
?>