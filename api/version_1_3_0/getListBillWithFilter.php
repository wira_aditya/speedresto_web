<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE);
 
if (isset($_POST['id_resto'])) {
 
    $id_resto = $_POST['id_resto'];
    $id_user = 0;
    $payment = 0;

    
    //from + to + waiters + payment
    if (isset($_POST['fromDate']) && isset($_POST['toDate']) && isset($_POST['id_user']) && isset($_POST['payment_method'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";
        $id_user = $_POST['id_user'];
        $payment = $_POST['payment_method'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from + waiters + payment
    elseif (isset($_POST['fromDate']) && isset($_POST['id_user']) && isset($_POST['payment_method'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = "2101-1-31 23:59:59";
        $payment = $_POST['payment_method'];
        $id_user = $_POST['id_user'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //to + waiters + payment
    elseif (isset($_POST['toDate']) && isset($_POST['id_user']) && isset($_POST['payment_method'])) {
        $fromDate = "1900-2-1 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";
        $payment = $_POST['payment_method'];
        $id_user = $_POST['id_user'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from + to + payment
    elseif (isset($_POST['fromDate']) && isset($_POST['toDate']) && isset($_POST['payment_method'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";
        $payment = $_POST['payment_method'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from + to + waiters
    elseif (isset($_POST['fromDate']) && isset($_POST['toDate']) && isset($_POST['id_user'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";
        $id_user = $_POST['id_user'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from + to
    elseif (isset($_POST['fromDate']) && isset($_POST['toDate'])) {
        $fromDate = $_POST['fromDate']." 00:00:00";
        $toDate = $_POST['toDate']." 23:59:59";     
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from + waiters
    elseif (isset($_POST['fromDate']) && isset($_POST['id_user'])) {
        $fromDate = $_POST['fromDate']." 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $id_user = $_POST['id_user'];   
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment); 
    }
    //from + payment
    elseif (isset($_POST['fromDate']) && isset($_POST['payment_method'])) {
        $fromDate = $_POST['fromDate']." 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $payment = $_POST['payment_method'];   
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment); 
    }
    //to + waiters
    elseif (isset($_POST['toDate']) && isset($_POST['id_user'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = $_POST['toDate']." 23:59:59";   
        $id_user = $_POST['id_user'];    
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //to + payment
    elseif (isset($_POST['toDate']) && isset($_POST['payment_method'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = $_POST['toDate']." 23:59:59";   
        $payment = $_POST['payment_method'];    
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //waiters + payment
    elseif (isset($_POST['id_user']) && isset($_POST['payment_method'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";   
        $id_user = $_POST['id_user']; 
        $payment = $_POST['payment_method'];   
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);
    }
    //from
    elseif (isset($_POST['fromDate'])) {
        $fromDate = $_POST['fromDate']." 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);    
    }
    //to
    elseif (isset($_POST['toDate'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = $_POST['toDate']." 23:59:59";
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);    
    }
    //waiters
    elseif (isset($_POST['id_user'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $id_user = $_POST['id_user'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);   
    }
    //payment
    elseif (isset($_POST['payment_method'])) {
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $payment = $_POST['payment_method'];
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);   
    }
    //all waiters + all date
    elseif (isset($_POST['id_resto'])){
        $fromDate = "1900-2-1 00:00:00"; //set far far away date :v
        $toDate = "2101-1-31 23:59:59";
        $result = $db->getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment);   
    }


                
    if ($result1["error"] == FALSE) {
        echo json_encode($result);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list bill!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters id resto is missing!";
    echo json_encode($response);
}
?> 