<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_resto']) && isset($_POST['id_customer'])) {
 
    // receiving the post params
    $id_resto = $_POST['id_resto'];
    $id_customer = $_POST['id_customer'];
 
    $result = $db->deleteMyResto($id_resto, $id_customer);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted resto!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id resto is missing!";
    echo json_encode($response);
}
?>