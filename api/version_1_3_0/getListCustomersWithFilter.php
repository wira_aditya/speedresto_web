<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto'])) {

    $id_resto = $_POST['id_resto'];

    //from + to
    if (isset($_POST['fromDate']) && isset($_POST['toDate'])) {
        $fromDate = $_POST['fromDate'];
        $toDate = $_POST['toDate'];     
        $result = $db->getListCustomersWithFilter($id_resto, $fromDate, $toDate);
    }

    //from
    elseif (isset($_POST['fromDate'])) {
        $fromDate = $_POST['fromDate']; 
        $toDate = "2101-1-31"; //set far far away date :v
        $result = $db->getListCustomersWithFilter($id_resto, $fromDate, $toDate);    
    }
    //to
    elseif (isset($_POST['toDate'])) {
        $fromDate = "1900-2-1"; //set far far away date :v
        $toDate = $_POST['toDate'];
        $result = $db->getListCustomersWithFilter($id_resto, $fromDate, $toDate);    
    }

    if ($result["error"] == FALSE) {
        echo json_encode($result);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list bill!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters id resto is missing!";
    echo json_encode($response);
}
?> 