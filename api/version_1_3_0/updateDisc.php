<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['discount']) && isset($_POST['tax']) && isset($_POST['service'])) {
 
    $email = $_POST['email'];
    $disc = $_POST['discount'];
    $tax = $_POST['tax'];
    $service = $_POST['service'];
    $info = $db->updateDisc($email, $disc, $tax, $service);

    if ($info) {
        // info already existed
        $response["error"] = FALSE;
        echo json_encode($response);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error !";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>