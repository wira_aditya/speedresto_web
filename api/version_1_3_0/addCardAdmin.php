<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto']) && isset($_POST['name_card']) && isset($_POST['charge'])) {
 
    $id_resto = $_POST['id_resto'];
    $name_card = $_POST ['name_card']; 
    $charge = $_POST['charge'];

    $result = $db->addCardAdmin($id_resto, $name_card, $charge);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>