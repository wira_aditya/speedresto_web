<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_customer']) && isset($_POST['id_menu'])) {
 
    // receiving the post params
    $id_customer = $_POST['id_customer'];
    $id_menu = $_POST['id_menu'];
 
    $result = $db->deleteCart($id_customer, $id_menu);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted booking!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>