<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_resto']) && isset($_POST['dateTimeUser'])) {
 
    $id_resto = $_POST['id_resto'];
    $dateTimeUser = $_POST['dateTimeUser']; 
    if (isset($_POST['isFromAdmin'])) {
        $isFromAdmin = "yes";
    } else {
        $isFromAdmin = "no";
    } 

    $result = $db->getListMenu($id_resto, $isFromAdmin, $dateTimeUser);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result); 
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while get list menu!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (id resto and keyword) is missing!";
    echo json_encode($response);
}
?>