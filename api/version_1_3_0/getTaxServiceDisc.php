<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_resto'])) {
    
    $id_resto = $_POST['id_resto'];
    $info = $db->getTaxServiceDisc($id_resto);

    if ($info) {
        // info already existed
        $response["error"] = FALSE;
        $response["info"]["disc"] = $info["disc_order"];
        $response["info"]["tax"] = $info["tax"];
        $response["info"]["service"] = $info["service"];
        echo json_encode($response);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters email is missing!";
    echo json_encode($response);
}
?>