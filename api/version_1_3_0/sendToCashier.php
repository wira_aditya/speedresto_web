<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_tr']) && isset($_POST['id_resto'])) {
 
    $id_tr = $_POST['id_tr'];
    $id_resto = $_POST['id_resto'];

    $result = $db->sendToCashier($id_tr, $id_resto);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Dont have item in this transaction or token cashier";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>