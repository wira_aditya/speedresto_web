<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['total']) && isset($_POST['disc']) && isset($_POST['tunai']) && isset($_POST['kartu']) && isset($_POST['id_kartu']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['charge']) && isset($_POST['id_cashier']) && isset($_POST['persen_charge']) && isset($_POST['disc_nota'])) {
 
    // receiving the post params
    $date = date("Y-m-d H:i:s");
    $date1 = str_replace("-", "", $date);
    $date2 = str_replace(" ", "", $date1);

    $no_bill = str_replace(":", "", $date2);
    $total = $_POST['total'];
    $disc = $_POST['disc'];
    $tunai = $_POST['tunai'];
    $kartu = $_POST['kartu'];
    $id_kartu = $_POST['id_kartu'];
    $tax = $_POST['tax'];
    $service = $_POST['service'];
    $charge = $_POST['charge'];
    $id_cashier = $_POST['id_cashier'];
    $persen_charge = $_POST['persen_charge'];
    $disc_nota = $_POST['disc_nota'];
 
    $result = $db->savePayment($no_bill, $total, $disc, $tunai, $kartu, $id_kartu, $tax, $service, $charge, $id_cashier, $persen_charge, $disc_nota);
    if($result) {
        $response["status"] = "success";
        $response["last_id"] = $result;
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>