<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_customer']) && isset($_POST['id_resto'])) {
 
    $id_customer = $_POST['id_customer'];
    $id_resto = $_POST['id_resto'];

    $result = $db->addRestoToCustomerDetail($id_customer, $id_resto);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while add user to resto!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "id cust or id restaurant is missing!";
    echo json_encode($response);
}
?>