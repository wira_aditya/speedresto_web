<?php 
	header("Content-type: application/vnd.ms-excel");
	header("Content-Disposition: attachment; filename=Recap_Report.xls");
	
	require_once 'include/DB_Connect.php';
   	$db = new Db_Connect();
    $conn = $db->connect();

    $userID = '389';
 ?>
 
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Cetak IKM</title>
	<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
	<style>
		*{
			padding:0; 
			box-sizing: border-box; 
			font-family: 'Roboto', sans-serif;
			-moz-box-sizing: border-box;
			font-size : 12px;
		}	
		table{
			border-top:0.6px solid #000;
			border-bottom:0.6px solid #000;
			width: 100%;
			border-collapse: collapse;
		}
		th,td{padding: 5px;}
		th{text-align: center; font-weight: bold;}
		.kanan{float: right;}
		.kiri{float: left;}
		.half{width: 50%;padding:5px 10px 0px 0px;flex:1;position: relative;}
		@page {size: A4;padding-right:10px;size: landscape;}
	    @media print {
	        .page {
	            border: initial;
	            border-radius: initial;
	            width: initial;
	            min-height: initial;
	            box-shadow: initial;
	            background: initial;
	            page-break-after: always;
	            page-break-before:always;
	        }
	    }
		.label{width:25%;display: inline-block;}
		.txt{width: 73%;display:inline-block;}
		.label,.txt{padding: 5px 0px}
		/*header*/
		header>.top{width: 100%;font-weight: bold}
		.top>p{margin-top: 10px}
		.group{
		    position: absolute;
		    bottom: 0;
		    width: 100%;
		}
		/*content*/
	    .head>th{border-bottom:double #000;}
		.footer{}
		
		#pageFooter:after {
		counter-increment: page;
		content: counter(page);
	</style>
</head>
<body>

	<header>
		<div class="top">
			<center><h1>LAPORAN REKAP SPEEDRESTO</h1></center>
		</div>
	</header>
	<content>
	
		<table border='1'>
			<thead>
				<tr>
					<th style="width:10%">No</th> <!-- no -->
					<th style="width:10%">Id Tr</th> <!-- table _ikm field desa_ -->
					<th style="width:10%">Meja</th> <!-- table _ikm field desa_ -->
					<th style="width:10%">Tanggal</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Nama Waiter</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Total Tr</th> <!-- table _ikm field telp_ -->
					<th style="width:10%">Fungsi DB</th>
					<th style="width:10%">Total Salah</th>
					<th style="width:10%">Qty Benar</th>

				</tr>
			</thead>
			<tbody>
				
			<?php
				
				$con="";

				$stmt = $conn->prepare("SELECT a.no_table, a.id_transaction,b.name, a.total_bill, DATE_FORMAT( a.date_order, '%d %M %Y %H:%i' ) AS tgl FROM transaction a INNER JOIN users b ON a.id_user=b.id_user WHERE a.UserID = ? ORDER BY a.id_transaction DESC");
		        $stmt->bind_param("s", $userID);
		        $result_data["transaction"] = array();
		        $result_data["error"] = FALSE;
		        $result_data["num_rows"] = 0;

		        if ($stmt->execute()) {
		            $result = $stmt->get_result();
		            $result_data["num_rows"] = $result->num_rows;

		            while ($row = $result->fetch_assoc()) {
		            	$totalDet=0;
		            	$QtyDet=0;
		            	$HargaDet=0;
		            	$stmtDet = $conn->prepare("SELECT a.qty,b.HargaJual FROM transaction_detail a INNER JOIN menu b ON a.id=b.id WHERE a.id_transaction = ?");
		            	$stmtDet->bind_param("s", $row["id_transaction"]);
		            	if ($stmtDet->execute()) {
		            		$resultDet = $stmtDet->get_result();
		            		while ($rowDet = $resultDet->fetch_assoc()) {
		            			$totalItem = 0;
		            			$totalItem = $rowDet["qty"] * $rowDet["HargaJual"];

		            			$totalDet=$totalDet+$totalItem;
		            			$QtyDet=$QtyDet+$rowDet["qty"];
		            			$HargaDet=$HargaDet+$rowDet["HargaJual"];
		            		}
		            	}

		            	if($totalDet > $row["total_bill"])
		            	{
		            		echo "<tr>
								<td>$no</td>
								<td>".$row["id_transaction"]."</td>
								<td>".$row["no_table"]."</td>
								<td>".$row["tgl"]."</td>
								<td>".$row["name"]."</td>
								<td>".$row["total_bill"]."</td>
								<td>".$QtyDet." x ".$HargaDet."</td>
								<td>".$totalDet."</td>
								<td>".($row["total_bill"] / $HargaDet)."</td>
							</tr>"; 
		            	}

		            	
						
		            }
		            $stmt->free_result();
		            $stmt->close();
		            
		                 
		        }
			?>
	
			</tbody>	
		</table>
		<div class="half kiri">
			Tanggal Cetak : <?php echo date('d-m-Y');?>
		</div>
		
	</content>
</body>
</html>
<script>
</script>