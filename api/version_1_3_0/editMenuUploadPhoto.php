<?php 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
function compress_image($source_url, $destination_url, $quality, $nama) {
	ini_set('memory_limit', '128M');
	include 'ImageResize.php';
	
	$info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source_url); 
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source_url); 
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source_url); 
        imagejpeg($image, $destination_url, $quality); 
    
	
	//use \Eventviva\ImageResize;
	//use \Eventviva\ImageResizeException;
	$imageR = new \Eventviva\ImageResize($destination_url);
	$imageR->resize(100, 100);
	$imageR->save(__DIR__.'/../storage/photo_menu_s/'.$nama);
	
	$imageR2 = new \Eventviva\ImageResize($destination_url);
	$imageR2->resize(300, 300);
	$imageR2->save(__DIR__.'/../storage/photo_menu_m/'.$nama);
	return $imageR2; 
} 
  
// json response array
$response = array("error" => FALSE);
if(isset($_FILES['image']['name']) && $_FILES['image']['size'] > 1048576)
{
	$response["error"] = TRUE;
    $response["error_msg"] = "Your image size more than 1MB";
    echo json_encode($response);
}
else
{
	$fileinfo = pathinfo($_FILES['image']['name']);
	//$name_photo = $_FILES['image']['name'];
	$name_photo = $_POST['name'];
	$extension = "jpg";
	$mask =__DIR__.'/../storage/photo_menu_s/'.$name_photo.'_*.*';
	$mask3 =__DIR__.'/../storage/photo_menu/'.$name_photo.'_*.*';
	$mask2 =__DIR__.'/../storage/photo_menu_m/'.$name_photo.'_*.*';
	array_map('unlink', glob($mask));
	array_map('unlink', glob($mask2));
	array_map('unlink', glob($mask3));
	$name_photo=$name_photo."_".date('Y-m-d_H-i-s');
	//$tmp = explode(".", $name_photo);
	$path_photo = $name_photo . "." . $extension;
	$filename = compress_image($_FILES["image"]["tmp_name"], __DIR__.'/../storage/photo_menu/'.$path_photo, 20, $path_photo);
	if ($filename) {
		$id_menu = $_POST['name'];
		$result = $db->updatePathPhotoMenu($id_menu, $path_photo);
	}
}

?>