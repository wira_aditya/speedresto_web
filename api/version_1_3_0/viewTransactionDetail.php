<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_transaction'])) {

    $id_transaction = $_POST['id_transaction'];

    $result = $db->viewTransactionDetail($id_transaction);

    if ($result["error"] == FALSE) {
        
        echo json_encode($result);
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>