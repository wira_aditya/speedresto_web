<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_customer']) && isset($_POST['id_menu']) && isset($_POST['qty']) && isset($_POST['note_order']) && isset($_POST['id_promo'])) {
 
    $id_customer = $_POST['id_customer'];
    $id_menu = $_POST['id_menu'];
    $qty = $_POST['qty'];
    $note_order = $_POST['note_order'];
    $id_promo = $_POST['id_promo'];
    if (isset($_POST["isFromNotif"])) {
        $isFromNotif = "1";
    } else {
        $isFromNotif = "0";
    }

    $result = $db->saveCart($id_customer, $id_menu, $qty, $note_order, $isFromNotif, $id_promo);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while saving cart!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>