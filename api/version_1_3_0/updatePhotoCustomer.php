<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
function compress_image($source_url, $destination_url, $quality, $nama) {

    ini_set('memory_limit', '128M');
    include 'ImageResize.php';

    $info = getimagesize($source_url);
    if ($info['mime'] == 'image/jpeg') 
        $image = imagecreatefromjpeg($source_url); 
    elseif ($info['mime'] == 'image/gif') 
        $image = imagecreatefromgif($source_url); 
    elseif ($info['mime'] == 'image/png') 
        $image = imagecreatefrompng($source_url); 
        imagejpeg($image, $destination_url, $quality); 
        

        //use \Eventviva\ImageResize;
    //use \Eventviva\ImageResizeException;
    $imageR = new \Eventviva\ImageResize($destination_url);
    $imageR->resize(100, 100);
    $imageR->save(__DIR__.'/../storage/photo_customer_s/'.$nama);
    
    $imageR2 = new \Eventviva\ImageResize($destination_url);
    $imageR2->resize(500, 500);
    $imageR2->save(__DIR__.'/../storage/photo_customer_m/'.$nama);
    
    return $imageR2; 
} 
 
// json response array
$response = array("error" => FALSE);
if(isset($_FILES['image']['name']) && $_FILES['image']['size'] > 1048576)
{
	$response["error"] = TRUE;
    $response["error_msg"] = "Your image size more than 1MB";
    echo json_encode($response);
}
else if(isset($_FILES['image']['name'])) {
    $name_photo = $_FILES['image']['name'];
    // $uidUser = $_POST['name'];


        $id_cust = $_POST['name'];
        $fileinfo = pathinfo($_FILES['image']['name']);
        $extension = "jpg";
        $path_photo = $id_cust . "." . $extension; 
        $filename = compress_image($_FILES["image"]["tmp_name"], __DIR__.'/../storage/photo_customer/'.$path_photo, 20, $path_photo);

        // $upload_path = '../storage/photo_customer/';
        // $file_path = $upload_path . $id_cust . "." . $extension;
        // move_uploaded_file($_FILES['image']['tmp_name'], $file_path);

        $user = $db->updateCustomer3($id_cust, $path_photo);
        if ($user) {
            // user stored successfully
            $response["error"] = FALSE;
            echo json_encode($response);
        } else {
            // user failed to store
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknown error occurred in update user!";
            echo json_encode($response);
        }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters path photo is missing!";
    echo json_encode($response);
}
?>