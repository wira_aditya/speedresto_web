<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array 
$response = array("error" => FALSE);
 
if (isset($_POST['idUser']) && isset($_POST['noTable']) && isset($_POST['totalBill']) && isset($_POST['tax']) && isset($_POST['service']) && isset($_POST['idResto']) && isset($_POST['kode']) && isset($_POST['id_menu']) && isset($_POST['qty_menu']) && isset($_POST['note_menu']) && isset($_POST['id_promo']) && isset($_POST['price_menu'])) {
 
    // receiving the post params
    $id_user = $_POST['idUser'];
    $no_table = $_POST['noTable'];
    $total_bill = $_POST['totalBill'];
    $tax = $_POST['tax'];
	$service = $_POST['service'];
    $status = "U"; //unpaid / belum dibayar
	$id_resto = $_POST['idResto'];
    $code = $_POST['kode'];
    $id_menus=$_POST['id_menu'];
    $qty_menus=$_POST['qty_menu'];
    $note_menus=$_POST['note_menu'];
    $id_promos=$_POST['id_promo'];
    $price_menus=$_POST['price_menu'];
    $user = $db->storeTransaction($id_user, $no_table, $total_bill, $tax, $service, $status, $id_resto, $code, $id_menus, $qty_menus, $note_menus, $id_promos, $price_menus);
    if ($user) {
        // user stored successfully
        echo json_encode($user);
    } else {
        // user failed to store
        $response["error"] = TRUE;
        $response["error_msg"] = "Error Network occurred in transaction!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (user, table number, tax, service, discount or total bill) is missing!";
    echo json_encode($response);
}


?>
