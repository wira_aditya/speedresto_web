<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['status_delivery']) && isset($_POST['id_booking'])) {
    // receiving the post params
    $status_delivery = $_POST['status_delivery'];
    $id_booking = $_POST['id_booking'];
 
    $result = $db->updateStatusDeliveryBooking($status_delivery, $id_booking);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while update status delivery booking!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>