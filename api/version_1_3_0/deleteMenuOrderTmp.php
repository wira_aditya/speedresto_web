<?php
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
  
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['id_user']) && isset($_POST['id_menu']) && isset($_POST['kode'])) {
 
    // receiving the post params
    $id_user = $_POST['id_user'];
    $id_menu = $_POST['id_menu'];
    $code = $_POST['kode'];
 
    $result = $db->deleteMenuOrderTmp($id_user, $id_menu, $code);
    if($result) {
        $response["status"] = "success";
        echo json_encode($response);
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error while deleted booking!";
        echo json_encode($response);
    }
} else {
    // required post params is missing
    $response["error"] = TRUE;
    $response["error_msg"] = "params is missing!";
    echo json_encode($response);
}
?>