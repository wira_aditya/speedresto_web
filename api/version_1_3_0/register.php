<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['gender'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $gender = $_POST['gender'];

    // check if user is already existed with the same email
    if ($db->isUserExistedUsers($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "User already existed with " . $email;
        echo json_encode($response);
    } else {
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "User already existed with " . $email;
            echo json_encode($response);
        } else {

           if ($db->isUserExistedCustomer($email)) {
                // user already existed
                $response["error"] = TRUE;
                $response["error_msg"] = "User already existed with " . $email;
                echo json_encode($response);
            }
            else{
                if ($db->isUserExistedCashier($email)) {
                    // user already existed
                    $response["error"] = TRUE;
                    $response["error_msg"] = "User already existed with " . $email;
                    echo json_encode($response);
                }
                else {
                    // create a new user
                    $user = $db->storeUser($name, $email, $password, $gender);
                    if ($user) {
                        // user stored successfully
                        $response["error"] = FALSE;
                        $response["user"]["id_user"] = $user["id_user"];
                        $response["user"]["name"] = $user["name"];
                        $response["user"]["email"] = $user["email"];
                        $response["user"]["created_at"] = $user["created_at"];
                        $response["user"]["updated_at"] = $user["updated_at"];
                        $response["user"]["gender"] = $user["gender"];
                        $response["user"]["path_photo"] = $user["path_photo"];
                        $response["user"]["id_restaurant"] = $user["UserID"];
                        $response["user"]["name_restaurant"] = $user["name_resto"];
                        $response["user"]["address"] = $user["address"];
                        echo json_encode($response);
                    } else {
                        // user failed to store
                        $response["error"] = TRUE;
                        $response["error_msg"] = "Unknown error occurred in registration!";
                        echo json_encode($response);
                    }
                }
            }
        }
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (name, email, password or gender) is missing!";
    echo json_encode($response);
}
?>