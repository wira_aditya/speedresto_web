<?php
if(isset($_GET['m']) && $_GET['m']!="")
{
	require_once 'include/cone.php';
	
	
	$i=	mysqli_real_escape_string($con, $_GET['m']); 
	
	$stmt = $con->prepare("SELECT TIME_FORMAT(a.waktu_mulai_promo, '%H:%i') as waktu_mulai_promo, TIME_FORMAT(a.waktu_akhir_promo, '%H:%i') as waktu_akhir_promo, a.UserID, a.Keterangan, a.HargaJual, DATE_FORMAT( a.tgl_mulai_promo, '%d %M' ) AS tgl_mulai, DATE_FORMAT( a.tgl_akhir_promo, '%d %M %Y' ) AS tgl, a.harga_promo,a.Gambar, b.NamaPerusahaan FROM menu a inner join restaurant b on a.UserID = b.UserID WHERE a.id=?");
    
	$stmt->bind_param("s", $i);
    $result = $stmt->execute();
    $jumlah = "";
    $text_jumlah="";

    // check for successful store
    if ($result) {
        $data = $stmt->get_result()->fetch_assoc();
		
		$cafe=$data['NamaPerusahaan'];
		$menu=$data['Keterangan'];
		$harga=$data['HargaJual'];
		$promo=$data['harga_promo'];
		$mulai=$data['tgl_mulai'];
		$tgl=$data['tgl'];
		$jam_mulai=$data['waktu_mulai_promo'];
		$jam_akhir=$data['waktu_akhir_promo'];
		$gambar=$data['Gambar'];
		$id=$data['UserID'];
	}
	else{
        $cafe="";
		$menu="";
		$harga=0;
		$promo=0;
		$mulai="";
		$tgl="";
		$jam_mulai="";
		$jam_akhir="";
		$gambar="";
		$id="0";
    }	
	$stmt->close();	

	$harga_lama = number_format($harga,0,",",".");
	$harga_baru = number_format($promo,0,",",".");
	?>

<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Speed Resto Promo (<?php echo strtoupper($cafe);?>)</title>
	
	<meta property="og:title" content="<?php echo strtoupper($cafe);?>" />
	<meta property="og:type" content="article" />
	<meta property="og:image" content="<?php echo $server;?>/storage/photo_menu_m/<?php echo $gambar;?>">
	<meta property="og:url" content="<?php echo $server;?>menu.php?m=<?php echo $i;?>" />

	<meta property="og:description" content="<?php echo $menu;?> (<?php echo $harga_lama;?>) now <?php echo $harga_baru;?>, <?php echo $mulai;?> until <?php echo $tgl." (".$jam_mulai." - ".$jam_akhir.$text_jumlah.")";?>" />
	<link rel="stylesheet" href="include/css.css">
</head>
	<body>

		<div>
			<p><img src="../storage/photo_menu_m/<?php echo $gambar;?>" width="100" height="100" style="float:left; margin:0 8px 4px 0;" />Go to <?php echo $cafe;?>. 
			<b><?php echo $menu;?></b> 
			discount <strike>Rp. <?php echo $harga_lama;?></strike> to <b>Rp. <?php echo $harga_baru;?></b> 
			from <b><?php echo $mulai;?></b> until <b><?php echo $tgl;?> (<?php echo $jam_mulai." - ".$jam_akhir.$text_jumlah;?>)</b></p>
			<p></p>
			<p></p>
			<p></p>
		</div>
		<div class="bggrid" align="center" >
			<!-- <h3>Other menu from <?php echo strtoupper($cafe);?></h3> -->
			<h4>Download Now !!! <br> (Speed Resto Apps)</h4>
			
			<a href="https://play.google.com/store/apps/details?id=net.bamboomedia.speedresto&hl=in"><img src="include/playstore.png" width="141" height="42" /></a>

			<a href="https://itunes.apple.com/us/app/speed-resto/id1289141799?ls=1&mt=8"><img src="include/appstore.png" width="141" height="42" /></a> 
			
			<div class="container">
			<div class="row">
				<?php
					$stmtMenu = $con->prepare("SELECT a.qty_promo, a.Keterangan, a.HargaJual, a.Note, a.harga_promo, a.status_promo, DATE_FORMAT( a.tgl_akhir_promo, '%d %M %Y' ) AS tgl, a.Gambar FROM menu a WHERE a.aktif='1' AND a.QtyStock='1' AND a.UserID=?");
					$stmtMenu->bind_param("s", $id);

					if ($stmtMenu->execute()) {   
		            $resultMenu = $stmtMenu->get_result();
		             
		            while ($row = $resultMenu->fetch_assoc()) {
		            ?>
		            	<a class="col-xs-12 mt10">
							<div class="card">
								<div class="card-article row">
									<p><img src="../storage/photo_menu_m/<?php echo $row["Gambar"];?>" width="150" height="150" />
									<br><b><?php echo $row["Keterangan"];?></b>  
									<br>
									<?php 
									if ($row["status_promo"]=="1")
									{
										$qtyP="";
										if($row["qty_promo"] != "")
										{
											$qtyP=" (sisa ".$jumlah.")";
										}
										?><strike>
											<?php echo number_format($row["HargaJual"],0,",",".");?></strike> <font color='red'><b><?php echo number_format($row["harga_promo"],0,",",".").$qtyP;?></b></font>
										<br>
										<?php echo "<font color='red'><b>".$row["tgl"]."</b></font>";
									}
									else
									{
										echo number_format($row["HargaJual"],0,",",".");
										?>
										<br>
										<br>
										<?php
									}
									?>
										
									</p>					
								</div>
							</div>
						</a>
					<?php
		            }

		                   
		        } else {
		            echo "menu not found";            
		        }
				?>
				
			</div>
		</div>
		</div>
	</body>
</html>	
	
<?php
}
?>
	


