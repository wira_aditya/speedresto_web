<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['name']) && isset($_POST['email']) && isset($_POST['password']) && isset($_POST['id_resto'])) {
 
    // receiving the post params
    $name = $_POST['name'];
    $email = $_POST['email'];
    $password = $_POST['password'];
    $id_resto = $_POST['id_resto'];

    // check if user is already existed with the same email
    if ($db->isUserExistedUsers($email)) {
        // user already existed
        $response["error"] = TRUE;
        $response["error_msg"] = "User already existed with " . $email;
        echo json_encode($response);
    } else {
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "User already existed with " . $email;
            echo json_encode($response);
        } else {

           if ($db->isUserExistedCustomer($email)) {
                // user already existed
                $response["error"] = TRUE;
                $response["error_msg"] = "User already existed with " . $email;
                echo json_encode($response);
            }
            else{
                if ($db->isUserExistedCashier($email)) {
                    // user already existed
                    $response["error"] = TRUE;
                    $response["error_msg"] = "User already existed with " . $email;
                    echo json_encode($response);
                }
                else {
                    // create a new user
                    $user = $db->storeCashier($name, $email, $password, $id_resto);
                    if ($user) {
                        // user stored successfully
                        $response["error"] = FALSE;
                        echo json_encode($response);
                    } else {
                        // user failed to store
                        $response["error"] = TRUE;
                        $response["error_msg"] = "Unknown error occurred in registration!";
                        echo json_encode($response);
                    }
                }
            }
        }
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters is missing!";
    echo json_encode($response);
}
?>