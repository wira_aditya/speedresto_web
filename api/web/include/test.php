<?php
 
require_once 'include/DB_Functions.php';
$db = new DB_Functions();
 
// json response array
$response = array("error" => FALSE);
 
if (isset($_POST['email']) && isset($_POST['id_resto'])) {
    
    $id_resto = $_POST['id_resto'];
    $email = $_POST['email'];
    $info = $db->getDisc($email, $id_resto);

    if ($info) {
        // info already existed
        $response["error"] = FALSE;
        $response["info"]["total_customers"] = $info["total_customers"];
        $response["info"]["disc"] = $info["disc_order"];
        $response["info"]["tax"] = $info["tax"];
        $response["info"]["service"] = $info["service"];
        echo json_encode($response); 
    
    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Unknow error!";
        echo json_encode($response);
    }
} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters resto is missing!";
    echo json_encode($response);
}
?>