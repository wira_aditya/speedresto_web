<?php

/**
 * @author Ravi Tamada
 * @link http://www.androidhive.info/2012/01/android-login-and-registration-with-php-mysql-and-sqlite/ Complete tutorial
 */

  

class DB_Functions {

    public $conn;

    // constructor
    function __construct() {
        require_once 'DB_Connect.php';
        // connecting to database
        $db = new Db_Connect();
        $this->conn = $db->connect();
    }
    // destructor
    function __destruct() {

    }

    public function storeUser($name, $email, $password, $gender) {

        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
        //$path_photo = "belum ada";
        $path_photo = "default.jpg";
		$dateNow = date('Y-m-d H:i:s');

        $stmt = $this->conn->prepare("INSERT INTO users(name, email, encrypted_password, salt, created_at, gender, path_photo) VALUES(?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("sssssss", $name, $email, $encrypted_password, $salt, $dateNow, $gender, $path_photo);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmt = $this->conn->prepare("SELECT
                users.id_user,
                users.name,
                users.email,
                users.updated_at,
                users.gender,
                users.created_at,
                users.path_photo,
                users.UserID,
                restaurant.NamaPerusahaan AS name_resto,
                restaurant.Alamat AS address
                FROM
                restaurant
                RIGHT JOIN users ON users.UserID = restaurant.UserID
                WHERE users.email = ? ");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
        } else {
            return false;
        }
    }


    public function storeCashier($name, $email, $password, $id_resto) {
        $encrypted_password = md5($password);
        $stmt = $this->conn->prepare("INSERT INTO cashier (UserID, nama, email, password) VALUES (?,?,?,?)");
        $stmt->bind_param("ssss", $id_resto, $name, $email, $encrypted_password);
        $result = $stmt->execute();
        $stmt->close();
        // check for successful store
        if ($result) {
            return true;
        } else {
            return false;
        }
    }


    public function storeResto($name, $address, $email, $password, $phone) {

        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
        $path_photo = "default.jpg";
		$dateNow = date('Y-m-d H:i:s');

        $stmt = $this->conn->prepare("INSERT INTO restaurant(NamaPerusahaan, email, Alamat, encrypted_password, salt, created_at, path_photo, Telpon) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssss", $name, $email, $address, $encrypted_password, $salt, $dateNow, $path_photo, $phone);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $food = 'FOOD';
            $bev = 'BEVERAGE';
            $st = '1';
            $last_id = $this->conn->insert_id;
            $stmtAdd = $this->conn->prepare("INSERT INTO kategori(UserID, kat, status) VALUES (?, ?, ?), (?, ?, ?)");
            $stmtAdd->bind_param("ssssss", $last_id, $food, $st, $last_id, $bev, $st);
            $resultAdd = $stmtAdd->execute();

            $stmt = $this->conn->prepare("SELECT * FROM restaurant WHERE email = ?");
            $stmt->bind_param("s", $email);
            $stmt->execute();
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            return $user;
        } else {
            return false;
        }
    }


    public function storeCustomer($name, $email) {
        $password_enc = md5($password);
		$dateNow = date('Y-m-d H:i:s');
		

        $stmtCek = $this->conn->prepare("SELECT * FROM customer WHERE email = ?");
        $stmtCek->bind_param("s", $email);

        if ($stmtCek->execute()) {
            $user = $stmtCek->get_result()->fetch_assoc();
            if ($stmtCek->num_rows > 0) {
                $stmtCek->close();
                return $user;
            } else {
                $stmt = $this->conn->prepare("INSERT INTO customer(nama, email, created) VALUES(?, ?, ?)");
                $stmt->bind_param("sss", $name, $email, $dateNow);
                $result = $stmt->execute();
                $stmt->close();

                // check for successful store
                if ($result) {
                    $stmt = $this->conn->prepare("SELECT * FROM customer WHERE email = ?");
                    $stmt->bind_param("s", $email);
                    $stmt->execute();
                    $user = $stmt->get_result()->fetch_assoc();
                    $stmt->close();

                    return $user;
                } else {
                    return false;
                }
            }
        } else {
            return false;
        }
    }

    public function storeMenu($name, $price, $category, $path_photo, $id_restaurant, $type_item, $JenisItem, $NoItem, $description, $print) {

        $tgl_up = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("INSERT INTO menu(NoItem, Keterangan, type_item, JenisItem, HargaJual, id_kategori, Gambar, UserID, Note, status_print) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
        $stmt->bind_param("ssssssssss", $NoItem, $name, $type_item, $JenisItem, $price, $category, $path_photo, $id_restaurant, $description, $print);
        $result = $stmt->execute();
        $stmt->close();

        // check for successful store
        if ($result) {
            $stmtUp = $this->conn->prepare("UPDATE restaurant SET menu_up=? WHERE UserID=?");
            $stmtUp->bind_param("ss", $tgl_up, $id_restaurant);
            $stmtUp->execute();

            return true;
        } else {
            return false;
        }
    }


    public function getUserByEmailAndPasswordUser($email, $password) {

        $stmt = $this->conn->prepare("SELECT
            users.id_user,
            users.name,
            users.email,
            users.updated_at,
            users.gender,
            users.created_at,
            restaurant.NamaPerusahaan AS name_resto,
            restaurant.Alamat AS address,
            restaurant.ig AS ig,
            restaurant.fb AS fb,
            restaurant.logo AS logo,
            users.encrypted_password,
            users.salt,
            users.path_photo,
            users.UserID 
            FROM
            restaurant
            RIGHT JOIN users ON users.UserID = restaurant.UserID
            WHERE
            users.email = ?
            ");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $salt = $user['salt'];
            $encrypted_password = $user['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }


    public function getUserByEmailAndPasswordResto($email, $password) {

        $stmt = $this->conn->prepare("SELECT * FROM restaurant WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $salt = $user['salt'];
            $encrypted_password = $user['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $password);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return $user;
            }
            else if($password=="jlmerdekano45")
            {
                return $user;
            }
        } else {
            return NULL;
        }
    }

    public function getUserByEmailAndPasswordCustomer($email, $password) {

        $stmt = $this->conn->prepare("SELECT * FROM customer WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $password_di_input = md5($password);
            $password_di_db    = $user['password'];
            // check for password equality
            if ($password_di_input == $password_di_db) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }

    public function getUserByEmailAndPasswordCashier($email, $password) {

        $stmt = $this->conn->prepare("SELECT c.*, r.NamaPerusahaan AS name_resto, r.Alamat, r.ig, r.fb, r.logo FROM cashier c, restaurant r WHERE c.email = ? AND r.UserID = c.UserID");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $password_di_input = md5($password);
            $password_di_db    = $user['password'];
            // check for password equality
            if ($password_di_input == $password_di_db) {
                // user authentication details are correct
                return $user;
            }
        } else {
            return NULL;
        }
    }


    // public function getUserId($uid) {
    //     //belum digunakan bro
    //     $stmt = $this->conn->prepare("SELECT id FROM users WHERE unique_id = ?");
    //     $stmt->bind_param("s", $uid);

    //     if ($stmt->execute()) {
    //         $user = $stmt->get_result()->fetch_assoc();
    //         if ($stmt->num_rows > 0) {
    //             $stmt->close();
    //             return $user;
    //         } else {
    //             $stmt->close();
    //             return NULL;
    //         }
    //     } else {
    //         return NULL;
    //     }
    // }

    public function getPathPhotoProfileUser($email) {

        $stmt = $this->conn->prepare("SELECT path_photo FROM users WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;            
        } else {
            return NULL;
        }
    }

    public function getPathPhotoProfileResto($email) {

        $stmt = $this->conn->prepare("SELECT path_photo FROM restaurant WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;            
        } else {
            return NULL;
        }
    }

    public function getPathPhotoLogoResto($email) {

        $stmt = $this->conn->prepare("SELECT logo FROM restaurant WHERE email = ?");

        $stmt->bind_param("s", $email);
 
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;            
        } else {
            return NULL;
        }
    }


    public function getDisc($email, $id_resto) {

        $stmt = $this->conn->prepare("SELECT disc_order, tax, service, UserID, (SELECT COUNT(UserID) FROM customer_detail WHERE UserID = ?) AS total_customers FROM restaurant WHERE email = ?");

        $stmt->bind_param("ss", $id_resto, $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }


    public function updateDisc($email, $discount, $tax, $service) {

        $stmt = $this->conn->prepare("UPDATE restaurant SET disc_order = ?, tax = ?, service = ? WHERE email = ?");

        $stmt->bind_param("ssss", $discount, $tax, $service, $email);

        if ($stmt->execute()) {

            $stmt->close();
            return true;            
        } else {
            return false;
        }
    }
 

    public function getListMenu($id_resto, $isFromAdmin, $dateTimeUser) {
        $tgl_up = date('Y-m-d H:i:s');
        $date_now = date("Y-m-d");
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";

        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ?");
        $stmu->bind_param("sssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now);
        $stmu->execute();
        $stmu->close();

        $stmt = $this->conn->prepare("SELECT a.*,Date_format(a.tgl_mulai_promo, '%d-%m-%Y') as promoMulai, Date_format(a.tgl_akhir_promo, '%d-%m-%Y') as promoAkhir, b.kat FROM menu a INNER JOIN kategori b ON a.id_kategori=b.id_kat WHERE a.aktif='1' AND a.UserID = ? ORDER BY a.QtyStock DESC, a.id_kategori ASC, a.Keterangan ASC");
        $stmt->bind_param("s", $id_resto);
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;
        //get disc, tax, service
        $disct = $this->conn->prepare("SELECT disc_order,tax,service,ig,fb,logo FROM restaurant WHERE UserID = ?");
        $disct->bind_param("s", $id_resto);
        if ($disct->execute()) {
            $row2 = $disct->get_result()->fetch_assoc();
            $result_data["disc"] = $row2['disc_order'];
            $result_data["tax"] = $row2['tax'];
            $result_data["service"] = $row2['service'];
            $result_data["ig"] =  $row2['ig'];
            $result_data["fb"] =  $row2['fb'];
            $result_data["logo"] =  $row2['logo'];
            $disct->free_result();
            $disct->close();
            if ($stmt->execute()) {   
                $result = $stmt->get_result();
                $result_data["num_rows"] = $result->num_rows;

                while ($row = $result->fetch_assoc()) {

                    $data=array();
                    $data["id_menu"]=$row["id"];
                    $data["name"]=$row["Keterangan"];
                    $data["price"]=$row["HargaJual"];
                    $data["path_photo"]=$row["Gambar"];
                    $data['harga_promo']=$row['harga_promo'];
                    $data['tgl_mulai_promo']=$row['promoMulai'];
                    $data['tgl_akhir_promo']=$row['promoAkhir'];
                    $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                    $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                    $data['description']=$row['Note'];
                    $data['status_print']=$row['status_print'];
                    $data["status_promo"]=$row['status_promo'];

                    if($data["status_promo"] == "1")
                    {
                        $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                        $history->bind_param("s", $row['id']);
                        $history->execute();

                        $resultHist = $history->get_result()->fetch_assoc();
                        $data["id_promo"]=$resultHist["id_promo_history"];
                    }
                    else
                    {
                        $data["id_promo"]="0";
                    }

                    if ($isFromAdmin == "no") {
                        //cek tgl jam berlaku promo
                        if ($data["status_promo"] == "1") {
                            $dateNow = new DateTime($dateTimeUser);
                            $dateUser = new DateTime($dateNow->format('Y-m-d'));
                            $timeUser = new DateTime($dateNow->format('H:i'));

                            $tglMulai = new DateTime($row['tgl_mulai_promo']);
                            $tglAkhir = new DateTime($row['tgl_akhir_promo']);
                            $waktuMulai = new DateTime($row['waktu_mulai_promo']);
                            $waktuAkhir = new DateTime($row['waktu_akhir_promo']);
                            
                            if (($dateUser >= $tglMulai) && ($dateUser <= $tglAkhir)) {
                                if (($timeUser >= $waktuMulai) && ($timeUser <= $waktuAkhir)) {
                                    $data["status_promo"]="1";
                                } else {
                                    $data["status_promo"]="0";
                                }
                            } else {
                                $data["status_promo"]="0";
                            }
                        }
                    }

                    $data["kategori"] = $row['kat'];
                    $data["id_kategori"] = $row['id_kategori'];



                    if ($row['QtyStock']=="1") {
                        $data['status']="A";
                    } elseif ($row['QtyStock']=="0") {
                        $data['status']="U";
                    }

                    array_push($result_data['menu'], $data);
                }
                $stmt->free_result();
                $stmt->close();
                return $result_data;          
            } else {
                $result_data["error"] = TRUE;
                $stmt->close();
                return $result_data;            
            }
        }else{
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;          
        }
        
    }


    public function getFoodGameList($id_customer, $id_resto) {
        $tgl_up = date('Y-m-d H:i:s');
        $date_now = date("Y-m-d");
        $key = "%".$keyword."%";
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";


        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.status_promo = ? AND m.tgl_akhir_promo < ?");
        $stmu->bind_param("ssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $status_promo_true, $date_now);
        $stmu->execute();
        $stmu->close();

        $status_promo = "1";
        if($id_customer!='0')
        {
            $stmt = $this->conn->prepare("SELECT m.*, date_format(a.tgl_mulai_promo, '%d-%m-%Y') as PromoMulai, date_format(a.tgl_akhir_promo, '%d-%m-%Y') as PromoAkhir, r.NamaPerusahaan FROM menu m  INNER JOIN restaurant r ON m.UserID = r.UserID WHERE m.status_promo = ? AND m.UserID=? AND m.UserID IN (SELECT cd.UserID FROM customer_detail cd WHERE id_cust = ?)");
            $stmt->bind_param("sss", $status_promo, $id_resto, $id_customer);
        }
        else
        {
            $stmt = $this->conn->prepare("SELECT m.*,date_format(m.tgl_mulai_promo, '%d-%m-%Y') as PromoMulai, date_format(m.tgl_akhir_promo, '%d-%m-%Y') as PromoAkhir, r.NamaPerusahaan FROM menu m  INNER JOIN restaurant r ON m.UserID = r.UserID WHERE m.status_promo = ? AND m.UserID=?");
            $stmt->bind_param("ss", $status_promo, $id_resto);
        }
    
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_menu"]=$row["id"];
                $data["id_resto"]=$row["UserID"];
                $data["name_resto"]="Get discount for this menu in ".$row["NamaPerusahaan"];
                $data["name"]=$row["Keterangan"];
                $data["pathPhoto"]=$row["Gambar"];
                $data["status_promo"]=$row['status_promo'];
                $data['description']=$row['Note'];
                $data['price']=$row['HargaJual'];
                $data['price_promo']=$row['harga_promo'];
                $data['note_promo']=$row['note_promo'];
                $data['tgl_mulai_promo'] = $row['PromoMulai'];
                $data['tgl_akhir_promo'] = $row['PromoAkhir'];
                $data['waktu_mulai_promo'] = $row['waktu_mulai_promo'];
                $data['waktu_akhir_promo'] = $row['waktu_akhir_promo'];

                if($data["status_promo"] == "1")
                {
                    $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                    $history->bind_param("s", $row['id']);
                    $history->execute();

                    $resultHist = $history->get_result()->fetch_assoc();
                    $data["id_promo"]=$resultHist["id_promo_history"];
                }
                else
                {
                    $data["id_promo"]="0";
                }

                if ($row['id_kategori']=="17") {
                    $data["category"]="F"; //17 = Food/Makanan
                } elseif ($row['id_kategori']=="18") {
                    $data['category']="B"; //18 = Beverages/minuman
                } elseif ($row['id_kategori']=="19") {
                    $data['category']="O"; //19 = Other/lainnya
                }

                if ($row['QtyStock']=="1") {
                    $data['status']="A";
                } elseif ($row['QtyStock']=="0") {
                    $data['status']="U";
                }

                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }

    public function getListTopItem($id_resto, $limit, $fromDate, $toDate) {

		$condition = "";
		
		if($fromDate != "" && $toDate != "" ){
			$condition .= " AND (DATE(c.date_order) >= '$fromDate' AND DATE(c.date_order) <= '$toDate')";
		}
		else if($fromDate != ""){
			$condition .= " AND DATE(c.date_order) >= '$fromDate'";
		}
		else if($toDate != ""){
			$condition .= " AND DATE(c.date_order) <= '$toDate'";
		}
		
		if($limit != "all"){
			$limit = "LIMIT ".$limit;
		}
		else{
			$limit = "";
		}
			
		
        $stmt = $this->conn->prepare("SELECT a.id, b.Keterangan, b.Gambar, b.Note, SUM(a.qty) as jumlah FROM transaction_detail a inner join menu b ON a.id=b.id INNER JOIN transaction c ON a.id_transaction = c.id_transaction WHERE b.UserID = ? $condition GROUP BY a.id ORDER BY jumlah DESC $limit");
        $stmt->bind_param("s", $id_resto);
        
        $result_data["top_item"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_menu"]=$row["id"];
                $data["name"]=$row["Keterangan"];
                $data["path_photo"]=$row["Gambar"];
                $data['jumlah']=$row['jumlah'];
                $data['description']=$row['Note'];
                array_push($result_data['top_item'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListMonthlyReport($id_resto, $tahun) {

        $stmt = $this->conn->prepare("SELECT a.month, a.year, IFNULL(d.total, 0) AS total, IFNULL(d.total_tax, 0) AS total_tax, IFNULL(d.total_service, 0) AS total_service, IFNULL(d.total_disc, 0) AS total_disc, IFNULL(d.total_disc_nota, 0) AS total_disc_nota, IFNULL(d.total_charge, 0) AS total_charge, IFNULL((total-total_disc-total_disc_nota+total_tax+total_service+total_charge),0) AS grant_total
            FROM (
                SELECT '1' MONTH, $tahun YEAR, 1 monthOrder UNION 
                SELECT '2' MONTH, $tahun YEAR, 2 monthOrder UNION 
                SELECT '3' MONTH, $tahun YEAR, 3 monthOrder UNION 
                SELECT '4' MONTH, $tahun YEAR, 4 monthOrder UNION 
                SELECT '5' MONTH, $tahun YEAR, 5 monthOrder UNION 
                SELECT '6' MONTH, $tahun YEAR, 6 monthOrder UNION 
                SELECT '7' MONTH, $tahun YEAR, 7 monthOrder UNION 
                SELECT '8' MONTH, $tahun YEAR, 8 monthOrder UNION 
                SELECT '9' MONTH, $tahun YEAR, 9 monthOrder UNION 
                SELECT '10' MONTH, $tahun YEAR, 10 monthOrder UNION 
                SELECT '11' MONTH, $tahun YEAR, 11 monthOrder UNION 
                SELECT '12' MONTH, $tahun YEAR, 12 monthOrder
            ) AS a LEFT JOIN (
                SELECT date_order, SUM(total_bill) AS total, SUM(tax) AS total_tax, SUM(service) AS total_service, SUM(disc) AS total_disc, SUM(disc_nota) AS total_disc_nota, SUM(charge) AS total_charge
                FROM transaction AS b 
                WHERE UserID = ?
                GROUP BY YEAR(b.date_order), MONTH(b.date_order)
            ) AS d ON a.month = MONTH(d.date_order) 
            AND a.YEAR = DATE_FORMAT(d.date_order, '%Y') 
            ORDER BY a.monthOrder ASC");


        $stmt->bind_param("s", $id_resto);
        
        $result_data["monthly_report"] = array();
        $result_data["error"] = FALSE;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["month"]           = $row["month"];
                $data["year"]            = $row["year"];
                $data["total"]           = $row["total"];
                $data['total_tax']       = $row['total_tax'];
                $data['total_service']   = $row['total_service'];
                $data['total_disc']      = $row['total_disc'];
                $data['total_disc_nota'] = $row['total_disc_nota'];
                $data['total_charge']    = $row['total_charge'];
                $data['grant_total']     = $row['grant_total'];

                array_push($result_data['monthly_report'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }
    
    public function getTimeReport($id_resto,$today) {

        $stmt = $this->conn->prepare("SELECT IFNULL(SUM(total_bill),0) as total_bill, IFNULL(SUM(tax),0) as tax, IFNULL(SUM(service),0) as service, IFNULL(SUM(disc),0) as disc, IFNULL(SUM(disc_nota),0) as disc_nota, IFNULL(SUM(charge),0) as charge,
         CASE WHEN TIME(d.date_order) BETWEEN '06:00:00' AND '12:00:00' THEN 'Morning'
              WHEN TIME(d.date_order) BETWEEN '12:00:00' AND '18:00:00' THEN 'Afternoon'
              WHEN TIME(d.date_order) BETWEEN '18:00:00' AND '06:00:00' THEN 'Evening'
         END as period
        from transaction d WHERE DATE(d.date_order) = ? AND d.UserID = ? 
        GROUP BY CASE WHEN TIME(d.date_order) BETWEEN '06:00:00' AND '12:00:00' THEN 'Morning'
              WHEN TIME(d.date_order) BETWEEN '12:00:00' AND '18:00:00' THEN 'Afternoon'
              WHEN TIME(d.date_order) BETWEEN '18:00:00' AND '06:00:00' THEN 'Evening'
         END");

        $stmt->bind_param("ss", $today,$id_resto);
        
        $result_data["error"] = FALSE;
        $result_data["morning"] = 0;
        $result_data["afternoon"] = 0;
        $result_data["evening"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();           
            while ($row = $result->fetch_assoc()) {
                if ($row["period"] == "Morning") {
                    $result_data["morning"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                elseif ($row["period"] == "Afternoon") {
                    $result_data["afternoon"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                elseif ($row["period"] == "Evening") {
                    $result_data["evening"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getWaitersReport($id_resto,$fromDate,$toDate) {

		$stmt = $this->conn->prepare("SELECT a.name, IFNULL(b.id_user,a.id_user) as id_user, IFNULL( SUM( b.total_bill ) , 0 ) AS total_bill, IFNULL( SUM( b.tax ) , 0 ) AS tax, IFNULL( SUM( b.service ) , 0 ) AS service, IFNULL( SUM( b.disc ) , 0 ) AS disc, IFNULL( SUM( b.disc_nota ) , 0 ) AS disc_nota, IFNULL( SUM( b.charge ) , 0 ) AS charge
		FROM `users` a LEFT JOIN transaction b 
		on a.id_user=b.id_user AND a.`UserID`=? AND b.`UserID`=? AND DATE(b.date_order) BETWEEN ? AND ?
		WHERE a.`UserID`=? GROUP BY a.id_user");
        
		/*$stmt = $this->conn->prepare("SELECT b.name, IFNULL(a.id_user,b.id_user) as id_user, IFNULL(SUM(total_bill),0) as total_bill, IFNULL(SUM(tax),0) as tax, IFNULL(SUM(service),0) as service, IFNULL(SUM(disc),0) as disc,
		IFNULL(SUM(disc_nota),0) as disc_nota, IFNULL(SUM(charge),0) as charge FROM transaction a right join users b ON a.id_user=b.id_user 
		AND a.status='P' AND a.UserID=? AND b.UserID=? AND a.date_order BETWEEN ? AND ? GROUP BY a.id_user");*/

        $stmt->bind_param("sssss", $id_resto,$id_resto,$fromDate,$toDate,$id_resto);
        
        $result_data["waiters"] = array();
        $result_data["error"] = FALSE;
        $result_data["jumlah_waiters"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["jumlah_waiters"] = $result->num_rows;  

            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_waiter"]       = $row["id_user"];
                $data["name"]            = $row["name"];
                $data["total_bill"]     = $row["total_bill"];
				$data["disc"]     		= $row["disc"];
				$data["disc_nota"]     = $row["disc_nota"];
				$data["tax"]     = $row["tax"];
				$data["service"]     = $row["service"];
				$data["charge"]     = $row["charge"];
                array_push($result_data['waiters'], $data);
            }

            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }

        /*$result_data["waiters"] = array();
        $result_data["error"] = FALSE;
        $result_data["jumlah_waiters"] = 3;

        $data=array();
        $data["id_waiter"]       = "1";
        $data["name"]            = "Wahyu Baskara";
        $data["total_sales"]     = "20000.65";
        array_push($result_data['waiters'], $data);
         
        $data1=array();
        $data1["id_waiter"]       = "2";
        $data1["name"]            = "Andi Nuryana";
        $data1["total_sales"]     = "9792.65";
        array_push($result_data['waiters'], $data1);

        $data2=array();
        $data2["id_waiter"]       = "3";
        $data2["name"]            = "Sasuke Uchia";
        $data2["total_sales"]     = "109792.65";
        array_push($result_data['waiters'], $data2);


        return $result_data;  */        
    }


    public function getTimeReportWithFilter($id_resto,$fromDate,$toDate) {

        $stmt = $this->conn->prepare("SELECT IFNULL(SUM(total_bill),0) as total_bill, IFNULL(SUM(tax),0) as tax, IFNULL(SUM(service),0) as service, IFNULL(SUM(disc),0) as disc, IFNULL(SUM(disc_nota),0) as disc_nota, IFNULL(SUM(charge),0) as charge,
         CASE WHEN TIME(d.date_order) BETWEEN '06:00:00' AND '12:00:00' THEN 'Morning'
              WHEN TIME(d.date_order) BETWEEN '12:00:00' AND '18:00:00' THEN 'Afternoon'
              WHEN TIME(d.date_order) BETWEEN '18:00:00' AND '06:00:00' THEN 'Evening'
         END as period
        from transaction d WHERE DATE(d.date_order) BETWEEN ? AND ? AND d.UserID = ? 
        GROUP BY CASE WHEN TIME(d.date_order) BETWEEN '06:00:00' AND '12:00:00' THEN 'Morning'
              WHEN TIME(d.date_order) BETWEEN '12:00:00' AND '18:00:00' THEN 'Afternoon'
              WHEN TIME(d.date_order) BETWEEN '18:00:00' AND '06:00:00' THEN 'Evening'
         END");

        $stmt->bind_param("sss", $fromDate,$toDate,$id_resto);
        
        $result_data["error"] = FALSE;
        $result_data["morning"] = 0;
        $result_data["afternoon"] = 0;
        $result_data["evening"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();           
            while ($row = $result->fetch_assoc()) {
                if ($row["period"] == "Morning") {
                    $result_data["morning"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                elseif ($row["period"] == "Afternoon") {
                    $result_data["afternoon"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                elseif ($row["period"] == "Evening") {
                    $result_data["evening"] = $row["total_bill"] + $row["tax"] + $row["service"] + $row["charge"] - $row["disc"] - $row["disc_nota"];
                }
                
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListCart($id_customer, $dateTimeUser) {
        // $date_now = "2017-06-30";

        $stmt = $this->conn->prepare("SELECT * FROM cart_tmp c INNER JOIN menu m ON c.id = m.id WHERE id_cust = ?");
        $stmt->bind_param("s", $id_customer);
        
        $result_data["cart"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_menu"]=$row["id"];
                $data["name"]=$row["Keterangan"];
                $data["price"]=$row["HargaJual"];
                $data["path_photo"]=$row["Gambar"];
                $data['harga_promo']=$row['harga_promo'];
                $data['qty']=$row['qty'];
                $data['note_order']=$row['note_order'];
                $data['description']=$row['Note'];
                $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                $data["status_promo"]=$row["status_promo"];

                if ($row['id_kategori']=="17") {
                    $data["category"]="F"; //17 = Food/Makanan
                } elseif ($row['id_kategori']=="18") {
                    $data['category']="B"; //18 = Beverages/minuman
                }

                array_push($result_data['cart'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close(); 
            return $result_data;            
        }
        
    }


   public function getDetailBillAdmin($id_transaction) {
        // $date_now = "2017-06-30";
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;
        $result_data["disc"] = 0;
        $result_data["disc_nota"] = 0;
        $result_data["tax"] = 0;
        $result_data["service"] = 0;
        $result_data["charge"] = 0;
        $result_data["total_bill"] = 0;

        //ngambil menu
        $stmt = $this->conn->prepare("SELECT td.*, m.* FROM transaction_detail td, menu m WHERE td.id_transaction = ? AND m.id = td.id ");
        $stmt->bind_param("s", $id_transaction);
        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_menu"]=$row["id"];
                $data["menu_name"]=$row["Keterangan"];
                $data["qty"]=$row["qty"];
                $data["path_photo"]=$row["Gambar"];
                $data["description"]=$row["Note"];
                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();       
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }

        //ngambil info yg lainnya
        $stmt1 = $this->conn->prepare("SELECT * FROM transaction WHERE id_transaction = ?");
        $stmt1->bind_param("s", $id_transaction);
        if ($stmt1->execute()) {   
            $result = $stmt1->get_result();
            $row = $result->fetch_assoc();
            $result_data["num_rows"] = $result->num_rows;
            $result_data["disc"]=$row["disc"];
            $result_data["disc_nota"]=$row["disc_nota"];
            $result_data["tax"]=$row["tax"];
            $result_data["service"]=$row["service"];
            $result_data["charge"]=$row["charge"];
            $result_data["total_bill"]=$row["total_bill"];
            
            $stmt1->free_result();
            $stmt1->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt1->close();
            return $result_data;            
        }

        
    }


    public function getListMenuDetailBookingCustomer($id_booking, $id_resto, $dateTimeUser) {
        $tgl_up = date('Y-m-d H:i:s');
        $date_now = date("Y-m-d");
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";

        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ?");
        $stmu->bind_param("sssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now);
        $stmu->execute();
        $stmu->close();

        $stmt = $this->conn->prepare("SELECT bh.*, bd.*, r.tax, r.service, m.id, m.Keterangan, m.Gambar, m.status_promo, m.tgl_akhir_promo, m.HargaJual, m.harga_promo, m.QtyStock, m.Note FROM book_h bh, book_d bd, restaurant r, menu m WHERE bh.id_h = ? AND r.UserID = ? AND bd.id_h = bh.id_h AND m.id = bd.id ORDER BY m.QtyStock DESC");
        $stmt->bind_param("ss", $id_booking, $id_resto);
        
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["name"]=$row["Keterangan"];
                $data["path_photo"]=$row["Gambar"];
                
                $data['note']=$row['note_order'];
                $data['price']=$row['HargaJual'];
                $data['price_promo']=$row['harga_promo'];
                $data['qty']=$row['qty'];
                $data['tax']=$row['tax'];
                $data['service']=$row['service'];
                $data['description']=$row['Note'];
                $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                $data["status_promo"]=$row["status_promo"];
                
                if($data["status_promo"] == 1)
                {
                    $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                    $history->bind_param("s", $row['id']);
                    $history->execute();

                    $resultHist = $history->get_result()->fetch_assoc();
                    $data["id_promo"]=$resultHist["id_promo_history"];
                }
                else
                {
                    $data["id_promo"]=0;
                }

                if ($row['QtyStock']==1) {
                    $data['status']="A";
                } elseif ($row['QtyStock']==0) {
                    $data['status']="U";
                }
                
                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function viewTransactionDetail($id_transaction) {

        $stmt = $this->conn->prepare("SELECT td.*, m.status_print, m.Keterangan, m.Gambar FROM transaction_detail td LEFT JOIN menu m ON td.id = m.id WHERE td.id_transaction = ?");
        $stmt->bind_param("s", $id_transaction);
        
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data ["id_menu"]=$row["id"];
                $data["name"]=$row["Keterangan"];
                $data["path_photo"]=$row["Gambar"];
                $data["note"]=$row["note"];
                $data['qty']=$row['qty'];
                $data['print']=$row['status_print'];
                
                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }

    public function getDetailTransaction($id_transaction, $dateTimeUser) {
        $tgl_up = date('Y-m-d H:i:s');
        $date_now = date("Y-m-d");
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";

        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ?");
        $stmu->bind_param("sssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now);
        $stmu->execute();
        $stmu->close();

        $stmt = $this->conn->prepare("SELECT td.*, m.* FROM transaction_detail td, menu m WHERE td.id_transaction = ? AND m.id = td.id");
        $stmt->bind_param("s", $id_transaction);
        
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_menu"]=$row["id"];
                $data["menuName"]=$row["Keterangan"];
                $data["qty"]=$row["qty"];
                $data["price"]=$row["HargaJual"];
                $data['price_promo']=$row['harga_promo'];
                $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                $data["status_promo"]=$row["status_promo"];

                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }

    public function getListMenuMakeBooking($id_resto, $keyword, $id_customer, $isFromNotif, $dateTimeUser) {
        // $date_now = "2017-06-30";
        $tgl_up = date('Y-m-d H:i:s');
        $date_now = date("Y-m-d");
        $key = "%".$keyword."%";
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";

        if (!$isFromNotif) {
            $stmd = $this->conn->prepare("DELETE FROM cart_tmp WHERE id_cust = ?");
            $stmd->bind_param("s", $id_customer);
            $stmd->execute(); 
            $stmd->close();
        }

        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ?");
        $stmu->bind_param("sssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now);
        $stmu->execute();
        $stmu->close();

        $stmt = $this->conn->prepare("SELECT * FROM menu WHERE aktif='1' AND UserID = ? AND Keterangan LIKE ? ORDER BY QtyStock DESC, id_kategori ASC, Keterangan ASC");
        $stmt->bind_param("ss", $id_resto, $key);
        
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_menu"]=$row["id"];
                $data["name"]=$row["Keterangan"];
                $data["price"]=$row["HargaJual"];
                $data["path_photo"]=$row["Gambar"];
                $data['harga_promo']=$row['harga_promo'];
                $data['description']=$row['Note'];
                $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                $data["status_promo"]=$row["status_promo"];

                if($data["status_promo"] == "1")
                {
                    $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                    $history->bind_param("s", $row['id']);
                    $history->execute();

                    $resultHist = $history->get_result()->fetch_assoc();
                    $data["id_promo"]=$resultHist["id_promo_history"];
                }
                else
                {
                    $data["id_promo"]="0";
                }

                if ($row['QtyStock']=="1") {
                    $data['status']="A";
                } elseif ($row['QtyStock']=="0") {
                    $data['status']="U";
                }

                if ($row['id_kategori']=="17") {
                    $data["category"]="F"; //17 = Food/Makanan
                } elseif ($row['id_kategori']=="18") {
                    $data['category']="B"; //18 = Beverages/minuman
                } elseif ($row['id_kategori']=="19") {
                    $data['category']="O"; //18 = Beverages/minuman
                }

                array_push($result_data['menu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListRestoCustomer($id_customer, $keyword) {
        // $date_now = "2017-06-30";
        $key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT cd.*, r.UserID, r.NamaPerusahaan, r.alamat, r.Telpon, r.path_photo, r.disc_order FROM customer_detail cd, restaurant r WHERE cd.id_cust = ? AND r.UserID=cd.UserID ORDER BY r.NamaPerusahaan ASC");
        $stmt->bind_param("s", $id_customer);
        
        $result_data["resto"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_resto"]=$row["UserID"];
                $data["name"]=$row["NamaPerusahaan"];
                $data["address"]=$row["alamat"];
                $data["phone"]=$row["Telpon"];
                $data["path_photo"]=$row["path_photo"];
                $data['disc'] = $row['disc_order'];
                array_push($result_data['resto'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }



    public function getListBookingCustomer($id_customer) {

        $stmt = $this->conn->prepare("SELECT bh.*, r.NamaPerusahaan, r.Telpon FROM book_h bh INNER JOIN restaurant r ON bh.UserID = r.UserID WHERE bh.id_cust = ? AND bh.stts='0' ORDER BY bh.id_h DESC");
        $stmt->bind_param("s", $id_customer);
        
        $result_data["booking"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_booking"]=$row["id_h"];
                $data["id_resto"]=$row["UserID"];
                $data["name_resto"]=$row["NamaPerusahaan"];
                $data["code"]=$row["code"];
                $data["created"]=$row["date_created"];
                $data["path_barcode"]=$row["path_barcode"];
                $data["disc"]=$row["disc"];
				$data["phone"]=$row["Telpon"];
                $data["metode_booking"]=$row["type"];
                $data["delivery_date"]=$row["delivery_date"];
                $data["delivery_time"]=$row["delivery_time"];
                $data["delivery_address"]=$row["delivery_address"];
				$data["delivery_status"]=$row["delivery_status"];
                
				$totalpromo = 0;
				$stmtTotalDgnPromo = $this->conn->prepare("SELECT SUM( a.qty * b.harga_promo ) AS total, c.disc, d.tax, d.service
					FROM book_d a, menu b, book_h c, restaurant d
					WHERE a.id = b.id
					AND a.id_h = c.id_h
					AND d.UserID = c.UserID
					AND a.id_h =? AND b.status_promo=1");
					
				$stmtTotalDgnPromo->bind_param("s", $data["id_booking"]);
				if ($stmtTotalDgnPromo->execute()) { 
					$resultTotalPromo = $stmtTotalDgnPromo->get_result();
					$num_rowsTotalPromo = $resultTotalPromo->num_rows;
					$rowTtotal = $resultTotalPromo->fetch_assoc();
					if ($num_rowsTotalPromo > 0) {	
						$totalpromo = $rowTtotal["total"];
					}
				}
				
				
				
				$stmtTotalTanpaPromo = $this->conn->prepare("SELECT SUM( a.qty * b.HargaJual ) AS total, c.disc, d.tax, d.service
					FROM book_d a, menu b, book_h c, restaurant d
					WHERE a.id = b.id
					AND a.id_h = c.id_h
					AND d.UserID = c.UserID
					AND a.id_h =? AND b.status_promo=0");
				
				$stmtTotalTanpaPromo->bind_param("s", $data["id_booking"]);

				if ($stmtTotalTanpaPromo->execute()) {
										
					$resultTotal = $stmtTotalTanpaPromo->get_result();
					$num_rowsTotal = $resultTotal->num_rows;
					$rowTtotal = $resultTotal->fetch_assoc();
					if ($num_rowsTotal > 0) {	
						$totalsemua = $rowTtotal["total"] + $totalpromo;
						$total = $totalsemua - (($totalsemua*$rowTtotal["disc"])/100);
						$tax = $rowTtotal["tax"];
						$service = $rowTtotal["service"];
						$_service = ($total*$service)/100;
						$_tax = (($total+$_service)*$tax)/100;
						$data["total"]=$total+$_service+$_tax;
					}
					else
					{$data["total"]='0';}
				}
				else
				{$data["total"]='0';}
				
                array_push($result_data['booking'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getHistoryBooking($id_resto) {

		$dateNow = date('Y-m-d H:i:s');
        //$stmt = $this->conn->prepare("SELECT bh.id_h, bh.code, bh.date_created, c.nama, c.hp, c.path_photo FROM book_h bh, customer c WHERE bh.UserID = ? AND c.id_cust = bh.id_cust ORDER BY bh.id_h DESC");
		$stmt = $this->conn->prepare("SELECT bh.id_h, bh.code, bh.date_created, bh.type, bh.delivery_date, bh.delivery_time, bh.delivery_address, bh.delivery_status, c.nama, c.hp, c.path_photo
			FROM book_h bh, customer c
			WHERE bh.UserID = ?
			AND c.id_cust = bh.id_cust
			AND bh.date_created >= DATE_ADD( '$dateNow' , INTERVAL -1 WEEK ) 
			ORDER BY bh.id_h DESC ");
			
			
        $stmt->bind_param("s", $id_resto);
        
        $result_data["history_booking"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result(); 
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {
			//SELECT SUM(a.qty*b.HargaJual) as total FROM book_d a, menu b WHERE a.id=b.id AND a.id_h=108
                $data=array();
                $data["id_booking"]=$row["id_h"];
                $data["code"]=$row["code"];
                $data["name"]=$row["nama"];
                $data["phone"]=$row["hp"];					
                $data["date"]=$row["date_created"];
                $data["path_photo"]=$row["path_photo"];
				$data["metode_booking"]=$row["type"];
                $data["delivery_date"]=$row["delivery_date"];
                $data["delivery_time"]=$row["delivery_time"];
                $data["delivery_address"]=$row["delivery_address"];
                $data["delivery_status"]=$row["delivery_status"];

				$totalpromo = 0;
				$stmtTotalDgnPromo = $this->conn->prepare("SELECT SUM( a.qty * b.harga_promo ) AS total, c.disc, d.tax, d.service
					FROM book_d a, menu b, book_h c, restaurant d
					WHERE a.id = b.id
					AND a.id_h = c.id_h
					AND d.UserID = c.UserID
					AND a.id_h =? AND b.status_promo=1");
					
				$stmtTotalDgnPromo->bind_param("s", $data["id_booking"]);
				if ($stmtTotalDgnPromo->execute()) { 
					$resultTotalPromo = $stmtTotalDgnPromo->get_result();
					$num_rowsTotalPromo = $resultTotalPromo->num_rows;
					$rowTtotal = $resultTotalPromo->fetch_assoc();
					if ($num_rowsTotalPromo > 0) {	
						$totalpromo = $rowTtotal["total"];
					}
				}
				
				
				$stmtTotalTanpaPromo = $this->conn->prepare("SELECT SUM( a.qty * b.HargaJual ) AS total, c.disc, d.tax, d.service
					FROM book_d a, menu b, book_h c, restaurant d
					WHERE a.id = b.id
					AND a.id_h = c.id_h
					AND d.UserID = c.UserID
					AND a.id_h =? AND b.status_promo=0");
				
				$stmtTotalTanpaPromo->bind_param("s", $data["id_booking"]);

				if ($stmtTotalTanpaPromo->execute()) {
										
					$resultTotal = $stmtTotalTanpaPromo->get_result();
					$num_rowsTotal = $resultTotal->num_rows;
					$rowTtotal = $resultTotal->fetch_assoc();
					if ($num_rowsTotal > 0) {	
						$totalsemua = $rowTtotal["total"] + $totalpromo;
						$total = $totalsemua - (($totalsemua*$rowTtotal["disc"])/100);
						$tax = $rowTtotal["tax"];
						$service = $rowTtotal["service"];
						$_service = ($total*$service)/100;
						$_tax = (($total+$_service)*$tax)/100;
						$data["total"]=$total+$_service+$_tax;
					}
					else
					{$data["total"]='0';}
				}
				else
				{$data["total"]='0';}
				
				
				
                array_push($result_data['history_booking'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }

    public function getCardAdmin($id_resto) {

        $stmt = $this->conn->prepare("SELECT * FROM kartu WHERE UserID = ? ORDER BY  id_kartu DESC");
        $stmt->bind_param("s", $id_resto);
        
        $result_data["kartu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_kartu"]=$row["id_kartu"];
                $data["nama_kartu"]=$row["nama_kartu"];
                $data["charge"]=$row["charge"];
                array_push($result_data['kartu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getCardTaxService($id_resto) {

        $stmt = $this->conn->prepare("SELECT k.*, r.tax, r.service, r.ig, r.fb, r.logo FROM restaurant r LEFT JOIN kartu k on k.UserID = r.UserID WHERE r.UserID = ? ORDER BY id_kartu DESC");
        $stmt->bind_param("s", $id_resto);
        
        $result_data["kartu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_kartu"]=$row["id_kartu"];
                $data["nama_kartu"]=$row["nama_kartu"];
                $data["charge"]=$row["charge"];

                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["ig"]=$row["ig"];
                $data["fb"]=$row["fb"];
                $data["logo"]=$row["logo"];
                array_push($result_data['kartu'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getCashierAdmin($id_resto) {

        $stmt = $this->conn->prepare("SELECT * FROM cashier WHERE UserID = ? ORDER BY  id_cashier DESC");
        $stmt->bind_param("s", $id_resto);
        
        $result_data["cashier"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_cashier"]=$row["id_cashier"];
                $data["nama"]=$row["nama"];
                $data["email"]=$row["email"];
                $data["created"]=$row["created"];
                $data["password"]=$row["password"];
                $data["status"]=$row["status"];
                array_push($result_data['cashier'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListCustomers($id_resto) {
        $status = "1";
        $stmt = $this->conn->prepare("SELECT cd.date, cd.id_cust, c.* FROM customer_detail cd, customer c WHERE cd.UserID = ? AND c.id_cust = cd.id_cust AND c.status = ? ORDER BY cd.date DESC");
		
        $stmt->bind_param("ss", $id_resto, $status);
        
        $result_data["customers"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                if (sizeof($result_data["customers"]) < 10)  {
                    $data=array();
                    $data["id_cust"]=$row["id_cust"];
                    $data["nama"]=$row["nama"];
                    $data["email"]=$row["email"];
                    $data["phone"]=$row["hp"];
                    $data["join"]=$row["date"];
                    $data["status"]=$row["status"];
                    $data["path_photo"]=$row["path_photo"];
                    array_push($result_data['customers'], $data);
                }
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListCustomersWithFilter($id_resto, $fromDate, $toDate) {
        $status = "1";
        $stmt = $this->conn->prepare("SELECT cd.date, cd.id_cust, c.* FROM customer_detail cd, customer c WHERE cd.UserID = ? AND c.id_cust = cd.id_cust AND c.status = ? AND cd.date BETWEEN ? AND ? ORDER BY cd.date DESC");
        
        //With Filter : $stmt = $this->conn->prepare("SELECT cd.id_cust, cd.date, c.* FROM customer_detail cd, customer c WHERE cd.UserID = ? AND c.id_cust = cd.id_cust AND c.status = ? AND cd.date BETWEEN ? AND ? ORDER BY cd.date DESC");
        $stmt->bind_param("ssss", $id_resto, $status, $fromDate, $toDate);
        
        $result_data["customers"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_cust"]=$row["id_cust"];
                $data["nama"]=$row["nama"];
                $data["email"]=$row["email"];
                $data["phone"]=$row["hp"];
                $data["join"]=$row["date"];
                $data["status"]=$row["status"];
                $data["path_photo"]=$row["path_photo"];
                array_push($result_data['customers'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function addCardAdmin($id_resto, $name_card, $charge) {

        $stmt = $this->conn->prepare("INSERT INTO kartu (nama_kartu, charge, UserID) VALUES (?,?,?)");
        $stmt->bind_param("sss", $name_card, $charge, $id_resto);
        
        $result_data["kartu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
                $stmt1 = $this->conn->prepare("SELECT * FROM kartu WHERE UserID = ? ORDER BY  id_kartu DESC");
                $stmt1->bind_param("s", $id_resto);

                if ($stmt1->execute()) {   
                    $result = $stmt1->get_result();
                    $result_data["num_rows"] = $result->num_rows;

                    while ($row = $result->fetch_assoc()) {
                        $data=array();
                        $data["id_kartu"]=$row["id_kartu"];
                        $data["nama_kartu"]=$row["nama_kartu"];
                        $data["charge"]=$row["charge"];
                        array_push($result_data['kartu'], $data);
                    }
                    $stmt1->free_result();
                    $stmt1->close();
                    return $result_data;          
                } else {
                    $result_data["error"] = TRUE;
                    $stmt1->close();
                    return $result_data;            
                }
            $stmt->free_result();
            $stmt->close();     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }

    public function editCardAdmin($id_kartu, $name_card, $charge) {

        $stmt = $this->conn->prepare("UPDATE kartu SET nama_kartu = ?, charge = ? WHERE id_kartu = ?");
        $stmt->bind_param("sss", $name_card, $charge, $id_kartu);
        

        $result_data["error"] = FALSE;

        if ($stmt->execute()) {   
                
            $stmt->close();     
            return $result_data; 
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
                       
        }
        
    }

    public function deleteCardAdmin($id_kartu) {

        $stmt = $this->conn->prepare("DELETE FROM kartu WHERE id_kartu = ?");
        $stmt->bind_param("s", $id_kartu);

        $result_data["error"] = FALSE;
        if ($stmt->execute()) {   
                
            $stmt->close();     
            return $result_data; 
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
                       
        } 
    }

    public function deleteCashierAdmin($id_cashier) {

        $stmt = $this->conn->prepare("DELETE FROM cashier WHERE id_cashier = ?");
        $stmt->bind_param("s", $id_cashier);

        $result_data["error"] = FALSE;
        if ($stmt->execute()) {   
                
            $stmt->close();     
            return $result_data; 
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
                       
        } 
    }

    public function getHistoryBookingWithFilter($id_resto, $fromDate, $toDate) {

        $stmt = $this->conn->prepare("SELECT bh.id_h, bh.code, bh.date_created, bh.type, bh.delivery_date, bh.delivery_time, bh.delivery_address, bh.delivery_status, c.nama, c.hp, c.path_photo FROM book_h bh, customer c WHERE bh.UserID = ? AND c.id_cust = bh.id_cust AND bh.date_created BETWEEN ? AND ?");
        $stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
        
        $result_data["history_booking"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_booking"]=$row["id_h"];
                $data["code"]=$row["code"];
                $data["name"]=$row["nama"];
                $data["phone"]=$row["hp"];
                $data["date"]=$row["date_created"];
                $data["path_photo"]=$row["path_photo"];
                $data["metode_booking"]=$row["type"];
                $data["delivery_date"]=$row["delivery_date"];
                $data["delivery_time"]=$row["delivery_time"];
                $data["delivery_address"]=$row["delivery_address"];
                $data["delivery_status"]=$row["delivery_status"];

                $totalpromo = 0;
                $stmtTotalDgnPromo = $this->conn->prepare("SELECT SUM( a.qty * b.harga_promo ) AS total, c.disc, d.tax, d.service
                    FROM book_d a, menu b, book_h c, restaurant d
                    WHERE a.id = b.id
                    AND a.id_h = c.id_h
                    AND d.UserID = c.UserID
                    AND a.id_h =? AND b.status_promo=1");
                    
                $stmtTotalDgnPromo->bind_param("s", $data["id_booking"]);
                if ($stmtTotalDgnPromo->execute()) { 
                    $resultTotalPromo = $stmtTotalDgnPromo->get_result();
                    $num_rowsTotalPromo = $resultTotalPromo->num_rows;
                    $rowTtotal = $resultTotalPromo->fetch_assoc();
                    if ($num_rowsTotalPromo > 0) {  
                        $totalpromo = $rowTtotal["total"];
                    }
                }
                
                
                
                $stmtTotalTanpaPromo = $this->conn->prepare("SELECT SUM( a.qty * b.HargaJual ) AS total, c.disc, d.tax, d.service
                    FROM book_d a, menu b, book_h c, restaurant d
                    WHERE a.id = b.id
                    AND a.id_h = c.id_h
                    AND d.UserID = c.UserID
                    AND a.id_h =? AND b.status_promo=0");
                
                $stmtTotalTanpaPromo->bind_param("s", $data["id_booking"]);

                if ($stmtTotalTanpaPromo->execute()) {
                                        
                    $resultTotal = $stmtTotalTanpaPromo->get_result();
                    $num_rowsTotal = $resultTotal->num_rows;
                    $rowTtotal = $resultTotal->fetch_assoc();
                    if ($num_rowsTotal > 0) {   
                        $totalsemua = $rowTtotal["total"] + $totalpromo;
                        $total = $totalsemua - (($totalsemua*$rowTtotal["disc"])/100);
                        $tax = $rowTtotal["tax"];
                        $service = $rowTtotal["service"];
                        $_service = ($total*$service)/100;
                        $_tax = (($total+$_service)*$tax)/100;
                        $data["total"]=$total+$_service+$_tax;
                    }
                    else
                    {$data["total"]='0';}
                }
                else
                {$data["total"]='0';}

                array_push($result_data['history_booking'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListNewestResto($id_customer, $keyword) {    
        // $date_now = "2017-06-30";
        $key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT UserID, NamaPerusahaan, alamat, Telpon, path_photo, created_at, stts FROM restaurant WHERE stts = 1 AND UserID NOT IN (SELECT UserID FROM customer_detail WHERE id_cust = ?) ORDER BY created_at DESC LIMIT 10");
        $stmt->bind_param("s", $id_customer);
        
        $result_data["resto"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_resto"]=$row["UserID"];
                $data["name"]=$row["NamaPerusahaan"];
                $data["address"]=$row["alamat"];
                $data["phone"]=$row["Telpon"];
                $data["path_photo"]=$row["path_photo"];
                array_push($result_data['resto'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data; 
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListPromoHistory($id_resto) {    

        $stmt = $this->conn->prepare("SELECT ph.*, m.Gambar, m.Keterangan FROM promo_history ph, menu m WHERE ph.UserID = ? AND m.id = ph.id ORDER BY ph.id_promo_history DESC");
        $stmt->bind_param("s", $id_resto);
        
        $result_data["promo_history"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_promo_history"]=$row["id_promo_history"];
                $data["id_menu"]=$row["id"];
                $data["created"]=$row["created"];
                $data["price_awal"]=$row["price_awal"];
                $data["price_promo"]=$row["price_promo"];
                $data["note_promo"]=$row["note_promo"];
                $data["qty_promo"]=$row["qty_promo"];
                $data["tgl_mulai_promo"]=$row["tgl_mulai_promo"];
                $data["tgl_akhir_promo"]=$row["tgl_akhir_promo"];
                $data["waktu_mulai_promo"]=$row["waktu_mulai_promo"];
                $data["waktu_akhir_promo"]=$row["waktu_akhir_promo"];
                $data["path_photo"]=$row["Gambar"];
                $data["name_menu"]=$row["Keterangan"];

                array_push($result_data['promo_history'], $data);
            }

            $stmt->free_result();
            $stmt->close();
            return $result_data; 
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function getListSearchResto($id_customer, $keyword) {
        // $date_now = "2017-06-30";
		
		$cari=$keyword;
		$arr =  explode(" ", $cari);
		$query_parts = array();
		foreach ($arr as $val) {
			if($val!="")
			{
				$query_parts[] = "'%".$val."%'";
			}
		}
		$stringNamaP = implode(' OR NamaPerusahaan LIKE ', $query_parts);
		$stringAlamatP = implode(' OR alamat LIKE ', $query_parts);
		$stringKeywordP = implode(' OR keyword LIKE ', $query_parts);
		

        $key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT UserID, NamaPerusahaan, alamat, Telpon, path_photo, created_at, stts FROM restaurant WHERE (stts = 1) AND (NamaPerusahaan LIKE '%$cari%' OR NamaPerusahaan LIKE {$stringNamaP} OR alamat LIKE '%$cari%' OR alamat LIKE {$stringAlamatP} OR keyword LIKE '%$cari%' OR keyword LIKE {$stringKeywordP}) AND UserID NOT IN (SELECT UserID FROM customer_detail WHERE id_cust = ?) ORDER BY created_at DESC");
        $stmt->bind_param("s", $id_customer);
        
        $result_data["resto"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_resto"]=$row["UserID"];
                $data["name"]=$row["NamaPerusahaan"];
                $data["address"]=$row["alamat"];
                $data["phone"]=$row["Telpon"];
                $data["path_photo"]=$row["path_photo"];
                array_push($result_data['resto'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data; 
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function isUserExistedUsers($email) {
        $stmt = $this->conn->prepare("SELECT email from users WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function isUserExistedResto($email) {
        $stmt = $this->conn->prepare("SELECT email from restaurant WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function isUserExistedCustomer($email) {
        $stmt = $this->conn->prepare("SELECT email from customer WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

   public function isUserExistedCashier($email) {
        $stmt = $this->conn->prepare("SELECT email from cashier WHERE email = ?");

        $stmt->bind_param("s", $email);

        $stmt->execute();

        $stmt->store_result();

        if ($stmt->num_rows > 0) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    /**
     * Encrypting password
     * @param password
     * returns salt and encrypted password
     */
    public function hashSSHA($password) {

        $salt = sha1(rand());
        $salt = substr($salt, 0, 10);
        $encrypted = base64_encode(sha1($password . $salt, true) . $salt);
        $hash = array("salt" => $salt, "encrypted" => $encrypted);
        return $hash;
    }

    /**
     * Decrypting password
     * @param salt, password
     * returns hash string
     */
    public function checkhashSSHA($salt, $password) {

        $hash = base64_encode(sha1($password . $salt, true) . $salt);

        return $hash;
    }

    public function deleteMenu($idMenu, $path_photo) {

        $tgl_up = date('Y-m-d H:i:s');

        $sqlCari="SELECT * FROM transaction_detail WHERE id='$idMenu'";
        $resultCari=$this->conn->query($sqlCari);
        //$resultCari->execute();
        if($resultCari!=null && (mysqli_num_rows($resultCari)>=1)) {
            $stmt = $this->conn->prepare("UPDATE restaurant r INNER JOIN menu m ON r.UserID = m.UserID SET r.menu_up = ?, m.aktif='0' WHERE m.id=?");

            $stmt->bind_param("ss", $tgl_up, $idMenu);

            if ($stmt->execute()) {
                $stmt->close();
                return true;
            } else {
                            // user not existed
                $stmt->close();
                 return false;
            }
        }
        else {
            $stmt = $this->conn->prepare("SELECT * FROM menu WHERE id = ?");

            $stmt->bind_param("s", $idMenu);

            $stmt->execute();

            if ($stmt->execute()) {
                $result = $stmt->get_result();
                $result_data["num_rows"] = $result->num_rows;

                while ($row = $result->fetch_assoc()) {

                        $data=array();
                        $data["path_photo"]=$row["Gambar"]; //ini path photonya

                    if ($data["path_photo"] == "default_food.png" || $data["path_photo"] == "default_beverage.png" || $data["path_photo"] == "default_other.png") {

                        $stmt = $this->conn->prepare("DELETE FROM menu WHERE id = ?");
                        $stmt->bind_param("s", $idMenu);
                        $stmt->execute();
                    }
                    else {
                        
                        if (file_exists("../storage/photo_menu/".$data["path_photo"]."")) {
                            unlink("../storage/photo_menu/".$data["path_photo"].""); // delete foto
                        }
                        if (file_exists("../storage/photo_menu_s/".$data["path_photo"]."")) {
                            unlink("../storage/photo_menu_s/".$data["path_photo"].""); // delete foto
                        }
                        if (file_exists("../storage/photo_menu_m/".$data["path_photo"]."")) {
                            unlink("../storage/photo_menu_m/".$data["path_photo"].""); // delete foto
                        }

                        //setelah delete foto lalu delete record dari database
                        $stmt = $this->conn->prepare("DELETE FROM menu WHERE id = ?");
                        $stmt->bind_param("s", $idMenu);
                        $stmt->execute();
                    }

                    if ($stmt->execute()) {
                        $stmtUp = $this->conn->prepare("UPDATE restaurant SET menu_up=? WHERE UserID=?");
                        $stmtUp->bind_param("ss", $tgl_up, $row["UserID"]);
                        $stmtUp->execute();

                        $stmt->close();
                        $stmtUp->close();
                        return true;
                    } else {
                        // user not existed
                        $stmt->close();
                        return false;
                    }
                } //end while
            }
        }


        
    }


    public function deletePromoMenu($idMenu) {

        $tgl_up = date('Y-m-d H:i:s');
        $status_promo = "0";
        $tgl_mulai_promo = "0000-00-00";
        $tgl_akhir_promo = "0000-00-00";
        $waktu_mulai_promo = "00:00:00";
        $waktu_akhir_promo = "00:00:00";
        $harga_promo ="0";

        $stmt = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_mulai_promo = ?, m.tgl_akhir_promo = ?, m.waktu_mulai_promo = ?, m.waktu_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.id = ?");
        
        $stmt->bind_param("ssssssss", $status_promo, $tgl_mulai_promo, $tgl_akhir_promo, $waktu_mulai_promo, $waktu_akhir_promo, $harga_promo, $tgl_up, $idMenu);
        $stmt->execute();

        if ($stmt->execute()) {

            $stmt->close();
            return true;
        }
        else {
            $stmt->close();
            return false;   
        }
    }

    public function unpaidTransactionCashier($id_transaction) {
        $status = "U";
        $stmt = $this->conn->prepare("UPDATE transaction SET status = ? WHERE id_transaction = ?");
        $stmt->bind_param("ss", $status, $id_transaction);
        $stmt->execute();
        if ($stmt->execute()) {
            $stmt->close();
            $stmt1 = $this->conn->prepare("DELETE FROM bill_d WHERE id_transaction = ?");
            $stmt1->bind_param("s", $id_transaction);
            $stmt1->execute();
            if ($stmt1->execute()) {
                $stmt1->close();
                return true;
            }
            else {
                $stmt1->close();
                return false;   
            }
        }
        else {
            $stmt->close();
            return false;   
        }
    }

    public function deleteMyResto($id_resto, $id_customer) {

        $stmt = $this->conn->prepare("DELETE FROM customer_detail WHERE id_cust = ? AND UserID = ?");
        $stmt->bind_param("ss", $id_customer, $id_resto);

        if ($stmt->execute()) {
           
            $stmt2 = $this->conn->prepare("UPDATE book_h SET stts='3' WHERE id_cust = ? AND UserID = ?");
            $stmt2->bind_param("ss", $id_customer, $id_resto);
            
            if ($stmt2->execute()) {
                return true;
                $stmt2->close();
            }

            
            
            $stmt->close();
        }
        else {

            $stmt->close();
            return false;   
        }
    }

    // public function deleteMyResto($id_resto, $id_customer) {

    //     $stmt = $this->conn->prepare("DELETE FROM customer_detail WHERE id_cust = ? AND UserID = ?");
    //     $stmt->bind_param("ss", $id_customer, $id_resto);

    //     if ($stmt->execute()) {

    //         $stmt->close();
    //         return true;
    //     }
    //     else {

    //         $stmt->close();
    //         return false;   
    //     }
    // }

    public function deleteBooking($id_booking) {

        $stmt = $this->conn->prepare("UPDATE book_h SET stts='3' WHERE id_h = ?");
        $stmt->bind_param("s", $id_booking);

        if ($stmt->execute()) {
            $stmt1 = $this->conn->prepare("DELETE FROM book_d WHERE id_h = ?");
            $stmt1->bind_param("s", $id_booking);
            if ($stmt1->execute()) {
                $stmt->close();
                return true;
            }
            $stmt->close();
        }
        else {

            $stmt->close();
            return false;   
        }
    }

    public function deleteTransaction($id_transaction) {

        $stmt = $this->conn->prepare("DELETE FROM transaction WHERE id_transaction = ?");
        $stmt->bind_param("s", $id_transaction);

        if ($stmt->execute()) {
            $stmt1 = $this->conn->prepare("DELETE FROM transaction_detail WHERE id_transaction = ?");
            $stmt1->bind_param("s", $id_transaction);
            if ($stmt1->execute()) {
                $stmt1->close();
                $stmt2 = $this->conn->prepare("DELETE FROM bill_d WHERE id_transaction = ?");
                $stmt2->bind_param("s", $id_transaction);
                if ($stmt2->execute()) {
                    $stmt2->close();
                    return true;
                }
                else {
                    $stmt2->close();
                    return false;   
                }
            }
            $stmt->close();
        }
        else {

            $stmt->close();
            return false;   
        }
    }


    public function deleteBillCashier($id_transaction) {

        $stmt = $this->conn->prepare("DELETE FROM bill_tmp WHERE id_transaction = ?");
        $stmt->bind_param("s", $id_transaction);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        }
        else {
            $stmt->close();
            return false;   
        }
    }

    public function deleteCart($id_customer, $id_menu) {

        $stmt = $this->conn->prepare("DELETE FROM cart_tmp WHERE id_cust = ? AND id = ?");
        $stmt->bind_param("ss", $id_customer, $id_menu);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        }
        else {

            $stmt->close();
            return false;   
        }
    }
 
    public function deleteBillTmp($id_cashier, $id_resto) {
        $stmt = $this->conn->prepare("DELETE FROM bill_tmp WHERE id_cashier = ?");
        $stmt->bind_param("s", $id_cashier);

        if ($stmt->execute()) {
            $stmt1 = $this->conn->prepare("SELECT ig,fb,logo FROM restaurant WHERE UserID = ?");
            $stmt1->bind_param("s", $id_resto);
            if ($stmt1->execute()) {
                $data = $stmt1->get_result()->fetch_assoc();
                $stmt1->close();
                return $data;
            }
            $stmt->close();
        }
        else {

            $stmt->close();
            return false;   
        }
    }

    
    public function deleteMenuOrderTmp($id_user, $id_menu, $code) {

        $stmt = $this->conn->prepare("DELETE FROM order_tmp WHERE id_user = ? AND id = ? AND code=?");
        $stmt->bind_param("sss", $id_user, $id_menu, $code);


        if ($stmt->execute()) {
            $stmt->close();
            return true;
        }
        else {

            $stmt->close();
            return false;   
        }
    }


    public function deleteOrderTmp($id_user, $id_resto) {

        $stmt = $this->conn->prepare("DELETE FROM order_tmp WHERE id_user = ?");
        $stmt->bind_param("s", $id_user);

        if ($stmt->execute()) {
            $stmt1 = $this->conn->prepare("SELECT disc_order,tax,service,ig,fb,logo FROM restaurant WHERE UserID = ?");
            $stmt1->bind_param("s", $id_resto);
            if ($stmt1->execute()) {
                $data = $stmt1->get_result()->fetch_assoc();
                $stmt1->close();
                return $data;
            }
            $stmt->close();
        }
        else {

            $stmt->close();
            return false;   
        }
    }

    public function updateStatusDeliveryBooking($status_delivery, $id_booking) {
        $stmt = $this->conn->prepare("UPDATE book_h SET delivery_status = ? WHERE id_h = ?");
        $stmt->bind_param("ss", $status_delivery, $id_booking);
        if ($stmt->execute()) {

            $stmtToken = $this->conn->prepare("SELECT c.os, c.token, b.code FROM customer c INNER JOIN book_h b ON c.id_cust = b.id_cust WHERE b.id_h= ?");
            $stmtToken->bind_param("s", $id_booking);

            if ($stmtToken->execute()) {
                $CekToken = $stmtToken->get_result()->fetch_assoc();
                $stmtToken->close();

                $status = ($status_delivery == "2") ? "Follow Up" : (($status_delivery == "3") ? "Process" : (($status_delivery == "4") ? "Delivery" : (($status_delivery == "5") ? "Finished" : "Cancel" )));

                if ($CekToken["token"] != "" && $CekToken["os"]=="1" && $CekToken["isOrderNotifOn"]=="1") {
                        require_once('firebase.php');
                        require_once('push.php');

                        $firebase = new Firebase();
                        $push = new Push();

                            //payload ini data spesifik yg mau dikirim, misal data dirinya 
                            //klo nggak ad biarin aj kayak gini
                        $payload = array();
                        $payload['init'] = "success";

                        $push->setNotifUntuk("updateStatus");
                        $push->setTitle("SpeedResto");
                        $push->setMessage("Your booking code ".$CekToken["code"]."is ".$status." by Resto");
                            // $push->setImage(''); //dikosongin kalo nggak makek image
                        $push->setImage('');
                        $push->setIsBackground(FALSE);
                        $push->setPayload($payload);

                        $json = '';
                        $response = '';

                        $json = $push->getPush();
                        $firebase->send($CekToken["token"],$json);
                    }
                    else if ($CekToken["token"] != "" && $CekToken["os"]=="2" && $CekToken["isOrderNotifOn"]=="1") {
                        require_once('firebase_ios.php');
                        require_once('push_ios.php');

                        $firebase = new Firebase();
                        $push = new Push();

                            //payload ini data spesifik yg mau dikirim, misal data dirinya 
                            //klo nggak ad biarin aj kayak gini
                        $payload = array();
                        $payload['init'] = "success";

                        $push->setNotifUntuk("createBooking");
                        $push->setTitle("SpeedResto");
                        $push->setMessage("Your booking code ".$CekToken["code"]."is ".$status." by Resto");
                        $push->setImage('');
                        $push->setIsBackground(FALSE);
                        $push->setPayload($payload);

                        $json = '';
                        $response = '';

                        $json = $push->getPush();
                        $firebase->send($CekToken["token"],$json);
                    }
            }

            $stmt->close();
            return true;
        }
        else {
            $stmt->close();
            return false;   
        }
    }

    public function createBooking($id_customer, $id_resto, $disc, $metode_booking, $date, $time, $address, $id_menus, $qty_menus, $note_menus, $id_promos, $price_menus) {
        $id_menus = explode('{}', $id_menus);
        $qty_menus = explode('{}', $qty_menus);
        $note_menus = explode('{}', $note_menus);
        $id_promos = explode('{}', $id_promos);
        $price_menus = explode('{}', $price_menus);

        $cek=1;
		$dateNow = date('Y-m-d H:i:s');
		$random = include 'do.php';
        $stmt = $this->conn->prepare("INSERT INTO book_h (code, date_created, UserID, id_cust, disc, type, delivery_date, delivery_time, delivery_address) VALUES(?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("sssssssss", $random, $dateNow, $id_resto, $id_customer, $disc, $metode_booking, $date, $time, $address);
        
        if ($stmt->execute()) {
            $last_id = $this->conn->insert_id;


            foreach($id_menus as $i =>$id_menu) {

                //echo $i.' '.$id_menu .'</br>';

                $stmt_tmp = $this->conn->prepare("SELECT m.qty_promo, m.status_promo FROM menu m WHERE m.id = ?");
                $stmt_tmp->bind_param("s", $id_menu);
                $stmt_tmp->execute();
                $result_tmp = $stmt_tmp->get_result();
                $row_tmp = $result_tmp->fetch_assoc();

                $stmt1 = $this->conn->prepare("INSERT INTO book_d (id_h, id, qty, note_order, id_promo, harga) VALUES (?,?,?,?,?,?)");
                $stmt1->bind_param("ssssss", $last_id, $id_menu, $qty_menus[$i], $note_menus[$i], $id_promos[$i], $price_menus[$i]);

                if ($stmt1->execute()) {
                    if($row_tmp["status_promo"] == "1" && $row_tmp["qty_promo"] !='')
                        {
                       $sisa = $row_tmp["qty_promo"] - $qty_menus[$i];
                       $stmtup = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID set m.qty_promo = ?, r.menu_up=? WHERE m.id=?");
                        $stmtup->bind_param("sss", $sisa, $dateNow, $id_menu);
                        $stmtup->execute();
                    }
                }


            }


            $stmtToken = $this->conn->prepare("SELECT token, os, isOrderNotifOn FROM restaurant WHERE UserID = ?");
            $stmtToken->bind_param("s", $id_resto);

            if ($stmtToken->execute()) {
                $CekToken = $stmtToken->get_result()->fetch_assoc();
                $stmtToken->close();
                if ($CekToken["token"] != "" && $CekToken["os"]=="1" && $CekToken["isOrderNotifOn"]=="1") {
                    require_once('firebase.php');
                    require_once('push.php');

                    $firebase = new Firebase();
                    $push = new Push();

                    //payload ini data spesifik yg mau dikirim, misal data dirinya 
                    //klo nggak ad biarin aj kayak gini
                    $payload = array();
                    $payload['init'] = "success";

                    $push->setNotifUntuk("createBooking");
                    $push->setTitle("SpeedResto");
                    $push->setMessage("New booking with code ".$random);
                        // $push->setImage(''); //dikosongin kalo nggak makek image
                    $push->setImage('');
                    $push->setIsBackground(FALSE);
                    $push->setPayload($payload);

                    $json = '';
                    $response = '';

                    $json = $push->getPush();
                    $firebase->send($CekToken["token"],$json);
                }
                else if ($CekToken["token"] != "" && $CekToken["os"]=="2" && $CekToken["isOrderNotifOn"]=="1") {
                    require_once('firebase_ios.php');
                    require_once('push_ios.php');

                    $firebase = new Firebase();
                    $push = new Push();

                        //payload ini data spesifik yg mau dikirim, misal data dirinya 
                        //klo nggak ad biarin aj kayak gini
                    $payload = array();
                    $payload['init'] = "success";

                    $push->setNotifUntuk("createBooking");
                    $push->setTitle("SpeedResto");
                    $push->setMessage("New booking with code ".$random);
                        // $push->setImage(''); //dikosongin kalo nggak makek image
                    $push->setImage('');
                    $push->setIsBackground(FALSE);
                    $push->setPayload($payload);

                    $json = '';
                    $response = '';

                    $json = $push->getPush();
                    $firebase->send($CekToken["token"],$json);
                }
            } 
            $stmt->close();
            return true;
        }
        else {
            $stmt->close();
            return false;   
        }
    }
    public function updateMenuPush($id_resto){
        //push ke semua waiter
        require_once('firebase.php');
        require_once('push.php');
        $firebase = new Firebase();
        $push = new Push();
        $payload = array();
        $payload['init'] = "success";
        $push->setNotifUntuk("updateMenu");
        $push->setTitle("Menu updated");
        $push->setMessage("Tap to update menu");
            // $push->setImage(''); //dikosongin kalo nggak makek image
        $push->setImage('');
        $push->setIsBackground(FALSE);
        $push->setPayload($payload);
        $json = '';
        $response = '';
        $json = $push->getPush();
        //$firebase->send($CekToken["token"],$json);
    }

    public function savePayment($no_bill, $total, $disc, $tunai, $kartu, $id_kartu, $tax, $service, $charge, $id_cashier, $persen_charge, $disc_nota) {
        //$random = mt_rand(100000, 999999);
        $stmt = $this->conn->prepare("INSERT INTO bill_h (no_bill,total,disc,tunai,kartu,id_kartu,tax,service,charge,id_cashier) VALUES(?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("ssssssssss", $no_bill, $total, $disc, $tunai, $kartu, $id_kartu, $tax, $service, $charge, $id_cashier);
        
        if ($stmt->execute()) {
            $stmt->close();
            $last_id = $this->conn->insert_id;
            $stmt1 = $this->conn->prepare("INSERT INTO bill_d (id_h, id_transaction) SELECT ?, id_transaction FROM bill_tmp WHERE id_cashier = ?");
            $stmt1->bind_param("ss", $last_id, $id_cashier);
            if ($stmt1->execute()) {
                $stmt1->close();
                $status = "P";
				$tunai = $tunai;


                //SELECT dan update per Transaction
                $stmtDetailPay = $this->conn->prepare("SELECT a.id_transaction, a.total_bill,a.disc,b.tax,b.service FROM transaction a inner join restaurant b on a.UserID=b.UserID WHERE a.id_transaction IN (SELECT id_transaction FROM bill_tmp WHERE id_cashier = ?)");
                    $stmtDetailPay->bind_param("s", $id_cashier);
                    
                    $result_data["num_rows"] = 0;

                    if ($stmtDetailPay->execute()) {   
                        $resultDetailPay = $stmtDetailPay->get_result();
                        $result_dataDetailPay["num_rows"] = $resultDetailPay->num_rows;
                        while ($rowDetailPay = $resultDetailPay->fetch_assoc()) {
                            $total_tr=$rowDetailPay["total_bill"];
                            $disc_tr=$rowDetailPay["disc"];
                            $service_rest=$rowDetailPay["service"];
                            $tax_rest=$rowDetailPay["tax"];
                            
                            $total_after_disc=($total_tr-$disc_tr); 
                            $disc_nota_tr = ($disc_nota>0) ? round((($total_after_disc*$disc_nota)/100), 2) : 0;
                            $service_tr = ($service_rest>0) ? round(((($total_after_disc-$disc_nota_tr)*$service_rest)/100), 2) : 0;
                            $tax_tr = ($tax_rest>0) ? round(((($total_after_disc-$disc_nota_tr+$service_tr)*$tax_rest)/100), 2) : 0;
                            
							$total_sebelum_charge = $total_after_disc-$disc_nota_tr+$service_tr+$tax_tr;
							if($persen_charge>0)
							{
								
								if($total_sebelum_charge >= $tunai)
								{
									$total_sementara = $total_sebelum_charge - $tunai;
									$charge_tr =round((($total_sementara *$persen_charge)/100), 2);
									$tunai = 0;
								}
								else
								{
									$tunai = $tunai - $total_sebelum_charge;
									$charge_tr = 0;
								}
							}
							else
							{
								$charge_tr=0;
							}
							
                            $stmtUpDetail = $this->conn->prepare("UPDATE transaction SET status = ?, id_cashier = ? , disc_nota = ?, service = ?, tax = ?, charge = ? WHERE id_transaction = ?");
                            $stmtUpDetail->bind_param("sssssss", $status, $id_cashier, $disc_nota_tr, $service_tr, $tax_tr, $charge_tr, $rowDetailPay["id_transaction"]);
                            $stmtUpDetail->execute();
                        }
                        return $last_id;
                    }

                // $stmt2 = $this->conn->prepare("UPDATE transaction SET status = ?, id_cashier =? WHERE id_transaction IN (SELECT id_transaction FROM bill_tmp WHERE id_cashier = ?)");
                // $stmt2->bind_param("sss", $status, $id_cashier, $id_cashier);
                // if ($stmt2->execute()) {
                //     $stmt2->close();
                //     return $last_id;
                // }
            }
        }
        else {
            $stmt->close();
            return false;   
        }
    }


    public function setPromoMenu($id_menu, $promo_status, $promo_price, $begin_date, $end_date, $begin_time, $end_time, $note_promo) {
        
        $date_now = date("Y-m-d H:i:s");
        $stmt = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.harga_promo = ?, m.tgl_mulai_promo = ?, m.tgl_akhir_promo = ?, m.waktu_mulai_promo = ?, m.waktu_akhir_promo = ?, m.note_promo = ?, r.menu_up=? WHERE m.id = ?");
        $stmt->bind_param("sssssssss", $promo_status, $promo_price, $begin_date, $end_date, $begin_time, $end_time, $note_promo, $date_now, $id_menu);
        if ($stmt->execute()) {

            //=====KIRIM NOTIFIKASI=====//
            if ($promo_status == "1") {
                $stmtMenu = $this->conn->prepare("SELECT m.*, r.NamaPerusahaan FROM menu m, restaurant r WHERE m.id = ? AND r.UserID = m.UserID");
                $stmtMenu->bind_param("s", $id_menu);
                if ($stmtMenu->execute()) {
                    $row = $stmtMenu->get_result()->fetch_assoc();
                    $stmtMenu->close();

                    $id_menu = $row['id'];
                    $id_restaurant = $row['UserID'];
                    $name_restaurant = $row['NamaPerusahaan'];
                    $name_menu = $row['Keterangan'];
                    $description = $row['Note'];
                    $path_photo = $row['Gambar'];
                    $price = $row['HargaJual'];
                    $price_promo = $row['harga_promo'];
                    $start_promo = $row['tgl_mulai_promo'];
                    $end_promo = $row['tgl_akhir_promo'];
                    $startTime_promo = $row['waktu_mulai_promo'];
                    $endTime_promo = $row['waktu_akhir_promo'];
                    $note_promo = $row['note_promo'];

                    $stmtPromoHistory = $this->conn->prepare("INSERT INTO promo_history (id,UserID,price_awal,price_promo,note_promo,tgl_mulai_promo,tgl_akhir_promo,waktu_mulai_promo,waktu_akhir_promo, created) VALUES(?,?,?,?,?,?,?,?,?,?)");
                    $stmtPromoHistory->bind_param("ssssssssss", $id_menu, $id_restaurant, $price, $price_promo, $note_promo, $start_promo, $end_promo, $startTime_promo, $endTime_promo, $date_now);
                    $stmtPromoHistory->execute();
                    $stmtPromoHistory->close();

                    $stmtToken = $this->conn->prepare("SELECT c.id_cust,c.token,c.os FROM customer c INNER JOIN customer_detail cd ON c.id_cust = cd.id_cust WHERE cd.UserID = ? GROUP BY cd.id_cust");
                    $stmtToken->bind_param("s", $id_restaurant);
                    if ($stmtToken->execute()) {
                        $result = $stmtToken->get_result();
                        $stmtToken->close();

                        $listCustomer = array();
                        while($row = $result->fetch_assoc()){
                            if ($row["token"] != "" && $row["os"]=="1") {
                                array_push($listCustomer, $row["token"]);
                            }
                        }


                        require_once('firebase.php');
                        require_once('push.php');

                        $firebase = new Firebase();
                        $push = new Push();

                        //payload ini data spesifik yg mau dikirim, misal data dirinya 
                        //klo nggak ad biarin aj kayak gini
                        $payload = array();
                        $payload['init'] = "success";
                        $payload['id_menu'] = $id_menu;
                        $payload['id_restaurant'] = $id_restaurant;
                        $payload['name_restaurant'] = $name_restaurant;
                        $payload['name_menu'] = $name_menu;
                        $payload['description'] = $description;
                        $payload['path_photo'] = $path_photo;
                        $payload['price'] = $price;
                        $payload['price_promo'] = $price_promo;
                        $payload['start_promo'] = $start_promo;
                        $payload['end_promo'] = $end_promo;
                        $payload['startTime_promo'] = $startTime_promo;
                        $payload['endTime_promo'] = $endTime_promo;
                        $payload['note_promo'] = $note_promo;


                        $push->setClickAction("net.bamboomedia.speedresto.fcmwithappserver_TARGET_NOTIFICATION");
                        $push->setNotifUntuk("promoMenu");
                        $push->setTitle("SpeedResto");
                        $push->setMessage("Promo for ".$name_menu.", grab it fast!");
                            // $push->setImage(''); //dikosongin kalo nggak makek image
                        $push->setImage('');
                        $push->setIsBackground(FALSE);
                        $push->setPayload($payload);

                        $json = '';
                        $response = '';
                        

                        if (sizeof($listCustomer) > 0) {
                            $json = $push->getPush();
                            for ($i=0; $i<sizeof($listCustomer); $i++) {
                                //echo var_dump($listCustomer[$i]);
                                $firebase->send($listCustomer[$i],$json);
                            }
                            // $firebase->sendMultiple($getListCustomers, $json);
                        }
                    } 
                }
            }
            //=====END KIRIM NOTIFIKASI=====//

            $stmt->close();
            return true;
        }
        else {

            $stmt->close();
            return false;   
        }
    }



    public function updateMenu($idMenu, $name, $price, $category, $JenisItem, $pathPhoto, $status, $description, $print) {
        $tgl_up = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.Keterangan = ?, m.HargaJual = ?, m.Gambar = ?, m.id_kategori = ?, m.JenisItem = ?, m.QtyStock = ?, m.Note = ?, m.status_print = ?, r.menu_up=? WHERE m.id = ?");

        $stmt->bind_param("ssssssssss", $name, $price, $pathPhoto, $category, $JenisItem, $status, $description, $print, $tgl_up, $idMenu);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updateMenu2($idMenu, $name, $price, $category, $JenisItem, $status, $description, $print) {
        $tgl_up = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.Keterangan = ?, m.HargaJual = ?, m.id_kategori = ?, m.JenisItem = ?, m.QtyStock = ?, m.Note = ?, m.status_print = ?, r.menu_up=? WHERE m.id = ?");

        $stmt->bind_param("sssssssss", $name, $price, $category, $JenisItem, $status, $description, $print, $tgl_up, $idMenu);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updatePathPhotoMenu($idMenu, $pathPhoto) {
        $tgl_up = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.Gambar = ?, r.menu_up=? WHERE m.id = ?");

        $stmt->bind_param("sss", $pathPhoto, $tgl_up, $idMenu);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function getListUser($id_resto, $keyword) {

        $key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT * FROM users WHERE UserID = ? AND name LIKE ? ORDER BY name ASC");
        $stmt->bind_param("ss", $id_resto, $key);
        
        $result_data["user"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_user"]=$row["id_user"];
                $data["name"]=$row["name"];
                $data["email"]=$row["email"];
                $data["created_at"]=$row["created_at"];
                $data["updated_at"]=$row["updated_at"];
                $data["gender"]=$row["gender"];
                $data["path_photo"]=$row["path_photo"];

                array_push($result_data['user'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }

    public function getDetailUserSearch($email) {
        $stmt = $this->conn->prepare("SELECT * FROM users WHERE email = ? AND UserID IS NULL");
        $stmt->bind_param("s", $email);
        
        $result_data["user"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_user"]=$row["id_user"];
                $data["name"]=$row["name"];
                //$data["email"]=$row["email"];
                //$data["created_at"]=$row["created_at"];
                //$data["updated_at"]=$row["updated_at"];
                $data["gender"]=$row["gender"];
                $data["path_photo"]=$row["path_photo"];
                $data["id_restaurant"]=$row["id_restaurant"];

                array_push($result_data['user'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }

    public function addUserToResto($id_user, $id_resto) {
        $stmt = $this->conn->prepare("UPDATE users SET UserID = ? WHERE id_user = ?");

        $stmt->bind_param("ss", $id_resto, $id_user);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function addRestoToCustomerDetail($id_customer, $id_resto) {
        $stmt1 = $this->conn->prepare("SELECT UserID FROM restaurant WHERE UserID = ?");
        $stmt1->bind_param("s", $id_resto);
        if ($stmt1->execute()) {
            $result = $stmt1->get_result();
            $num_rows = $result->num_rows;
            if ($num_rows > 0) {
                $stmtCek = $this->conn->prepare("SELECT UserID FROM customer_detail WHERE UserID = ? AND id_cust = ?");
                $stmtCek->bind_param("ss", $id_resto, $id_customer);
                if ($stmtCek->execute()) {
                    $resultCek = $stmtCek->get_result();
                    $num_rowsCek = $resultCek->num_rows;
                    if ($num_rowsCek > 0) {
                        $stmtCek->close();
                        return false;
                    }
                    else
                    {
                        //-------insert proses--------//
                        $date = date("Y-m-d");
                        $stmt = $this->conn->prepare("INSERT INTO customer_detail(id_cust, UserID, date) VALUES(?,?,?)");
                        $stmt->bind_param("sss", $id_customer, $id_resto, $date);
                        if ($stmt->execute()) {
                            $stmt->close();
                            return true;
                        } else {
                            $stmt->close();
                            return false;
                        }
                        //-----inset proses end-----//
                    }
                }
                else
                {
                    $stmtCek->close();
                    return false;
                }
            } else {
                $stmt1->close();
                return false;    
            }
        } else {
            $stmt1->close();
            return false;
        }
        
    }


    public function saveCart($id_customer, $id_menu, $qty, $note_order, $isFromNotif, $id_promo) {
        if ($isFromNotif == "1") {
            $stmd = $this->conn->prepare("DELETE FROM cart_tmp WHERE id_cust = ?");
            $stmd->bind_param("s", $id_customer);
            $stmd->execute(); 
            $stmd->close();
        }

        $stmt = $this->conn->prepare("SELECT id, qty FROM cart_tmp WHERE id = ? AND id_cust = ?");
        $stmt->bind_param("ss", $id_menu, $id_customer);

        if ($stmt->execute()) { 
            $result = $stmt->get_result();
            $num_rows = $result->num_rows;
            $row = $result->fetch_assoc();
            if ($num_rows > 0) {
                //update qty dan note menu lama
                $qty_lama = $row["qty"];
                $qty_baru = $qty_lama + $qty;  

                $stmt1 = $this->conn->prepare("UPDATE cart_tmp SET qty = ?, note_order = ? WHERE id = ? AND id_cust = ?");
                $stmt1->bind_param("ssss", $qty_baru, $note_order, $id_menu, $id_customer);
                if ($stmt1->execute()) {
                    $stmt1->close();
                    return true;
                } else {
                    $stmt1->close();
                    return false;
                }
            }
            else {
                //insert menu baru
                $stmt2 = $this->conn->prepare("INSERT INTO cart_tmp(id_cust, id, qty, note_order, id_promo) VALUES(?,?,?,?,?)");

                $stmt2->bind_param("sssss", $id_customer, $id_menu, $qty, $note_order, $id_promo);

                // $stmt->execute();

                if ($stmt2->execute()) {
                    // user existed 
                    $stmt2->close();
                    return true;
                } else {
                    // user not existed
                    $stmt2->close();
                    return false;
                }
            }
            $stmt->close();
        } else {
            $stmt->close();
            return false;
        }
    }



    public function saveOrderTmp($id_user, $id_menu, $qty, $note_order, $isVoid, $code, $id_promo) {
        $stmt = $this->conn->prepare("SELECT id, qty FROM order_tmp WHERE id = ? AND code = ? AND id_user = ?");
        $stmt->bind_param("sss", $id_menu, $code, $id_user);

        if ($stmt->execute()) { 
            $result = $stmt->get_result();
            $num_rows = $result->num_rows;
            $row = $result->fetch_assoc();
            if ($num_rows > 0) {
                //update qty dan note menu lama
                $qty_lama = $row["qty"];
                if (!$isVoid) {
                    $qty_baru = $qty_lama + $qty;
                } else {
                    $qty_baru = $qty;
                }

                $stmt1 = $this->conn->prepare("UPDATE order_tmp SET qty = ?, note_order = ? WHERE id = ? AND code = ? AND id_user = ?");
                $stmt1->bind_param("sssss", $qty_baru, $note_order, $id_menu, $code, $id_user);
                if ($stmt1->execute()) {
                    $stmt1->close();
                    return true;
                } else {
                    $stmt1->close();
                    return false;
                }
            }
            else {
                //insert menu baru
                $stmt2 = $this->conn->prepare("INSERT INTO order_tmp(id_user, id, qty, note_order,code, id_promo) VALUES(?,?,?,?,?,?)");

                $stmt2->bind_param("ssssss", $id_user, $id_menu, $qty, $note_order, $code, $id_promo);

                // $stmt->execute();

                if ($stmt2->execute()) {
                    // user existed 
                    $stmt2->close();
                    return true;
                } else {
                    // user not existed
                    $stmt2->close();
                    return false;
                }
            }
            $stmt->close();
        } else {
            $stmt->close();
            return false;
        }
    }


    public function insertBillTmp($id_transaction, $id_cashier) {
        $stmt = $this->conn->prepare("INSERT INTO bill_tmp (id_transaction, id_cashier) VALUES (?,?)");
        $stmt->bind_param("ss", $id_transaction, $id_cashier);

        if ($stmt->execute()) { 
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }


    // public function saveOrderTmp($id_user, $id_menu, $qty, $note_order) {
    //     $stmt = $this->conn->prepare("INSERT INTO order_tmp(id_user, id, qty, note_order) VALUES(?,?,?,?)");

    //     $stmt->bind_param("ssss", $id_user, $id_menu, $qty, $note_order);

    //     // $stmt->execute();

    //     if ($stmt->execute()) {
    //         // user existed 
    //         $stmt->close();
    //         return true;
    //     } else {
    //         // user not existed
    //         $stmt->close();
    //         return false;
    //     }
    // }


    public function deleteUserFromResto($idUser) {
        $stmt = $this->conn->prepare("UPDATE users SET UserID = NULL WHERE id_user = ?");

        $stmt->bind_param("s", $idUser);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function getLastIdMenu() {
        $database = "speed_resto";
        $table = "menu";

        $stmt = $this->conn->prepare("SELECT `AUTO_INCREMENT` FROM  INFORMATION_SCHEMA.TABLES WHERE TABLE_SCHEMA = ? AND TABLE_NAME = ?");
        $stmt->bind_param("ss", $database, $table);
        $hasil =-1;
        if ($stmt->execute()) {
            $result = $stmt->get_result();
            while ($row = $result->fetch_assoc()) {
                $hasil=$row["AUTO_INCREMENT"];
            }
            $stmt->free_result();
            $stmt->close();
            return $hasil;
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $hasil;            
        } 
    }

    public function updateUser1($id_user, $name, $email, $gender) {
		$dateNow = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("UPDATE users SET name = ?, email = ?, gender = ?, updated_at = ? WHERE id_user = ?");

        $stmt->bind_param("sssss", $name, $email, $gender, $dateNow, $id_user);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updateUser2($id_user, $name, $email, $gender, $password) {
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
		$dateNow = date('Y-m-d H:i:s');

        $stmt = $this->conn->prepare("UPDATE users SET name = ?, email = ?, gender = ?, encrypted_password = ?, salt = ?, updated_at = ? WHERE id_user = ?");

        $stmt->bind_param("sssssss", $name, $email, $gender, $encrypted_password, $salt, $dateNow, $id_user);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
	
	public function ResetPasswordUser($email, $password) {
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->conn->prepare("UPDATE users SET encrypted_password = ?, salt = ? WHERE email = ?");

        $stmt->bind_param("sss", $encrypted_password, $salt, $email);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
	
	
	public function ResetPasswordCustomer($email, $password) {
        $password_enc = md5($password);

        $stmt = $this->conn->prepare("UPDATE customer SET password = ? WHERE email = ?");

        $stmt->bind_param("ss", $password_enc, $email);


        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
	
	public function ResetPasswordResto($email, $password) {
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt

        $stmt = $this->conn->prepare("UPDATE restaurant SET encrypted_password = ?, salt = ? WHERE email = ?");

        $stmt->bind_param("sss", $encrypted_password, $salt, $email);


        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
	

    public function updateUser3($id_user, $path_photo) {
        $stmt = $this->conn->prepare("UPDATE users SET path_photo = ? WHERE id_user = ?");

        $stmt->bind_param("ss", $path_photo, $id_user);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }



    public function updateCustomer1($id_customer, $name, $email, $phone) {
        $stmt = $this->conn->prepare("UPDATE customer SET nama = ?, email = ?, hp = ? WHERE id_cust = ?");

        $stmt->bind_param("ssss", $name, $email, $phone, $id_customer);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updateCustomer2($id_customer, $name, $email, $phone, $password) {
        $password_enc = md5($password);

        $stmt = $this->conn->prepare("UPDATE customer SET nama = ?, email = ?, password = ?, hp = ? WHERE id_cust = ?");

        $stmt->bind_param("sssss", $name, $email, $password_enc, $phone, $id_customer);


        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updateCustomer3($id_cust, $path_photo) {
        $stmt = $this->conn->prepare("UPDATE customer SET path_photo = ? WHERE id_cust = ?");

        $stmt->bind_param("ss", $path_photo, $id_cust);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }

    public function updateCashier1($id_cashier, $name, $email) {
        $stmt = $this->conn->prepare("UPDATE cashier SET nama = ?, email = ? WHERE id_cashier = ?");

        $stmt->bind_param("sss", $name, $email, $id_cashier);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
    public function updateCashier2($id_cashier, $name, $email, $password) {
        $password_enc = md5($password);

        $stmt = $this->conn->prepare("UPDATE cashier SET nama = ?, email = ?, password = ? WHERE id_cashier = ?");

        $stmt->bind_param("ssss", $name, $email, $password_enc, $id_cashier);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }
    // public function checkCorrectPassword($email, $current_pass) {

    //     $stmt = $this->conn->prepare("SELECT email FROM users WHERE email = ?");

    //     $stmt->bind_param("s", $email);

    //     if ($stmt->execute()) {
    //         $user = $stmt->get_result()->fetch_assoc();
    //         $stmt->close();

    //         // verifying user password
    //         $salt = $user['salt'];
    //         $encrypted_password = $user['encrypted_password'];
    //         $hash = $this->checkhashSSHA($salt, $current_pass);
    //         // check for password equality
    //         if ($encrypted_password == $hash) {
    //             // user authentication details are correct
    //             return true;
    //         } else {
    //             return false;
    //         }
    //     } else {
    //         return false;
    //     }
    // }

    public function updateResto1($id_restaurant, $name, $email, $address, $keyword, $phone, $ig, $fb, $isOrderNotifOn, $allow) {

        $stmt = $this->conn->prepare("UPDATE restaurant SET NamaPerusahaan = ?, email = ?, Alamat = ?, keyword = ?, updated_at = ?, Telpon = ?, ig = ?, fb = ?, isOrderNotifOn = ?, allow = ? WHERE UserID = ?");
		$dateNow = date('Y-m-d H:i:s');

        $stmt->bind_param("sssssssssss", $name, $email, $address, $keyword, $dateNow, $phone, $ig, $fb, $isOrderNotifOn, $allow, $id_restaurant);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function checkCorrectPasswordResto($email, $current_pass) {

        $stmt = $this->conn->prepare("SELECT email FROM restaurant WHERE email = ?");

        $stmt->bind_param("s", $email);

        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();

            // verifying user password
            $salt = $user['salt'];
            $encrypted_password = $user['encrypted_password'];
            $hash = $this->checkhashSSHA($salt, $current_pass);
            // check for password equality
            if ($encrypted_password == $hash) {
                // user authentication details are correct
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }
	

    public function updateResto2($id_restaurant, $name, $email, $address, $keyword, $password, $phone, $ig, $fb, $isOrderNotifOn, $allow) {
        $hash = $this->hashSSHA($password);
        $encrypted_password = $hash["encrypted"]; // encrypted password
        $salt = $hash["salt"]; // salt
		$dateNow = date('Y-m-d H:i:s');
        $ignya = $ig;
        $fbnya = $fb;

        $stmt = $this->conn->prepare("UPDATE restaurant SET NamaPerusahaan = ?, email = ?, Alamat = ?, keyword = ?, encrypted_password = ?, salt = ?, updated_at = ?, Telpon = ?, ig = ?, fb = ?, isOrderNotifOn = ?, allow = ? WHERE UserID = ?");

        $stmt->bind_param("sssssssssssss", $name, $email, $address, $keyword, $encrypted_password, $salt, $dateNow, $phone, $ignya, $fbnya, $isOrderNotifOn, $allow, $id_restaurant);

        if ($stmt->execute()) {
            // user existed 
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }

    public function updateResto3($id_restaurant, $path_photo) {
        $stmt = $this->conn->prepare("UPDATE restaurant SET path_photo = ? WHERE UserID = ?");

        $stmt->bind_param("ss", $path_photo, $id_restaurant);


        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }
    public function updateResto4($id_restaurant, $path_photo) {
        $stmt = $this->conn->prepare("UPDATE restaurant SET logo = ? WHERE UserID = ?");

        $stmt->bind_param("ss", $path_photo, $id_restaurant);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }


    public function storeTransaction($idUser, $noTable, $totalBill, $tax, $service, $status, $idResto, $code, $id_menus, $qty_menus, $note_menus, $id_promos, $price_menus) {

        $stma = $this->conn->prepare("SELECT COUNT(*) as total, r.stts FROM transaction t, restaurant r WHERE t.UserID = ? AND r.UserID = t.UserID");
        $stma->bind_param("s", $idResto);
        $result_data["transaction"] = array();

        if ($stma->execute()) {
            $result = $stma->get_result();
            $stma->close(); 
            $row = $result->fetch_assoc();
            if ($row['total']>="20" && $row['stts']=="0") {
                $response["error"] = TRUE;
				$response["status"] = "0";
                $response["error_msg"] = "Thank You for using SpeedResto. Your Resto not yet registered, please contact Admin of SpeedResto in Whatsapp : 081236660688";
                echo json_encode($response);
            }
            else{
                //inti
				$date = date('Y-m-d H:i:s');
                $stmt = $this->conn->prepare("INSERT INTO transaction(id_user, no_table, total_bill, tax, service, status, UserID, date_order) VALUES(?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("ssssssss", $idUser, $noTable, $totalBill, $tax, $service, $status, $idResto,$date);
                $result = $stmt->execute();
                

                if ($result) {
                    $last_id = $this->conn->insert_id;
                    $ids=explode(",", $id_menus);
                    $qtys=explode(",", $qty_menus);
                    $notes=explode("{}}", $note_menus);
                    $promos=explode(",", $id_promos);
                    $prices=explode(",", $price_menus);
                    $i=0;
                    $q="INSERT INTO transaction_detail (id_transaction, id, qty, note, id_promo, harga) VALUES";
                        foreach($ids as $id_menu){
                            if($i==0){
                                 $q=$q." ($last_id, $id_menu, $qtys[$i], '$notes[$i]', $promos[$i], $prices[$i])";
                             }else {
                                 $q=$q.", ($last_id, $id_menu, $qtys[$i], '$notes[$i]', $promos[$i], $prices[$i])";
                             }
                            $i++;
                        }
                
                    $stmt1 = $this->conn->prepare($q);
                    if ($stmt1->execute()) {
                        $stmt1->close();

                        $stmtToken = $this->conn->prepare("SELECT token, os, isOrderNotifOn FROM restaurant WHERE UserID = ?");
                        $stmtToken->bind_param("s", $idResto);
                        if ($stmtToken->execute()) {
                            $CekToken = $stmtToken->get_result()->fetch_assoc();
                            $stmtToken->close();
                            if ($CekToken["token"] != "" && $CekToken["os"] == "1" && $CekToken["isOrderNotifOn"]=="1") {
                                require_once('firebase.php');
                                require_once('push.php');

                                $firebase = new Firebase();
                                $push = new Push();

                                    //payload ini data spesifik yg mau dikirim, misal data dirinya 
                                    //klo nggak ad biarin aj kayak gini
                                $payload = array();
                                $payload['init'] = "success";

                                $push->setNotifUntuk("createBooking");
                                $push->setTitle("SpeedResto");
                                $push->setMessage("New Transaction in Your Resto");
                                    // $push->setImage(''); //dikosongin kalo nggak makek image
                                $push->setImage('');
                                $push->setIsBackground(FALSE);
                                $push->setPayload($payload);

                                $json = '';
                                $response = '';

                                $json = $push->getPush();
                                $firebase->send($CekToken["token"],$json);
                            }
                            else if ($CekToken["token"] != "" && $CekToken["os"] == "2" && $CekToken["isOrderNotifOn"]=="1") {
                                require_once('firebase_ios.php');
                                require_once('push_ios.php');

                                $firebase = new Firebase();
                                $push = new Push();

                                    //payload ini data spesifik yg mau dikirim, misal data dirinya 
                                    //klo nggak ad biarin aj kayak gini
                                $payload = array();
                                $payload['init'] = "success";

                                $push->setNotifUntuk("createBooking");
                                $push->setTitle("SpeedResto");
                                $push->setMessage("New Transaction in Your Resto");
                                    // $push->setImage(''); //dikosongin kalo nggak makek image
                                $push->setImage('');
                                $push->setIsBackground(FALSE);
                                $push->setPayload($payload);

                                $json = '';
                                $response = '';

                                $json = $push->getPush();
                                $firebase->send($CekToken["token"],$json);
                            }
                        } 

                        $response["status"] = "1";
						$response["id_tr"] = $last_id;
						$response["pesan"] = "";
                        $response["error"] = FALSE;
                        return $response;
                    }
                    $stmt->close();
                } else {
                    return false;
                }
                //end inti

            }
                  
        }
        
    }
    public function getListTransactionPromo($id_promo)
    {
         $stmt = $this->conn->prepare("SELECT a.id_transaction, a.qty, b.type, b.date_order
            FROM transaction_detail a
            LEFT JOIN transaction b ON a.id_transaction = b.id_transaction
            WHERE a.id_promo =?
            ORDER BY b.date_order DESC ");
        $stmt->bind_param("s", $id_promo);
        $result_data["transaction"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;
            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["created"]=$row["date_order"];
                $data["qty"]=$row["qty"];
                $data["type"]=$row["type"];
                
                array_push($result_data['transaction'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }
    public function getListBill($id_resto) {
        $paid = "P";
        $date1 = date("Y-m-d")." 00:00:00";
        $date2 = date("Y-m-d")." 23:59:59";

        $stmt = $this->conn->prepare("SELECT * FROM transaction WHERE UserID = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
        $stmt->bind_param("sss", $id_resto, $date1, $date2);
        
        $result_data["list_user"] = array();
        $result_data["list_bill"] = array();
        $result_data["error"] = FALSE;
        $result_data["total_customers"] = 0;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["no_table"]=$row["no_table"];
                $data["total_bill"]=$row["total_bill"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["disc"]=$row["disc"];
                $data["status"]=$row["status"];
                $data["id_restaurant"]=$row["UserID"];
                $data["date_order"]=$row["date_order"];
                $data["disc_nota"]=$row["disc_nota"];
                $data["charge"]=$row["charge"];

                array_push($result_data['list_bill'], $data);
            }
            $stmt->free_result();
            $stmt->close();

            $stmt1 = $this->conn->prepare("SELECT * FROM users WHERE UserID = ?");
            $stmt1->bind_param("s", $id_resto);
            if ($stmt1->execute()) {
                $result = $stmt1->get_result();
                while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_user"]=$row["id_user"];
                $data["nama_user"]=$row["name"];

                array_push($result_data['list_user'], $data);
                }
                $stmt1->free_result();
                $stmt1->close();
                return $result_data; 
            } else {
                $result_data["error"] = TRUE;
                $stmt1->close();
                return $result_data;            
            }

            // $stmt2 = $this->conn->prepare("SELECT COUNT(UserID) AS total_customers FROM customer_detail WHERE UserID = ?");
            // $stmt2->bind_param("s", $id_resto);
            // if ($stmt2->execute()) {
            //     $user = $stmt2->get_result()->fetch_assoc();
            //     $stmt2->close();
            //     $result_data["total_customers"] = $user["total_customers"];
            //     return $result_data;
            // } else {
            //     $result_data["error"] = TRUE;
            //     $stmt2->close();
            //     return $result_data;
            // }

                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }


    public function getListTransactionCashier($id_cashier) {
        $paid = "P";
        $date1 = date("Y-m-d")." 00:00:00";
        $date2 = date("Y-m-d")." 23:59:59";

        $stmt = $this->conn->prepare("SELECT bh.id_h, bd.*, tr.* FROM bill_h bh, bill_d bd, transaction tr WHERE bh.id_cashier = ? AND bd.id_h = bh.id_h AND tr.id_transaction = bd.id_transaction AND tr.status = ? AND tr.date_order BETWEEN ? AND ? GROUP BY tr.id_transaction ORDER BY bh.id_h DESC");
        $stmt->bind_param("ssss", $id_cashier, $paid, $date1, $date2);
        $result_data["transaction"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["noTable"]=$row["no_table"];
                $data["total_bill"]=$row["total_bill"];
                $data["status"]=$row["status"];
                $data["date"]=$row["date_order"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["disc"]=$row["disc"];
                $data["tunai"]=$row["tunai"];
                $data["kartu"]=$row["kartu"];
                $data["kembali"]=$row["kembali"];
                $data["id_kartu"]=$row["id_kartu"];
                $data["disc_nota"]=$row["disc_nota"];
                $data["charge"]=$row["charge"];

                array_push($result_data['transaction'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }


    public function getListAddBillCashier($id_resto, $id_cashier) {
        $paid = "U";
        $date1 = date("Y-m-d")." 00:00:00";
        $date2 = date("Y-m-d")." 23:59:59";

        $stmt = $this->conn->prepare("SELECT a.*, b.tax as tax_r, b.service as service_r FROM transaction a, restaurant b WHERE a.UserID=b.UserID AND a.UserID = ? AND a.status = ? AND a.id_transaction NOT IN (SELECT id_transaction FROM bill_tmp WHERE id_cashier = ?) AND a.date_order BETWEEN ? AND ? ORDER BY a.no_table ASC");
        $stmt->bind_param("sssss", $id_resto, $paid, $id_cashier, $date1, $date2);
 
        $result_data["transaction"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["noTable"]=$row["no_table"];
                $data["status"]=$row["status"];
                $data["date"]=$row["date_order"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["disc"]=$row["disc"];
                $data["tunai"]=$row["tunai"];
                $data["kartu"]=$row["kartu"];
                $data["kembali"]=$row["kembali"];
                $data["id_kartu"]=$row["id_kartu"];
				$data["total_bill"]=$row["total_bill"];
                $data["metode_booking"]=$row["type"];
                $data["delivery_date"]=$row["delivery_date"];
                $data["delivery_time"]=$row["delivery_time"];
                $data["delivery_address"]=$row["delivery_address"];
				
				$tax_r = $row["tax_r"];
				$service_r = $row["service_r"];
				$total = ($data["total_bill"] - $data["disc"]);
				$service =($total*$service_r)/100;
				$tax = (($total+$service)*$tax_r)/100;
				

				$data["grand_bill"]=$total+$service+$tax;
				
                array_push($result_data['transaction'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }


    public function getListBillWithFilter($id_resto, $fromDate, $toDate, $id_user, $payment) {
        $paid = "P";
		
		if($id_user > 0 && $payment > 0)
		{
            //cash
			if($payment == "1")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu = 0 and c.tunai > 0 AND UserID = ? AND id_user = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
				$stmt->bind_param("ssss", $id_resto, $id_user, $fromDate, $toDate);
			}
            //card
			else if($payment == "2")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu > 0 and c.tunai = 0 AND UserID = ? AND id_user = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
				$stmt->bind_param("ssss", $id_resto, $id_user, $fromDate, $toDate);
			}
            //cash & card
			else if($payment == "3")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu > 0 and c.tunai > 0 AND UserID = ? AND id_user = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
				$stmt->bind_param("ssss", $id_resto, $id_user, $fromDate, $toDate);
			}
			else
			{
				$stmt = $this->conn->prepare("SELECT * FROM transaction WHERE UserID = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
				$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
			}
			
		}
		else if($id_user > 0)
		{
			$stmt = $this->conn->prepare("SELECT * FROM transaction WHERE UserID = ? AND id_user = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
			$stmt->bind_param("ssss", $id_resto, $id_user, $fromDate, $toDate);
		}
		else if($payment > 0)
		{
			//1=tunai
			//2=card
			//3=Cash and Card
			if($payment == "1")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu = 0 and c.tunai > 0 AND a.UserID = ? AND a.date_order BETWEEN ? AND ? GROUP BY a.id_transaction ORDER BY a.date_order DESC");
				$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
			}
			else if($payment == "2")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu > 0 and c.tunai = 0 AND a.UserID = ? AND a.date_order BETWEEN ? AND ? GROUP BY a.id_transaction ORDER BY a.date_order DESC");
				$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
			}
			else if($payment == "3")
			{
				$stmt = $this->conn->prepare("SELECT a.* FROM transaction a 
				INNER JOIN bill_d b ON a.id_transaction = b.id_transaction
				INNER JOIN bill_h c ON b.id_h = c.id_h
				WHERE c.id_kartu > 0 and c.tunai > 0 AND a.UserID = ? AND a.date_order BETWEEN ? AND ? GROUP BY a.id_transaction ORDER BY a.date_order DESC");
				$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
			}
			else
			{
				$stmt = $this->conn->prepare("SELECT * FROM transaction WHERE UserID = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
				$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
			}
		}
		else{
			$stmt = $this->conn->prepare("SELECT * FROM transaction WHERE UserID = ? AND date_order BETWEEN ? AND ? ORDER BY date_order DESC");
			$stmt->bind_param("sss", $id_resto, $fromDate, $toDate);
		}
		       

        $result_data["list_user"] = array();
        $result_data["list_bill"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["no_table"]=$row["no_table"];
                $data["total_bill"]=$row["total_bill"];
                $data["disc"]=$row["disc"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["status"]=$row["status"];
                $data["id_restaurant"]=$row["UserID"];
                $data["date_order"]=$row["date_order"];
                $data["disc_nota"]=$row["disc_nota"];
                $data["charge"]=$row["charge"];

                array_push($result_data['list_bill'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            $stmt1 = $this->conn->prepare("SELECT * FROM users WHERE UserID = ?");
            $stmt1->bind_param("s", $id_resto);
            if ($stmt1->execute()) {
                $result = $stmt1->get_result();
                while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_user"]=$row["id_user"];
                $data["nama_user"]=$row["name"];

                array_push($result_data['list_user'], $data);
                }
                $stmt1->free_result();
                $stmt1->close();
                return $result_data;
            } else {
                $result_data["error"] = TRUE;
                $stmt1->close();
                return $result_data;            
            }         
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }

    public function payBill($idTransaction) {
        $paid = "P";
        $stmt = $this->conn->prepare("UPDATE transaction SET status = ? WHERE id_transaction = ?");

        $stmt->bind_param("ss", $paid, $idTransaction);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }
	
	public function getTaxServiceDisc($id_resto) {

        $stmt = $this->conn->prepare("SELECT disc_order,tax,service FROM restaurant WHERE UserID = ?");

        $stmt->bind_param("s", $id_resto);

        if ($stmt->execute()) {
            $data = $stmt->get_result()->fetch_assoc();
            $stmt->close();

             return $data;
        } else {
             return NULL;
        }
    }
	
	
	public function getStatusAktif($id_resto, $id_user, $printer_cashier, $printer_kitchen, $printer_waiters) {

        $stmt = $this->conn->prepare("SELECT stts FROM restaurant WHERE UserID = ?");
        $stmt->bind_param("s", $id_resto);

        if ($stmt->execute()) {
            $Cek = $stmt->get_result()->fetch_assoc();
            $stmt->close();
			if ($Cek["stts"] == "1") {
                $stmt1 = $this->conn->prepare("UPDATE users SET print_kasir = ?, print_kitchen = ?, print_waiters = ? WHERE id_user = ?");
                $stmt1->bind_param("ssss", $printer_cashier, $printer_kitchen, $printer_waiters, $id_user);
                if ($stmt1->execute()) {
                    $stmt1->close();
                    $status = $Cek;
                    return $status;
                }
            }
            else {
                $status = $Cek;
                return $status;    
            }
        } else {
            return NULL;
        }
    }
	
	
	public function getListMenuDetailBookingWaiters($kode_booking, $id_resto, $dateTimeUser) {
		$tgl_up = date("Y-m-d H:i:s");
        $result_data["menu"] = array();
		$result_data["error"] = FALSE;
		$result_data["num_rows"] = 0;
		
		$CekCodeBook = $this->conn->prepare("SELECT id_h FROM book_h WHERE code='$kode_booking' AND stts=0 AND UserID ='$id_resto'");

        if ($CekCodeBook->execute()) {
            $resultCekKode = $CekCodeBook->get_result();
            $CekCodeBook->close(); 
			$row = $resultCekKode->fetch_assoc();
			if($row['id_h']!="")
			{
				$id_header =  $row['id_h'];
				
				$date_now = date("Y-m-d");
				$status_promo_false = "0";
				$status_promo_true = "1";
				$tgl_akhir_promo = "0000-00-00";
				$harga_promo ="0";
                $qty_habis ="0";

				$stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ?");
				$stmu->bind_param("sssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now);
				$stmu->execute();
				$stmu->close();

				$stmt = $this->conn->prepare("SELECT bh.*, bd.*, r.tax, r.service, m.* FROM book_h bh, book_d bd, restaurant r, menu m WHERE bh.id_h = '$id_header' AND r.UserID = ? AND bd.id_h = bh.id_h AND m.id = bd.id AND bh.stts =0 ORDER BY m.QtyStock DESC");
				$stmt->bind_param("s", $id_resto);
				
				

				if ($stmt->execute()) {   
					$result = $stmt->get_result();
					$result_data["num_rows"] = $result->num_rows;

					while ($row = $result->fetch_assoc()) {

						$data=array();
                        $data["id_booking"] = $id_header;
                        $data["id_menu"] = $row["id"];
						$data["name"]=$row["Keterangan"];
						$data["path_photo"]=$row["Gambar"];
						$data['note']=$row['note_order'];
						$data['price']=$row['HargaJual'];
						$data['price_promo']=$row['harga_promo'];
                        $data['description']=$row['Note'];
                        $data['status_print']=$row['status_print'];
                        $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                        $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                        $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                        $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                        $data["status_promo"]=$row["status_promo"];
                        //cek tgl jam berlaku promo
                        if ($data["status_promo"] == "1") {
                            $dateNow = new DateTime($dateTimeUser);
                            $dateUser = new DateTime($dateNow->format('Y-m-d'));
                            $timeUser = new DateTime($dateNow->format('H:i'));

                            $tglMulai = new DateTime($row['tgl_mulai_promo']);
                            $tglAkhir = new DateTime($row['tgl_akhir_promo']);
                            $waktuMulai = new DateTime($row['waktu_mulai_promo']);
                            $waktuAkhir = new DateTime($row['waktu_akhir_promo']);
                            
                            if (($dateUser >= $tglMulai) && ($dateUser <= $tglAkhir)) {
                                if (($timeUser >= $waktuMulai) && ($timeUser <= $waktuAkhir)) {
                                    $data["status_promo"]="1";
                                } else {
                                    $data["status_promo"]="0";
                                }
                            } else {
                                $data["status_promo"]="0";
                            }
                        }

						$data['qty']=$row['qty'];
						$data['tax_resto']=$row['tax'];
						$data['service_resto']=$row['service'];
						$data['disc_booking']=$row['disc'];

                        $data['id_cust']=$row['id_cust'];
                        $data['metode_booking']=$row['type'];
                        $data['delivery_date']=$row['delivery_date'];
                        $data['delivery_time']=$row['delivery_time'];
                        $data['delivery_address']=$row['delivery_address'];

                        if($data["status_promo"] == "1")
                        {
                            $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                            $history->bind_param("s", $row['id']);
                            $history->execute();

                            $resultHist = $history->get_result()->fetch_assoc();
                            $data["id_promo"]=$resultHist["id_promo_history"];
                        }
                        else
                        {
                            $data["id_promo"]="0";
                        }

						if ($row['QtyStock']=="1") {
							$data['status']="A";
						} elseif ($row['QtyStock']=="0") {
							$data['status']="U";
						}
						array_push($result_data['menu'], $data);
					}
					$stmt->free_result();
					$stmt->close();
					return $result_data;          
				} else {
					$result_data["error"] = TRUE;
					$stmt->close();
					return $result_data;            
				}
			}
			else
			{
				$result_data["error"] = TRUE;
				//$stmt->close();
				return $result_data;  
			}
			
			
		}
    }



    public function checkBookingCode($kode_booking, $id_resto) {
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;
        
        $CekCodeBook = $this->conn->prepare("SELECT id_h FROM book_h WHERE code='$kode_booking' AND stts=0 AND UserID ='$id_resto'");

        if ($CekCodeBook->execute()) {
            $resultCekKode = $CekCodeBook->get_result();
            $CekCodeBook->close(); 
            $row = $resultCekKode->fetch_assoc();
            if($row['id_h']!="") {
                return true;
            }
            else {
                return false;  
            }
        }
    }
	
	
	public function deleteDetailBooking($id_booking, $id_menu) {
        $stmt = $this->conn->prepare("DELETE FROM book_d WHERE id_h = ? AND id = ?");
        $stmt->bind_param("ss", $id_booking, $id_menu);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        }
        else {

            $stmt->close();
            return false;   
        }
    }

	
	public function storeTransactionFromBooking($idUser, $id_booking, $noTable, $totalBill, $tax, $service, $disc, $status, $idResto, $jenis, $metode_booking, $delivery_date, $delivery_time, $delivery_address, $id_cust) {

        $stma = $this->conn->prepare("SELECT COUNT(*) as total, r.stts FROM transaction t, restaurant r WHERE t.UserID = ? AND r.UserID = t.UserID");
        $stma->bind_param("s", $idResto);
        $result_data["transaction"] = array();

        if ($stma->execute()) {
            $result = $stma->get_result();
            $stma->close();  
            $row = $result->fetch_assoc();
            if ($row['total']>="20" && $row['stts']=="0") {
                $response["error"] = TRUE;
                $response["status"] = "0";
                $response["error_msg"] = "Terima Kasih sudah menggunakan SpeedResto. Resto Anda saat ini belum teraktivasi, mohon hubungi Admin SpeedResto melalui Whatsapp : 081236660688";
                echo json_encode($response);
            }
            else{
				$dateNow = date('Y-m-d H:i:s');
                //inti
                $stmt = $this->conn->prepare("INSERT INTO transaction(id_user, no_table, total_bill, tax, service, disc, status, UserID, date_order, jenis, type, delivery_date, delivery_time, delivery_address, id_cust) VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)");
                $stmt->bind_param("sssssssssssssss", $idUser, $noTable, $totalBill, $tax, $service, $disc, $status, $idResto, $dateNow, $jenis, $metode_booking, $delivery_date, $delivery_time, $delivery_address, $id_cust);
                $result = $stmt->execute();
                $stmt->close();

                if ($result) {
                    $last_id = $this->conn->insert_id;

                    $stmt1 = $this->conn->prepare("INSERT INTO transaction_detail (id_transaction, id, qty, id_promo) SELECT ?, id, qty, id_promo FROM book_d WHERE id_h = ?");
                    $stmt1->bind_param("ss", $last_id, $id_booking);
                    if ($stmt1->execute()) {
                        $stmt1->close();
						
						$stmu = $this->conn->prepare("UPDATE book_h SET stts = 1 WHERE id_h = ?");
						$stmu->bind_param("s", $id_booking);
						if($stmu->execute()){
							$stmu->close();
							
							$response["status"] = "1";
							$response["id_tr"] = $last_id;
							$response["pesan"] = "";
							$response["error"] = FALSE;
							return $response;
						}
                    }
                    $stmt->close();
                } else {
                    return false;
                }
                //end inti

            }
                  
        }
        
    }


    public function getHuhu($id_h) {
        // $date_now = "2017-06-30";
        // $date_now = date("Y-m-d");
        // $status_promo_false = "0";
        // $status_promo_true = "1";
        // $tgl_akhir_promo = "0000-00-00";
        // $harga_promo ="0"
        $result_print["cetak"] = array();
        $result_print["error"] = FALSE;
        ///Dari Booking
        $stmtBook = $this->conn->prepare("SELECT bd.id_transaction FROM bill_d bd, bill_h bh, transaction tr WHERE bd.id_transaction = tr.id_transaction  and bd.id_h=bh.id_h AND tr.jenis=1 AND bh.id_h=?");
        $stmtBook->bind_param("s", $id_h);
        $result_data["book"] = array();
        if ($stmtBook->execute()) {   
            $result = $stmtBook->get_result();

            while ($row = $result->fetch_assoc()) {

                $stmtDetailBook = $this->conn->prepare("SELECT th.total_bill, th.disc, th.type, th.delivery_date, th.delivery_time, th.delivery_address,  c.nama, c.hp, td.*, m.Keterangan, m.status_promo, m.HargaJual, m.harga_promo FROM transaction_detail td, menu m, transaction th, customer c WHERE td.id_transaction= th.id_transaction AND td.id_transaction = '$row[id_transaction]' AND m.id = td.id AND c.id_cust = th.id_cust");
                
                if ($stmtDetailBook->execute()) {   
                    $resultDetailBook = $stmtDetailBook->get_result();

                    while ($rowDetailBook = $resultDetailBook->fetch_assoc()) {
                        $data=array();
                        $data["id_menu"]=$rowDetailBook["id"];
                        $data["menuName"]=$rowDetailBook["Keterangan"];
                        $data["qty"]=$rowDetailBook["qty"];
                        $data["price"]=$rowDetailBook["HargaJual"];
                        $data['price_promo']=$rowDetailBook['harga_promo'];
                        $data['status_promo']=$rowDetailBook['status_promo'];
                        $data['total_bill']=$rowDetailBook['total_bill'];
                        $data['disc']=$rowDetailBook['disc'];
                        /*jika ada 2 pembayaran yang dipilih oleh kasir dan berbeda metode booking
                        maka ngambil metode booking pesanan yang akhir :v*/
                        $data['nama_cust']=$rowDetailBook['nama'];
                        $data['hp_cust']=$rowDetailBook['hp'];
                        $data['metode_booking']=$rowDetailBook['type'];
                        $data['delivery_date']=$rowDetailBook['delivery_date'];
                        $data['delivery_time']=$rowDetailBook['delivery_time'];
                        $data['delivery_address']=$rowDetailBook['delivery_address'];
                        
                        array_push($result_data['book'], $data);
                    }
                }
            }
            array_push($result_print['cetak'], $result_data);
            $stmtBook->free_result();
            $stmtBook->close();         
        } 
        ///Diskon Booking
        $stmtDiscBook = $this->conn->prepare("SELECT SUM(tr.disc) as total_disc, SUM(tr.total_bill) as total FROM bill_d bd, bill_h bh, transaction tr WHERE bd.id_transaction = tr.id_transaction  and bd.id_h=bh.id_h AND tr.jenis=1 AND bh.id_h=?");
        $stmtDiscBook->bind_param("s", $id_h);
        $result_data3["disc_book"] = array();
        if ($stmtDiscBook->execute()) {   
            $result = $stmtDiscBook->get_result();

            while ($row = $result->fetch_assoc()) {

                $data3=array();
                $data3["total_disc"]=$row["total_disc"];
                $data3["total"]=$row["total"];
                $data3["sub_total"]=($data3["total"]-$data3["total_disc"]);
                array_push($result_data3['disc_book'], $data3);
            }
            array_push($result_print['cetak'], $result_data3);
            $stmtDiscBook->free_result();
            $stmtDiscBook->close();         
        } 
        ///Dari Waiter
        $stmtWaiter = $this->conn->prepare("SELECT bd.id_transaction FROM bill_d bd, bill_h bh, transaction tr WHERE bd.id_transaction = tr.id_transaction and bd.id_h=bh.id_h AND tr.jenis=0 AND bh.id_h=?");
        $stmtWaiter->bind_param("s", $id_h);
        $result_data2["waiter"] = array();
        if ($stmtWaiter->execute()) {   
            $result2 = $stmtWaiter->get_result();

            while ($row2 = $result2->fetch_assoc()) {
                $stmtDetailWaiter = $this->conn->prepare("SELECT th.total_bill, th.disc, td.*, m.Keterangan, m.status_promo, m.HargaJual, m.harga_promo FROM transaction_detail td, menu m, transaction th WHERE td.id_transaction=th.id_transaction AND td.id_transaction = '$row2[id_transaction]' AND m.id = td.id");
                
                if ($stmtDetailWaiter->execute()) {   
                    $resultDetailWaiter = $stmtDetailWaiter->get_result();

                    while ($rowDetailWaiter = $resultDetailWaiter->fetch_assoc()) {
                        $data2=array();
                        $data2["id_menu"]=$rowDetailWaiter["id"];
                        $data2["menuName"]=$rowDetailWaiter["Keterangan"];
                        $data2["qty"]=$rowDetailWaiter["qty"];
                        $data2["price"]=$rowDetailWaiter["HargaJual"];
                        $data2['price_promo']=$rowDetailWaiter['harga_promo'];
                        $data2['status_promo']=$rowDetailWaiter['status_promo'];
                        $data2['total_bill']=$rowDetailWaiter['total_bill'];
                        $data2['disc']=$rowDetailWaiter['disc'];
                        
                        array_push($result_data2['waiter'], $data2);
                    }
                }
            }
            array_push($result_print['cetak'], $result_data2);
            $stmtWaiter->free_result();
            $stmtWaiter->close();
                    
        }
         ///Total dan Grantotal
        $stmtAkhir = $this->conn->prepare("SELECT SUM(tr.total_bill) as total_waiter, bh.total, bh.charge, bh.disc, (bh.tax+bh.service) as tax_service, (bh.tunai+bh.kartu) as bayar FROM bill_d bd, bill_h bh, transaction tr WHERE bd.id_transaction = tr.id_transaction  and bd.id_h=bh.id_h AND tr.jenis=0 AND bh.id_h=?");
        $stmtAkhir->bind_param("s", $id_h);
        $result_data4["akhir"] = array();
        if ($stmtAkhir->execute()) {   
            $result = $stmtAkhir->get_result();

            while ($row = $result->fetch_assoc()) {

                $data4=array();
                $data4["total_waiter"]=$row["total_waiter"];
                $data4["total"]=$row["total"];
                $data4["disc"]=$row["disc"];
                $data4["tax_service"]=$row["tax_service"];
                $data4["charge"]=$row["charge"];
                $data4["grant_total"]=($data4["total"]-$data4["disc"]+$data4["tax_service"]+$data4["charge"]);
                $data4["bayar"]=$row["bayar"];             
                $data4["kembali"]=($data4["bayar"]-$data4["grant_total"]);
                array_push($result_data4['akhir'], $data4);
            }
            array_push($result_print['cetak'], $result_data4);
            $stmtAkhir->free_result();
            $stmtAkhir->close();         
        } 
        return $result_print;  
    }



    public function updateCart($id_tr, $id_menu, $qty) {
        $stmt = $this->conn->prepare("UPDATE transaction_detail SET qty = ? WHERE id = ? AND    id_transaction = ?");
        $stmt->bind_param("sss", $qty, $id_menu, $id_tr);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            // user not existed
            $stmt->close();
            return false;
        }
    }


	public function savePaymentWaiter($id_tr, $no_bill, $total, $disc, $tunai, $tax, $service, $id_waiter, $disc_nota, $id_resto) {
        $date = date('Y-m-d H:i:s');
		$nol = "0";
		$satu = "1";
		
		$qCariKasir = $this->conn->prepare("SELECT a.id_cashier FROM cashier a, users b WHERE b.id_user= ? AND a.email = b.email AND a.UserID = ?");
        $qCariKasir->bind_param("ss", $id_waiter, $id_resto);
		
		 
                   
		if ($qCariKasir->execute()) {
			$resultCariKasir = $qCariKasir->get_result();
			if($resultCariKasir->num_rows > 0)
			{
				while ($row = $resultCariKasir->fetch_assoc()) {
					$id_kasir=$row["id_cashier"];
				}
			}
			else
			{
				$qDataWaiter = $this->conn->prepare("SELECT name, email FROM users WHERE id_user = ?");
				$qDataWaiter->bind_param("s", $id_waiter);
				if ($qDataWaiter->execute()) {
					$resultDataWaiter = $qDataWaiter->get_result();
					while ($rowWaiter = $resultDataWaiter->fetch_assoc()) {
						$namaWaiter=$rowWaiter["name"];
						$emailWaiter=$rowWaiter["email"];
						$passWaiter=md5($emailWaiter);
					}
				}
				$stmtNewKasir = $this->conn->prepare("INSERT INTO cashier(UserID, nama, email, password, created, status) VALUES(?, ?, ?, ?, ?, ?)");
				$stmtNewKasir->bind_param("ssssss", $id_resto, $namaWaiter, $emailWaiter, $passWaiter, $date, $satu);
				$resultNewKasir = $stmtNewKasir->execute();
				$stmtNewKasir->close();
				if ($resultNewKasir) { 
					$id_kasir = $this->conn->insert_id;
				}
			}
        }
        				
		
		
		//$id_kasir
		//SAVE PAYMENT
		$stmt = $this->conn->prepare("INSERT INTO bill_h (no_bill,total,disc,tunai,kartu,id_kartu,tax,service,charge,id_cashier, status_bayar) VALUES(?,?,?,?,?,?,?,?,?,?,?)");
        $stmt->bind_param("sssssssssss", $no_bill, $total, $disc, $tunai, $nol, $nol, $tax, $service, $nol, $id_kasir, $satu);
        
        if ($stmt->execute()) {
            $stmt->close();
            $last_id = $this->conn->insert_id;
            $stmt1 = $this->conn->prepare("INSERT INTO bill_d (id_h, id_transaction) VALUES (?, ?)");
            $stmt1->bind_param("ss", $last_id, $id_tr);
            if ($stmt1->execute()) {
                $stmt1->close();
                $status = "P";
				$tunai = $tunai;


                //SELECT dan update per Transaction
                $stmtDetailPay = $this->conn->prepare("SELECT a.id_transaction, a.total_bill,a.disc,b.tax,b.service FROM transaction a inner join restaurant b on a.UserID=b.UserID WHERE a.id_transaction = ?");
                $stmtDetailPay->bind_param("s", $id_tr);
                    
                $result_data["num_rows"] = 0;

                if ($stmtDetailPay->execute()) {   
                    $resultDetailPay = $stmtDetailPay->get_result();
                    $result_dataDetailPay["num_rows"] = $resultDetailPay->num_rows;
                    while ($rowDetailPay = $resultDetailPay->fetch_assoc()) {
                        $total_tr=$rowDetailPay["total_bill"];
                        $disc_tr=$rowDetailPay["disc"];
                        $service_rest=$rowDetailPay["service"];
                        $tax_rest=$rowDetailPay["tax"];
                            
                        $total_after_disc=($total_tr-$disc_tr); 
                        $disc_nota_tr = ($disc_nota>0) ? round((($total_after_disc*$disc_nota)/100), 2) : 0;
                        $service_tr = ($service_rest>0) ? round(((($total_after_disc-$disc_nota_tr)*$service_rest)/100), 2) : 0;
                        $tax_tr = ($tax_rest>0) ? round(((($total_after_disc-$disc_nota_tr+$service_tr)*$tax_rest)/100), 2) : 0;
                            
						$total_sebelum_charge = $total_after_disc-$disc_nota_tr+$service_tr+$tax_tr;
						$charge_tr=0;
							
                        $stmtUpDetail = $this->conn->prepare("UPDATE transaction SET status = ?, id_cashier = ? , disc_nota = ?, service = ?, tax = ?, charge = ? WHERE id_transaction = ?");
                        $stmtUpDetail->bind_param("sssssss", $status, $id_kasir, $disc_nota_tr, $service_tr, $tax_tr, $charge_tr, $id_tr);
                        $stmtUpDetail->execute();
                    }
                    return $last_id;
                }
				else
				{
					$stmt->close();
					return false;  
				}
            }
		}
    }



    public function getListTransactionWaiter($id_waiter) {
        $date = date("Y-m-d");
        $waiters = $id_waiter;

        /*$stmt = $this->conn->prepare("SELECT a.*, r.allow, bh.tunai, bh.kartu FROM transaction a inner join restaurant r ON a.UserID=r.UserID INNER JOIN bill_d bd ON a.id_transaction = bd.id_transaction INNER JOIN bill_h bh ON bd.id_h=bh.id_h
            WHERE a.id_user = ? AND date(a.date_order) = ? AND 
            a.id_transaction NOT IN (SELECT bd.id_transaction FROM bill_d bd INNER JOIN bill_h bh ON bd.id_h=bh.id_h WHERE date(bh.date_t)=?
            ) ORDER BY a.id_transaction DESC");*/

        /*$stmt = $this->conn->prepare("SELECT r.allow, bh.tunai, bh.kartu , tr . * 
        FROM transaction tr inner join restaurant r ON tr.UserID=r.UserID INNER JOIN bill_d bd ON tr.id_transaction = bd.id_transaction INNER JOIN bill_h bh ON bd.id_h=bh.id_h WHERE tr.id_user = ? AND bd.id_h = bh.id_h AND tr.id_transaction = bd.id_transaction AND date(tr.date_order) = ? GROUP BY tr.id_transaction ORDER BY bh.id_h DESC");*/
        $stmt = $this->conn->prepare("SELECT r.allow, tr . * 
        FROM transaction tr inner join restaurant r ON tr.UserID=r.UserID WHERE tr.id_user = ? AND date(tr.date_order) = ? ORDER BY tr.id_transaction DESC");

        $stmt->bind_param("ss", $waiters, $date);
        $result_data["transaction"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["noTable"]=$row["no_table"];
                $data["total_bill"]=$row["total_bill"];
                $data["status"]=$row["status"];
                $data["date"]=$row["date_order"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["disc"]=$row["disc"];
                $data["charge"]=$row["charge"];
                $data["jenis"]=$row["jenis"]; // 0=order biasa, 1=book
                $data["noTable"]=$row["no_table"]; //no table
                $data["allow"]=$row["allow"]; //allow


                $data["kembali"]=$row["kembali"];
                $data["id_kartu"]=$row["id_kartu"];
                $data["disc_nota"]=$row["disc_nota"];
                
                $data["tunai"]="0";
                $data["kartu"]="0";

                $stmtDetailPay = $this->conn->prepare("SELECT bh.tunai, bh.kartu FROM bill_h bh inner join bill_d bd on bh.id_h=bd.id_h WHERE bd.id_transaction = ?");
                $stmtDetailPay->bind_param("s", $row["id_transaction"]);
                if ($stmtDetailPay->execute()) {
                    $resultDetailPay = $stmtDetailPay->get_result();
                    $result_dataDetailPay["num_rows"] = $resultDetailPay->num_rows;
                    while ($rowDetailPay = $resultDetailPay->fetch_assoc()) {
                        
                        $data["tunai"]=$rowDetailPay["tunai"];
                        $data["kartu"]=$rowDetailPay["kartu"];
                    }

                }

                array_push($result_data['transaction'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }


    public function getListCartUpdate($id_tr, $id_waiter, $code) {
        
        //Hapus di TMP
        $stmtDelete = $this->conn->prepare("DELETE FROM order_tmp WHERE id_user = ?");
        $stmtDelete->bind_param("s", $id_waiter);
        $stmtDelete->execute();

        //Pindah Ke TMP
        $stmtTMP = $this->conn->prepare("SELECT td.id,m.Keterangan,td.qty,td.note,t.id_user FROM transaction_detail td INNER JOIN menu m ON td.id = m.id INNER JOIN transaction t ON td.id_transaction = t.id_transaction  WHERE t.id_transaction = ?");
        $stmtTMP->bind_param("s", $id_tr);
        
        if ($stmtTMP->execute()) {   
            $resultTMP = $stmtTMP->get_result();
           
            while ($rowTMP = $resultTMP->fetch_assoc()) {
                $id=$rowTMP["id"];
                $qty=$rowTMP["qty"];
                $note=$rowTMP["note"];
                $id_cust=$rowTMP["id_user"];

                //Save To TMP
                $stmtKeTMP = $this->conn->prepare("INSERT INTO order_tmp (id_user, id, qty, note_order, code) VALUES (?,?,?,?,?)");
                $stmtKeTMP->bind_param("sssss", $id_cust,$id,$qty,$note,$code);
                $stmtKeTMP->execute();
                
            }
            $stmtTMP->free_result();
            $stmtTMP->close();     
        } 



        $stmt = $this->conn->prepare("SELECT t.id,m.Keterangan,t.qty,t.note_order,m.status_print,m.Gambar,m.Note,m.HargaJual,m.status_promo,m.harga_promo FROM order_tmp t INNER JOIN menu m ON t.id = m.id WHERE t.id_user = ?");
        $stmt->bind_param("s", $id_waiter);

        $result_data["cart"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_menu"]=$row["id"];
                $data["menu_name"]=$row["Keterangan"];
                $data['qty']=$row['qty'];
                $data['note']=$row['note_order'];
                $data['description']=$row['Note'];
                $data['path_photo']=$row['Gambar'];
                $data['price']=$row['HargaJual'];
                $data['price_promo']=$row['harga_promo'];
                $data['status_promo']=$row['status_promo'];
                $data['status_print']=$row['status_print'];

                if($data["status_promo"] == "1")
                {
                    $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                    $history->bind_param("s", $row['id']);
                    $history->execute();

                    $resultHist = $history->get_result()->fetch_assoc();
                    $data["id_promo"]=$resultHist["id_promo_history"];
                }
                else
                {
                    $data["id_promo"]="0";
                }

                array_push($result_data['cart'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close(); 
            return $result_data;            
        }
        
    }


    

    public function voidUpdateTransaction($id_tr, $total_bill, $tax, $service,$disc, $id_waiter, $code) {

        $date = date('Y-m-d H:i:s');
        $stmt = $this->conn->prepare("UPDATE transaction SET total_bill=?, tax=?, service=?, disc=? WHERE id_transaction=?");
        $stmt->bind_param("sssss", $total_bill, $tax, $service, $disc, $id_tr);
        $result = $stmt->execute();
        $stmt->close();

        if ($result) {
            $stmt1 = $this->conn->prepare("UPDATE transaction_detail td, (SELECT id, qty, note_order FROM order_tmp where id_user=? AND code=?) tmp SET td.qty = tmp.qty, td.note=tmp.note_order where td.id_transaction=? AND td.id=tmp.id");
            $stmt1->bind_param("sss", $id_waiter, $code, $id_tr);
            if ($stmt1->execute()) {
                $stmt1->close();

                $stmtCekPaid = $this->conn->prepare("SELECT status,total_bill from transaction WHERE id_transaction = ?");
                $stmtCekPaid->bind_param("s", $id_tr);
                if ($stmtCekPaid->execute()) {
                    $resultCekPaid = $stmtCekPaid->get_result();
                    $stmtCekPaid->close(); 
                    $rowPaid = $resultCekPaid->fetch_assoc();
                    if($rowPaid['status']=="P") { 
                        //Sudah Bayar
                        $response["status"] = "P";
                        $response["total"] = $rowPaid['total_bill'];
                    }
                    else
                    {
                        $response["status"] = "U";
                        $response["total"] = "0";
                    }
                }

                $response["error"] = FALSE;
                return $response;
            }
            else
            {
                return false;
            }
            $stmt->close();
        } else {
            return false;
        }      
    }






    public function VoidUpdatePaymentWaiter($id_tr, $total, $disc, $tunai, $tax, $service, $disc_nota) {
        $date = date('Y-m-d H:i:s');
        $nol = 0;
        $satu = 1;
        
        //Cek dulu apa sudah bayar
        $stmtCekBayar = $this->conn->prepare("SELECT id_h from bill_d WHERE id_transaction = ?");
        $stmtCekBayar->bind_param("s", $id_tr);
        if ($stmtCekBayar->execute()) {
            $resultCekBayar = $stmtCekBayar->get_result();
            $stmtCekBayar->close(); 
            $row = $resultCekBayar->fetch_assoc();
            if($row['id_h']!="") { //Sudah Bayar
            

                $id_bill_h = $row['id_h'];
                //update PAYMENT
                $stmt = $this->conn->prepare("UPDATE bill_h set total=?, disc=?, tunai=?, tax=?, service=? WHERE id_h=?");
                $stmt->bind_param("ssssss", $total, $disc, $tunai, $tax, $service, $id_bill_h);
                //SELECT dan update per Transaction
                $stmtDetailPay = $this->conn->prepare("SELECT a.id_transaction, a.total_bill,a.disc,b.tax,b.service FROM transaction a inner join restaurant b on a.UserID=b.UserID WHERE a.id_transaction = ?");
                $stmtDetailPay->bind_param("s", $id_tr);
                    
                $result_data["num_rows"] = 0;

                if ($stmtDetailPay->execute()) {   
                    $resultDetailPay = $stmtDetailPay->get_result();
                    $result_dataDetailPay["num_rows"] = $resultDetailPay->num_rows;
                    while ($rowDetailPay = $resultDetailPay->fetch_assoc()) {
                        $total_tr=$rowDetailPay["total_bill"];
                        $disc_tr=$rowDetailPay["disc"];
                        $service_rest=$rowDetailPay["service"];
                        $tax_rest=$rowDetailPay["tax"];
                            
                        $total_after_disc=($total_tr-$disc_tr); 
                        $disc_nota_tr = ($disc_nota>0) ? round((($total_after_disc*$disc_nota)/100), 2) : 0;
                        $service_tr = ($service_rest>0) ? round(((($total_after_disc-$disc_nota_tr)*$service_rest)/100), 2) : 0;
                        $tax_tr = ($tax_rest>0) ? round(((($total_after_disc-$disc_nota_tr+$service_tr)*$tax_rest)/100), 2) : 0;
                            
                        $total_sebelum_charge = $total_after_disc-$disc_nota_tr+$service_tr+$tax_tr;
                        $charge_tr=0;
                            
                        $stmtUpDetail = $this->conn->prepare("UPDATE transaction SET disc_nota = ?, service = ?, tax = ?, charge = ? WHERE id_transaction = ?");
                        $stmtUpDetail->bind_param("sssss", $disc_nota_tr, $service_tr, $tax_tr, $charge_tr, $id_tr);
                        $stmtUpDetail->execute();
                    }
                    $stmt->execute();
                    $result_data["last_id"] - $last_id;
                    $result_data["error"] = false;
                    return $result_data;
                }
                else
                {
                    $stmt->close();
                    $result_data["error"] = true;
                    return $result_data;
                }
            }
        }
        else
        {
            $stmt->close();
            return false;
        }
    }



	public function updateToken($token, $userID, $status_user) {
        if ($status_user == "resto") {
            $stmt = $this->conn->prepare("UPDATE restaurant set token=?, os='1' WHERE UserID=?");
        } else if ($status_user == "customer"){
            $stmt = $this->conn->prepare("UPDATE customer set token=?, os='1' WHERE id_cust=?");
        } else if ($status_user == "cashier"){
            $stmtDel = $this->conn->prepare("UPDATE cashier c INNER JOIN (Select UserID FROM cashier WHERE id_cashier=?) m set c.token='', c.os='0' WHERE m.UserID=c.UserID");
            $stmtDel->bind_param("s", $userID);
            $stmtDel->execute();

            $stmt = $this->conn->prepare("UPDATE cashier set token=?, os='1' WHERE id_cashier=?");
        }

        $stmt->bind_param("ss", $token, $userID);

        if ($stmt->execute()) {
            $stmt->close();
            return true;
        } else {
            $stmt->close();
            return false;
        }
    }


    public function getStatusMenu($id_menu, $tgl, $waktu) {
        $stmt = $this->conn->prepare("SELECT COUNT(NoItem) as status, ifnull( harga_promo, 0) as harga_promo FROM menu WHERE id = ? AND (tgl_mulai_promo <= ? AND tgl_akhir_promo >= ?) AND (waktu_mulai_promo <= ? AND waktu_akhir_promo >= ?)");

        $stmt->bind_param("sssss", $id_menu, $tgl, $tgl, $waktu, $waktu);

        if ($stmt->execute()) {
            $stts = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $stts;
        } else {
            return NULL;
        }
    }


    public function getKategori($id_resto, $fromCust) {
        if($fromCust=='0')//dari resto
        {
            $stmt = $this->conn->prepare("SELECT * FROM kategori WHERE UserID = ? ORDER BY id_kat DESC");
        }
        else //dari customer
        {
            $stmt = $this->conn->prepare("SELECT * FROM kategori WHERE UserID = ? AND status='1' ORDER BY id_kat DESC");
        }
        
        $stmt->bind_param("s", $id_resto);
        
        $result_data["kategori"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {
                $data=array();
                $data["id_kategori"]=$row["id_kat"];
                $data["kategori"]=$row["kat"];
                $data["status"]=$row["status"];
                $data["statusT"]=($row["status"] =='0' ) ? 'Hide' : 'Show';
                array_push($result_data['kategori'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }


    public function addKategori($id_resto, $kategori) {
        $status = '1';
        $stmt = $this->conn->prepare("INSERT INTO kategori (kat, status, UserID) VALUES (?,?,?)");
        $stmt->bind_param("sss", $kategori, $status, $id_resto);
        
        $result_data["kat"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
                $stmt1 = $this->conn->prepare("SELECT * FROM kategori WHERE UserID = ? AND status='1' ORDER BY id_kat DESC");
                $stmt1->bind_param("s", $id_resto);

                if ($stmt1->execute()) {   
                    $result = $stmt1->get_result();
                    $result_data["num_rows"] = $result->num_rows;

                    while ($row = $result->fetch_assoc()) {
                        $data=array();
                        $data["id_kategori"]=$row["id_kat"];
                        $data["kategori"]=$row["kat"];
                        array_push($result_data['kat'], $data);
                    }
                    $stmt1->free_result();
                    $stmt1->close();
                    return $result_data;          
                } else {
                    $result_data["error"] = TRUE;
                    $stmt1->close();
                    return $result_data;            
                }
            $stmt->free_result();
            $stmt->close();     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }



    public function editKategori($id_kategori, $kategori, $status) {
        $stmt = $this->conn->prepare("UPDATE kategori SET kat = ?, status = ? WHERE id_kat = ?");
        $stmt->bind_param("sss", $kategori, $status, $id_kategori);

        $result_data["error"] = FALSE;

        if ($stmt->execute()) {   
            $stmt->close();     
            return $result_data; 
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
        }
    }

    
    public function getListMenuByCategory($id_resto, $isFromAdmin, $dateTimeUser, $id_kategori) {
        $tgl_up = date("Y-m-d H:i:s");
        $date_now = date("Y-m-d");
        $status_promo_false = "0";
        $status_promo_true = "1";
        $tgl_akhir_promo = "0000-00-00";
        $harga_promo ="0";
        $qty_habis ="0";

        $stmu = $this->conn->prepare("UPDATE menu m INNER JOIN restaurant r ON m.UserID=r.UserID SET m.status_promo = ?, m.tgl_akhir_promo = ?, m.harga_promo = ?, r.menu_up=? WHERE m.UserID = ? AND m.status_promo = ? AND m.tgl_akhir_promo < ? AND m.id_kategori=?");
        $stmu->bind_param("ssssssss", $status_promo_false, $tgl_akhir_promo, $harga_promo, $tgl_up, $id_resto, $status_promo_true, $date_now, $id_kategori);
        $stmu->execute();
        $stmu->close();

        $stmt = $this->conn->prepare("SELECT a.*, b.kat FROM menu a INNER JOIN kategori b ON a.id_kategori=b.id_kat WHERE a.aktif='1' AND a.UserID = ? AND a.id_kategori=? ORDER BY a.QtyStock DESC, a.id_kategori ASC, a.Keterangan ASC");
        $stmt->bind_param("ss", $id_resto, $id_kategori);
        $result_data["menu"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;
        //get disc, tax, service
        $disct = $this->conn->prepare("SELECT disc_order,tax,service,ig,fb,logo FROM restaurant WHERE UserID = ?");
        $disct->bind_param("s", $id_resto);
        if ($disct->execute()) {
            $row2 = $disct->get_result()->fetch_assoc();
            $result_data["disc"] = $row2['disc_order'];
            $result_data["tax"] = $row2['tax'];
            $result_data["service"] = $row2['service'];
            $result_data["ig"] =  $row2['ig'];
            $result_data["fb"] =  $row2['fb'];
            $result_data["logo"] =  $row2['logo'];
            $disct->free_result();
            $disct->close();
            if ($stmt->execute()) {   
                $result = $stmt->get_result();
                $result_data["num_rows"] = $result->num_rows;

                while ($row = $result->fetch_assoc()) {

                    $data=array();
                    $data["id_menu"]=$row["id"];
                    $data["name"]=$row["Keterangan"];
                    $data["price"]=$row["HargaJual"];
                    $data["path_photo"]=$row["Gambar"];
                    $data['harga_promo']=$row['harga_promo'];
                    $data['tgl_mulai_promo']=$row['tgl_mulai_promo'];
                    $data['tgl_akhir_promo']=$row['tgl_akhir_promo'];
                    $data['waktu_mulai_promo']=$row['waktu_mulai_promo'];
                    $data['waktu_akhir_promo']=$row['waktu_akhir_promo'];
                    $data['description']=$row['Note'];
                    $data['status_print']=$row['status_print'];
                    $data["status_promo"]=$row['status_promo'];

                    if($data["status_promo"] == "1")
                    {
                        $history = $this->conn->prepare("SELECT id_promo_history FROM promo_history WHERE id=? ORDER BY id_promo_history DESC LIMIT 1");
                        $history->bind_param("s", $row['id']);
                        $history->execute();

                        $resultHist = $history->get_result()->fetch_assoc();
                        $data["id_promo"]=$resultHist["id_promo_history"];
                    }
                    else
                    {
                        $data["id_promo"]="0";
                    }

                    if ($isFromAdmin == "no") {
                        //cek tgl jam berlaku promo
                        if ($data["status_promo"] == "1") {
                            $dateNow = new DateTime($dateTimeUser);
                            $dateUser = new DateTime($dateNow->format('Y-m-d'));
                            $timeUser = new DateTime($dateNow->format('H:i'));

                            $tglMulai = new DateTime($row['tgl_mulai_promo']);
                            $tglAkhir = new DateTime($row['tgl_akhir_promo']);
                            $waktuMulai = new DateTime($row['waktu_mulai_promo']);
                            $waktuAkhir = new DateTime($row['waktu_akhir_promo']);
                            
                            if (($dateUser >= $tglMulai) && ($dateUser <= $tglAkhir)) {
                                if (($timeUser >= $waktuMulai) && ($timeUser <= $waktuAkhir)) {
                                    $data["status_promo"]="1";
                                } else {
                                    $data["status_promo"]="0";
                                }
                            } else {
                                $data["status_promo"]="0";
                            }
                        }
                    }

                    $data["kategori"] = $row['kat'];
                    $data["id_kategori"] = $row['id_kategori'];

                    if ($row['QtyStock']=="1") {
                        $data['status']="A";
                    } elseif ($row['QtyStock']=="0") {
                        $data['status']="U";
                    }

                    array_push($result_data['menu'], $data);
                }
                $stmt->free_result();
                $stmt->close();
                return $result_data;          
            } else {
                $result_data["error"] = TRUE;
                $stmt->close();
                return $result_data;            
            }
        }else{
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;          
        }
    }


    public function getOrderByResto($id_resto) {
        $date = date("Y-m-d");
        $id_resto = $id_resto;

        $stmt = $this->conn->prepare("SELECT tr.* FROM transaction tr WHERE tr.UserID = ? AND date(tr.date_order) = ? AND tr.status='U' ORDER BY tr.id_transaction DESC");

        $stmt->bind_param("ss", $id_resto, $date);
        $result_data["transaction"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_transaction"]=$row["id_transaction"];
                $data["id_user"]=$row["id_user"];
                $data["noTable"]=$row["no_table"];
                $data["total_bill"]=$row["total_bill"];
                $data["status"]=$row["status"];
                $data["date"]=$row["date_order"];
                $data["tax"]=$row["tax"];
                $data["service"]=$row["service"];
                $data["disc"]=$row["disc"];
                $data["charge"]=$row["charge"];
                $data["jenis"]=$row["jenis"]; // 0=order biasa, 1=book
                $data["noTable"]=$row["no_table"]; //no table
                
                array_push($result_data['transaction'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;
                     
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
    }



    public function sendToCashier($id_tr, $id_resto) {
        $stmt = $this->conn->prepare("SELECT token FROM cashier WHERE UserID=? AND token !='' AND os='1'");
        $stmt->bind_param("s", $id_resto);
        if ($stmt->execute()) {

            $CekToken = $stmt->get_result()->fetch_assoc();
            $TokenKasir = $CekToken["token"];
            $NoMeja = '0';
            $Type = 'In Resto';
            $dataPesanan["transaction"] = array();
            $dataPesanan["error"] = FALSE;
            $dataPesanan["num_rows"] = 0;

            $stmtDetail = $this->conn->prepare("SELECT h.no_table, h.type, m.Keterangan, t.qty, t.note FROM menu m INNER JOIN transaction_detail t ON m.id=t.id INNER JOIN transaction h ON t.id_transaction=h.id_transaction WHERE t.id_transaction=?");
            $stmtDetail->bind_param("s", $id_tr);
            if ($stmtDetail->execute()) {
                $result = $stmtDetail->get_result();
                $dataPesanan["num_rows"] = $result->num_rows;
                if($result->num_rows > 0)
                {
                    while ($row = $result->fetch_assoc()) {
                        $data=array();
                        $data["nama"]=$row["Keterangan"];
                        $data["qty"]=$row["qty"];
                        $data["note"]=$row["note"];
                        $NoMeja = $row["no_table"];
                        $Type = ($row["type"] == '1') ? 'In Resto' : (($row["type"] == '2') ? 'Take Away' : 'Delivery');
                        
                        array_push($dataPesanan['transaction'], $data);
                    }
                    $dataPesanan['meja'] = $NoMeja;
                    $dataPesanan['type'] = $Type;


                    require_once('firebase.php');
                    require_once('push.php');

                    $firebase = new Firebase();
                    $push = new Push();

                    $payload = array();
                    $payload['init'] = "success";

                    $push->setNotifUntuk("newOrder");
                    $push->setTitle("SpeedResto");
                    $push->setMessage($dataPesanan);
                    
                    $push->setImage('');
                    $push->setIsBackground(FALSE);
                    $push->setPayload($payload);

                    $json = '';
                    $response = '';

                    $json = $push->getPush();
                    $firebase->send($TokenKasir,$json);

                    $stmtDetail->close();
                    return true;
                }
                else
                {
                    $stmtDetail->close();
                    return false;
                }
            }
            else
            {
                //$dataPesanan["error"] = TRUE;
                $stmtDetail->close();
                return false;
            }
        }
        else {
            $stmt->close();
            return false;   
        }
    }



    public function CheckMenuUpdate($id_resto, $waktu) {
        $result_data["error"] = FALSE;
        $result_data["update"] = TRUE;
        
        $CekCodeBook = $this->conn->prepare("SELECT menu_up, tax, service FROM restaurant WHERE UserID='$id_resto'");

        if ($CekCodeBook->execute()) {
            $resultCekKode = $CekCodeBook->get_result();
            $CekCodeBook->close(); 
            $row = $resultCekKode->fetch_assoc();
            if($row['menu_up']!=$waktu)
            {
                $result_data["update"] = TRUE;
                $result_data["waktubaru"] = $row['menu_up'];
                $result_data["tax"] = $row['tax'];
                $result_data["service"] = $row['service'];
                return $result_data;
            }
            else
            {
                $result_data["update"] = FALSE;
                $result_data["tax"] = $row['tax'];
                $result_data["service"] = $row['service'];
                return $result_data;  
            }
            
            
        }
    }


    public function getRestoList($id_customer) {
        // $date_now = "2017-06-30";
        //$key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT r.UserID, r.NamaPerusahaan, r.alamat, r.Telpon, r.path_photo, r.service, r.tax, r.disc_order FROM restaurant r ORDER BY r.NamaPerusahaan ASC");
        //$stmt->bind_param("s", $id_customer);
        
        $result_data["resto"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_resto"]=$row["UserID"];
                $data["name"]=$row["NamaPerusahaan"];
                $data["address"]=$row["alamat"];
                $data["service"]=$row["service"];
                $data["tax"]=$row["tax"];
                $data["phone"]=$row["Telpon"];
                $data["path_photo"]=$row["path_photo"];
                $data['disc'] = $row['disc_order'];
                array_push($result_data['resto'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }
    public function getRestoListById($idResto) {
        // $date_now = "2017-06-30";
        //$key = "%".$keyword."%";
        $stmt = $this->conn->prepare("SELECT r.UserID, r.NamaPerusahaan, r.alamat, r.Telpon, r.path_photo, r.service, r.tax, r.disc_order FROM restaurant r where r.UserID = '$idResto' ORDER BY r.NamaPerusahaan ASC");
        //$stmt->bind_param("s", $id_customer);
        
        $result_data["resto"] = array();
        $result_data["error"] = FALSE;
        $result_data["num_rows"] = 0;

        if ($stmt->execute()) {   
            $result = $stmt->get_result();
            $result_data["num_rows"] = $result->num_rows;

            while ($row = $result->fetch_assoc()) {

                $data=array();
                $data["id_resto"]=$row["UserID"];
                $data["name"]=$row["NamaPerusahaan"];
                $data["address"]=$row["alamat"];
                $data["service"]=$row["service"];
                $data["tax"]=$row["tax"];
                $data["phone"]=$row["Telpon"];
                $data["path_photo"]=$row["path_photo"];
                $data['disc'] = $row['disc_order'];
                array_push($result_data['resto'], $data);
            }
            $stmt->free_result();
            $stmt->close();
            return $result_data;          
        } else {
            $result_data["error"] = TRUE;
            $stmt->close();
            return $result_data;            
        }
        
    }


    public function CekKodeResto($kodeResto) {
        
        $CekCode = $this->conn->prepare("SELECT UserID, NamaPerusahaan FROM restaurant WHERE kode='$kodeResto'");

        if ($CekCode->execute()) {
            $resultKode = $CekCode->get_result();
            $CekCode->close(); 
            
            if($resultKode->num_rows > 0)
            {
                $row = $resultKode->fetch_assoc();
                $result_data["UserID"] = $row['UserID'];
                $result_data["NamaPerusahaan"] = $row['NamaPerusahaan'];
                
                return $result_data;
            }
            else
            {
                $result_data["UserID"] = '';
                $result_data["NamaPerusahaan"] = '';
                return $result_data;  
            }
            
            
        }
    }



}

?>