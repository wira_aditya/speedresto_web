<?php
 
/**
 * @author Ravi Tamada
 * @link URL Tutorial link
 */
class Push {
    

    // push message title
    private $click_action="";
    private $notif_untuk;
    private $title;
    private $text;
    private $message;
    private $image;
    // push message payload
    private $data;
    // flag indicating whether to show the push
    // notification or not
    // this flag will be useful when perform some opertation
    // in background when push is recevied
    private $is_background;
 
    function __construct() {
         
    }
    public function setClickAction($clickAction) {
        $this->click_action = $clickAction;
    }

    public function setNotifUntuk($notifUntuk) {
        $this->notif_untuk = $notifUntuk;
    }

    public function setTitle($title) {
        $this->title = $title;
    }
 
    public function setMessage($message) {
        $this->message = $message;
    }
 
    public function setImage($imageUrl) {
        $this->image = $imageUrl;
    }
 
    public function setPayload($data) {
        $this->data = $data;
    }
 
    public function setIsBackground($is_background) {
        $this->is_background = $is_background;
    }
 
    public function getPush() {
        $res = array();
        $res['title'] = $this->title;
        $res['text'] = $this->message;
        $res['sound'] = "default";
        $res['vibrate'] = 1;
        $res['badge'] = 0;
        
        return $res;
    }
 
}
?>