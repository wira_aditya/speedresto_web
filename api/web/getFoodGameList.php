<?php
 header('Access-Control-Allow-Origin: *');
 header('Access-Control-Allow-Headers: Content-type,Authorization,X-Requested-With');
require_once 'include/DB_Functions.php';
$db = new DB_Functions(); 
// json response array
$response = array("error" => FALSE); 
 
if (isset($_POST['id_customer'])) {
 
    if(($_POST['id_resto']=='' || $_POST['id_resto']=='0') && ($_POST['kode_resto']=='' || $_POST['kode_resto']=='0'))
    {
        $response["error"] = TRUE;
        $response["error_msg"] = "Required parameters (id resto and keyword) is missing!";
        echo json_encode($response);
    } 
    else
    {
        $id_customer = ($_POST['id_customer']!='0') ? $_POST['id_customer'] : '0';
        $kodeResto = $_POST['kode_resto'];
        $id_resto='';
        $nama_resto='';
        //$dateTimeUser = $_POST['dateTimeUser'];

        if($_POST['id_resto'] =='' ||  $_POST['id_resto']=='0')
        {
            $resultKode = $db->CekKodeResto($kodeResto);
            $id_resto=$resultKode["UserID"];
            $nama_resto=$resultKode["NamaPerusahaan"];
            if($id_resto=='')
            {
                $response["error"] = TRUE;
                $response["error_msg"] = "Resto Code Not Register";
                echo json_encode($response);
                die();
            }
        }
        else
        {
            $id_resto=$_POST['id_resto'];
            $nama_resto=$_POST['nama_resto'];
        }


        $result = $db->getFoodGameList($id_customer, $id_resto);

        if ($result["error"] == FALSE) {
            $result["id_resto"] = $id_resto;
            $result["nama_resto"] = $nama_resto;
            echo json_encode($result); 
        } else {
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknow error while get list menu!";
            echo json_encode($response);
        }
    }

} else {
    $response["error"] = TRUE;
    $response["error_msg"] = "Required parameters (id resto and keyword) is missing!";
    echo json_encode($response);
}
?>