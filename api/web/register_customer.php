<?php
header('Access-Control-Allow-Origin: *');
header('Access-Control-Allow-Headers: Content-type,Authorization,X-Requested-With');
$headers = apache_request_headers();

if($headers['Host'] == '192.168.0.108') {

    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
     
    // json response array
    $response = array("error" => FALSE);
     
    if (isset($_POST['name']) && isset($_POST['email'])) {
     
        // receiving the post params
        $name = $_POST['name'];
        $email = $_POST['email'];

       
        if ($db->isUserExistedResto($email)) {
            // user already existed
            $response["error"] = TRUE;
            $response["error_msg"] = "This email as Resto already existed with " . $email;
            echo json_encode($response);
        } else {

            if ($db->isUserExistedCashier($email)) {
                // user already existed
                $response["error"] = TRUE;
                $response["error_msg"] = "This email as Cashier already existed with " . $email;
                echo json_encode($response);
            }
            else {
                // create a new user
                $user = $db->storeCustomer($name, $email);
                if ($user) {
                    // user stored successfully
                    $response["error"] = FALSE;
                    $response["user"]["id_customer"] = $user["id_cust"];
                    $response["user"]["name"] = $user["nama"];
                    $response["user"]["email"] = $user["email"];
                    $response["user"]["phone"] = $user["hp"];
                    $response["user"]["created_at"] = $user["created"];
                    $response["user"]["status"] = $user["status"];
                    echo json_encode($response);
                } else {
                    // user failed to store
                    $response["error"] = TRUE;
                    $response["error_msg"] = "Unknown error occurred in registration!";
                    echo json_encode($response);
                }
            }
        }   
        

    } else {
        $response["error"] = TRUE;
        $response["error_msg"] = "Required parameters (name resto, address, phone, email, or password) is missing!";
        echo json_encode($response);
    }
}
else
{
    $response["error"] = TRUE;
    $response["error_msg"] = "Missing Connection";
    echo json_encode($response);
}
?>