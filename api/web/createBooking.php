<?php
$headers = apache_request_headers();
header('Access-Control-Allow-Origin: http://localhost:8080');
header('Access-Control-Allow-Headers: Content-type,Authorization,X-Requested-With');

if($headers['Host'] == '192.168.0.108') {
    require_once 'include/DB_Functions.php';
    $db = new DB_Functions();
      
    // json response array
    $response = array("error" => FALSE);
     
    if (isset($_POST['id_customer']) && isset($_POST['id_resto']) && isset($_POST['disc']) && isset($_POST['metode_booking']) && isset($_POST['date']) && isset($_POST['time']) && isset($_POST['address'])) {
     
        // receiving the post params
        $id_customer = $_POST['id_customer'];
        $id_resto = $_POST['id_resto'];
        $disc = $_POST['disc'];
        $metode_booking = $_POST['metode_booking'];
        $date = $_POST['date'];
        $time = $_POST['time'];
        $address = $_POST['address'];

        $id_menus=$_POST['id_menu'];
        $qty_menus=$_POST['qty_menu'];
        $note_menus=$_POST['note_menu'];
        $id_promos=$_POST['id_promo'];
        $price_menus=$_POST['price_menu'];


     
        $result = $db->createBooking($id_customer, $id_resto, $disc, $metode_booking, $date, $time, $address, $id_menus, $qty_menus, $note_menus, $id_promos, $price_menus);
        if($result==='lebih') {
            $response["error"] = TRUE;
            $response["error_msg"] = "Your book quantity more than promo quantity.";
            echo json_encode($response);
        }
        else if($result) {
            $response["status"] = "success";
            echo json_encode($response);
        } else { 
            $response["error"] = TRUE;
            $response["error_msg"] = "Unknow error";
            echo json_encode($response);
        }
    } else {
        // required post params is missing
        $response["error"] = TRUE;
        $response["error_msg"] = "params is missing!";
        echo json_encode($response);
    }
}
else
{
    $response["error"] = TRUE;
    $response["error_msg"] = "Missing Connection";
    echo json_encode($response);
}
?>