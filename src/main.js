import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store/'
import VueSweetalert2 from 'vue-sweetalert2';
import BootstrapVue from 'bootstrap-vue'

// import '@pencilpix/vue2-clock-picker/dist/vue2-clock-picker';
// const VueClockPickerPlugin = require('@pencilpix/vue2-clock-picker/dist/vue2-clock-picker.plugin.js')
// Vue.use(VueClockPickerPlugin)

import './registerServiceWorker'
import 'bootstrap/dist/css/bootstrap.css'
import 'font-awesome/css/font-awesome.min.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueCookies from 'vue-cookies'

Vue.use(BootstrapVue);
Vue.use(VueCookies)
Vue.use(VueSweetalert2);

Vue.config.productionTip = false
Vue.mixin({
  methods: {
    goFoodgame(param){
      router.push('food-game');
    },
    goresto(){
      router.push('resto');
    },
    goMenu(id){
      router.push('menu/'+id);
    },
    logout(){

      this.$swal({
        title: '',
        text: "Are you sure to logout?",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes',
        cancelButtonText: 'Cancel'
      }).then((result) => {
        if (result.value) {
        VueCookies.remove('spedrestoId');
        VueCookies.remove('spedrestoEmail');
        router.push('/');
        }
      })
      // console.log("asd");
      

    },
    formatPrice(value) {
      let val = (value/1).toFixed(0).replace('.', ',')
      return val.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ".")
    },
    
    cookiesCheck(){
      if(!$cookies.get('spedrestoId')&&!$cookies.get('spedrestoEmail')){
        router.push('/');
      }
    }
  }
})
new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
