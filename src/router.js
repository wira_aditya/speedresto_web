import Vue from 'vue'
import Router from 'vue-router'
import Home from './views/home/Home.vue'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home
    },
    {
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/About.vue')
    },
    {
      path: '/home-user',
      name: 'home-user',
      component: () => import(/* webpackChunkName: "about" */ './views/home/HomeUser.vue')
    },
    {
      path: '/food-game',
      name: 'food-game',
      component: () => import(/* webpackChunkName: "about" */ './views/foodgame/Foodgame.vue')
    },
    {
      path: '/menu',
      name: 'menu',
      component: () => import(/* webpackChunkName: "about" */ './views/menu/Menu.vue')
    },
    {
      path: '/cart',
      name: 'cart',
      component: () => import(/* webpackChunkName: "about" */ './views/cart/Cart.vue')
    },
    {
      path: '/booking',
      name: 'booking',
      component: () => import(/* webpackChunkName: "about" */ './views/booking/Booking.vue')
    }
    
  ]
})
