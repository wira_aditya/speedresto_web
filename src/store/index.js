import Vue from 'vue';
import Vuex from 'vuex';

import auth from './modules/auth';
import resto from './modules/resto';
import cart from './modules/cart';
// import user from './modules/user';
// import report from './modules/report';
// import serial from './modules/serial';
// import resetAktivasi from './modules/resetAktivasi';

Vue.use(Vuex);

const store = new Vuex.Store({
  // strict: true, // process.env.NODE_ENV !== 'production',
  modules: {
    resto,
    auth,
    cart,
    // user,
    // report,
    // serial,
    // resetAktivasi,
  },
  state: {
    
    // API_URL: (process.env.NODE_ENV === 'production') ? 'https://indocloudsoft.com/speedresto_beta3/' : 'http://localhost/speedresto_web/api/',
    API_URL: (process.env.NODE_ENV === 'production') ? 'https://indocloudsoft.com/speedresto/' : 'http://192.168.0.108/speedresto/',
    success_flag: false,
  },
  mutations: {
    SET_SUCCESS(State, status) {
      State.success_flag = status;
    },
  },
  actions: {
    setSuccessFlag({ commit }, status) {
      commit('SET_SUCCESS', status);
    },
  },
  getters: {
    isSuccess: state => state.success_flag,
  },
});

export default store;
