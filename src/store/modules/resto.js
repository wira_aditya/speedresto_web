import axios from 'axios';

// action
const actions = {
  async getFoodGame({ commit }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}web/getFoodGameList.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getResto({ commit }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}web/getRestoList.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getRestoById({ commit }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}web/getRestoListById.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getListMenu({ commit }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}web/getListMenu.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },

};
const mutations = {
  setState(state,data){
    state.tax = data.tax;
    state.service = data.service;
    state.disc = data.disc;
  }
};
const state = {
  tax:'',
  service:'',
  disc:''
};
export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
