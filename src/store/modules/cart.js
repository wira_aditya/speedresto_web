import axios from 'axios';

// action
const actions = {
  pushCart({commit}, data){
    commit('setCart',data);
  },
  resetCart({commit}){
    commit('reset');
  },
  reCreate({commit},data){
    commit('reCreate',data);
  },
  async submitCart({ commit }, params) {
    console.log(params);
    try {
      const res = await axios.post(`${this.state.API_URL}/web/createBooking.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getHistory({ commit }, params) {
    try {
      const res = await axios.post(`${this.state.API_URL}/web/getListBookingCustomer.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
  async getDetailHistory({commit}, params){
    try {
      const res = await axios.post(`${this.state.API_URL}/web/getListMenuDetailBookingCustomer.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  }
};
const mutations = {
  setCart(state,data){
    state.cartData.push(data)
  },
  reCreate(state,data){
    state.cartData = data
  },
  reset(state,data){
    state.cartData = []
  }
};
const state = {
  cartData:[]
};
export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
