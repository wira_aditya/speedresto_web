import axios from 'axios';

// action
const actions = {
  async getlogin({ commit }, params) {
    console.log(params);
    try {
      const res = await axios.post(`${this.state.API_URL}/web/register_customer.php`, params, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });
      return res.data;
    } catch (error) {
      return error;
    }
  },
};
const mutations = {};
const state = {
};
export default {
  namespaced: true,
  actions,
  mutations,
  state,
};
